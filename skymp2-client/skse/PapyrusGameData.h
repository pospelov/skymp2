#pragma once

class VMClassRegistry;
struct StaticFunctionTag;

#include "GameTypes.h"

namespace papyrusGameData
{
	void RegisterFuncs(VMClassRegistry* registry);
}