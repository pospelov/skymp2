#pragma once
#include <skymp2-cellhost/src/NetInterface.h>

class UDPClient : public IClient {
public:
	void connect(const char *ipAddress, unsigned short port) override;
	bool receive(NetEvent &event) noexcept override;
	void send(const RawMessage &message, Reliability reliability) noexcept override;

	UDPClient();
	~UDPClient() override;

private:
	struct Impl;
	Impl *const pImpl;

	UDPClient(const UDPClient &) = delete;
	UDPClient &operator=(const UDPClient &) = delete;
};