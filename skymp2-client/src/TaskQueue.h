#pragma once
class TaskQueue
{
public:
	using FPtr = std::function<void()> *;

	void Add(std::function<void()> fn) {
		this->q.push(new std::function<void()>(fn));
	}

	void Update() {
		try {
			FPtr f;
			while (this->q.pop(f)) {
				(*f)();
				delete f;
			}
		}
		catch (std::exception &) {
			throw;
		}
		catch (...) {
			throw std::logic_error("unhandled error in task");
		}
	}

	void RunTopAndClear() {
		vector<FPtr> fns;
		FPtr tmp;
		while (this->q.pop(tmp)) {
			fns.push_back(tmp);
		}
		if (!fns.empty()) {
			fns.back()->operator()();
			for (auto p : fns) delete p;
		}
	}

private:
	boost::lockfree::queue<std::function<void()> *, boost::lockfree::capacity<20000>> q;
};