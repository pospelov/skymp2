#include "stdafx.h"
#include "Net.h"
#include "services/GUI.h" // struct AuthData

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include <chrono>
using namespace std::chrono_literals;

#include "sdutil/Spawn.h"
#include "sdutil/PlayWav.h"
#include "sdutil/FormInfo.h"

#include "components/Look.h"

#include <unordered_set>

#include <skymp2-cellhost/src/Rak.h>

#define BTNID_ACCEPT_PARTY_INVITE "v20.hardcoded.acceptPartyInvite"
#define BTNID_CLOSE_DIALOG "v20.hardcoded.closeDialog"

enum : uint8_t {
	CONTENT_CHAT = 10,
	CONTENT_PARTY_INVITE = 11,
	CONTENT_PARTY_REQONCE = 12,
	CONTENT_CHEST_UPDATE = 13,
	CONTENT_WHISPER_START = 14,
	CONTENT_MPCH_ALIVE = 15,
};

inline int GetNumDgrams(uint8_t contentType) {
	switch (contentType) {
	case CONTENT_CHEST_UPDATE: return 3;
	default: return 10;
	}
}

using boost::asio::ip::udp;
using tcp = boost::asio::ip::tcp;
namespace http = boost::beast::http;

using reqid = uint16_t;
using Response = http::response<http::string_body>;

struct response_data
{
	Response response;
	reqid requestId = 0;
	boost::system::error_code err;
};

using res_queue = boost::lockfree::queue<response_data *, boost::lockfree::capacity<65534>>;
using res_queue_ptr = std::shared_ptr<res_queue>;

// Performs an HTTP GET and prints the response
class session : public std::enable_shared_from_this<session>
{
	tcp::resolver resolver_;
	tcp::socket socket_;
	boost::beast::flat_buffer buffer_; // (Must persist between reads)
	http::request<http::string_body> req_;
	Response res_;
	res_queue_ptr q_;
	json body_;

	void fail(boost::system::error_code e, const char *what) {
		auto data = new response_data;
		data->err = e;
		data->requestId = this->requestId_;
		this->q_->push(data);
	}

public:
	const reqid requestId_;
	const http::verb method_;

	// Resolver and socket require an io_context
	explicit
		session(boost::asio::io_context& ioc, res_queue_ptr q, reqid requestId, http::verb method, json body)
		: resolver_(ioc)
		, socket_(ioc)
		, q_(q)
		, requestId_(requestId)
		, method_(method)
		, body_(body)
	{
	}

	// Start the asynchronous operation
	void
		run(
			char const* host,
			char const* port,
			char const* target,
			int version)
	{
		// Set up an HTTP request message
		req_.version(version);
		req_.method(this->method_);
		req_.target(target);
		req_.set(http::field::host, host);
		req_.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
		if (this->method_ == http::verb::post || this->method_ == http::verb::put) {
			req_.set(http::field::content_type, "application/json");
			req_.body() = this->body_.dump();
			req_.prepare_payload();
		}

		// Look up the domain name
		resolver_.async_resolve(
			host,
			port,
			std::bind(
				&session::on_resolve,
				shared_from_this(),
				std::placeholders::_1,
				std::placeholders::_2));
	}

	void
		on_resolve(
			boost::system::error_code ec,
			tcp::resolver::results_type results)
	{
		if (ec)
			return fail(ec, "resolve");

		// Make the connection on the IP address we get from a lookup
		boost::asio::async_connect(
			socket_,
			results.begin(),
			results.end(),
			std::bind(
				&session::on_connect,
				shared_from_this(),
				std::placeholders::_1));
	}

	void
		on_connect(boost::system::error_code ec)
	{
		if (ec)
			return fail(ec, "connect");

		// Send the HTTP request to the remote host
		http::async_write(socket_, req_,
			std::bind(
				&session::on_write,
				shared_from_this(),
				std::placeholders::_1,
				std::placeholders::_2));
	}

	void
		on_write(
			boost::system::error_code ec,
			std::size_t bytes_transferred)
	{
		boost::ignore_unused(bytes_transferred);

		if (ec)
			return fail(ec, "write");

		// Receive the HTTP response
		http::async_read(socket_, buffer_, res_,
			std::bind(
				&session::on_read,
				shared_from_this(),
				std::placeholders::_1,
				std::placeholders::_2));
	}

	void
		on_read(
			boost::system::error_code ec,
			std::size_t bytes_transferred)
	{
		boost::ignore_unused(bytes_transferred);

		if (ec)
			return fail(ec, "read");

		auto data = new response_data;
		data->response = res_;
		data->requestId = this->requestId_;
		this->q_->push(data);

		// Gracefully close the socket
		socket_.shutdown(tcp::socket::shutdown_both, ec);

		// not_connected happens sometimes so don't bother reporting it.
		if (ec && ec != boost::system::errc::not_connected)
			return fail(ec, "shutdown");

		// If we get here then the connection is closed gracefully
	}
};

using Callback = std::function<void(Response, boost::system::error_code)>;

Inventory ConvertJsonToInv(json j) {
	Inventory res;
	auto entries = j.at("entries");
	auto size = entries.size();
	for (size_t i = 0; i < size; ++i) {
		Inventory::Entry e;
		e.count = entries[i]["count"];
		e.id = FormInfo::fromJson(entries[i]["form"]).toLocalId();
		res.AddItem(e);
	}
	return res;
}

struct SRunParty
{
	clock_t lastReq = 0;
	bool reqInProgress = false;
	json actualParty = nullptr;
	bool reqOnce = false;
};

struct SHandshake
{
	std::recursive_mutex resM;
	std::unique_ptr<AuthData> result;

	void SetResult(const AuthData &base, boost::system::error_code ec, const Response &resp) {
		auto res = new AuthData(base);
		res->responseBody = resp.body();
		res->responseStatus = ec ? 0 : resp.result_int();
		res->systemError = ec.message();

		std::lock_guard<std::recursive_mutex> l(resM);
		result.reset(res);
	}
};

inline std::pair<string, string> ParseHostAndPort(const string &hostAndPort) {
	string host;
	string port;
	for (size_t i = 0; i != hostAndPort.size(); ++i) {
		if (hostAndPort[i] == ':') {
			port = "read me pls";
			break;
		}
		host.push_back(hostAndPort[i]);
	}
	if (port == "read me pls") {
		port.clear();
		for (INT i = hostAndPort.size() - 1; i >= 0; --i) {
			if (hostAndPort[i] == ':') break;
			port = hostAndPort[i] + port;
		}
	}
	return { host, port };
}

template <class T> using LockfreeQueue = boost::lockfree::queue<T, boost::lockfree::capacity<65534>>;
using IOCTask = std::function<void(boost::asio::io_context &)>;

struct Net::Impl
{
	std::unique_ptr<CellhostClient> cellhostClient;

	LockfreeQueue<std::function<void()> *> immediateCallbacks;
	DWORD threadId = 0;
	std::shared_ptr<boost::asio::io_context> ioc;
	std::shared_ptr<volatile bool> pAlive;
	std::shared_ptr<LockfreeQueue<IOCTask *>> iocTasks;

	res_queue_ptr results;
	reqid unused_reqid = 0;
	vector<Callback> request_callbacks;
	vector<std::function<void(World &)>> tasks;
	string serverAddress;
	std::set<string> cellhostAddrs;
	bool cellhostSearching = false;
	formid cellOrWorld = 0;
	formid cell = 0;
	json mpch;
	SRunParty runParty;
	SHandshake handshake;
	int confirmRes = -1;
	int retryConfirmRes = -1;
	int startResetPassRes = -1;
	int finishResetPassRes = -1;
	bool newGameSound = false;
	bool timeReq = false;
	clock_t loadMyInventory = 0;

	volatile bool requestDataInProgress[2] = { false, false };
	clock_t lastPing = 0;
	clock_t lastUpdate = 0;
	json myLook;
	string myName;
	Equipment myEq;
	Movement myMov;
	std::vector<std::pair<clock_t, std::function<void()>>> timers;
	std::unordered_set<int> knownMessages;
	std::unordered_set<formid> touched; // activated refs
	std::list<std::pair<json, clock_t>> sentMovements;
	std::list<clock_t> sentPackets;
	std::atomic<int> ping, lagFull, outPacketRate;

	volatile bool isConnected = false; // TODO: remove volatile?

	void Req(http::verb method, const string &hostAndPort, const string &target, const json &body, Callback cb);
	void SetTimer(clock_t end, std::function<void()> f);
	void SetImmediate(std::function<void()> f); // threadsafe

	const string &GetTarget() const {
		return serverAddress;
	}

	formid GetMyId() const {
		if (!mpch.count("id")) return 0;
		return 0xff000000 + mpch["id"].get<formid>();
	}

	string GetToken() const {
		return mpch.is_object() ? mpch["token"].get<string>() : "";
	}
};

void Net::Impl::Req(http::verb method, const string &hostAndPort, const string &target, const json &body, Callback cb) {
	string host = ParseHostAndPort(hostAndPort).first;
	string port = ParseHostAndPort(hostAndPort).second;

	if (this->request_callbacks.size() != 65536) {
		this->request_callbacks.resize(65536, nullptr);
	}

	auto id = this->unused_reqid++;
	this->request_callbacks[id] = cb;

	auto q = this->results;
	iocTasks->push(new IOCTask([=](boost::asio::io_context &ioc) {
		std::shared_ptr<session> ses;
		ses.reset(new session(ioc, q, id, method, body));
		ses->run(host.data(), port.data(), target.data(), 11);
	}));
	//std::thread([=] { ioc->run(); }).detach();
}

void Net::Impl::SetTimer(clock_t end, std::function<void()> fn) {
	timers.push_back({ end, fn });
}

void Net::Impl::SetImmediate(std::function<void()> fn) {
	immediateCallbacks.push(new std::function<void()>(fn));
}

Net::Net(const string &serverAddress) {
	assert(ParseHostAndPort("localhost:666").first == "localhost");
	assert(ParseHostAndPort("localhost:666").second == "666");
	assert(ParseHostAndPort("localhost").first == "localhost");
	assert(ParseHostAndPort("localhost").second == "");

	pImpl.reset(new Impl);
	pImpl->results.reset(new res_queue);
	pImpl->serverAddress = serverAddress;

	auto pAlive = pImpl->pAlive = std::make_shared<volatile bool>(true);
	auto iocTasks = pImpl->iocTasks = std::make_shared<LockfreeQueue<IOCTask *>>();

	std::thread([pAlive, iocTasks] {
		boost::asio::io_context ioc;
		while (*pAlive) {
			Sleep(25);
			ioc.run_for(25ms);

			IOCTask *f = nullptr;
			while (iocTasks->pop(f)) {
				(*f)(ioc);
				delete f;
			}
		}
	}).detach();
}

Net::~Net() {
	(*pImpl->pAlive) = false;
	pImpl.reset();
}

void Net::FlushToWorld(World *world) {
	pImpl->threadId = GetCurrentThreadId();
	this->RunCallbacks();
	while (this->RunUDP());
	this->RunParty();
	this->RunImmediateCallbacks();

	if (world) {
		for (auto task : pImpl->tasks) task(*world);
		pImpl->tasks.clear();
	}

	// Update
	auto token = pImpl->GetToken();
	if (token.size()) {
		if (!pImpl->lastUpdate || clock() - pImpl->lastUpdate > 5 * 1000) {
			pImpl->lastUpdate = clock();
			auto body = json{
				{ "eq", pImpl->myEq }, { "mov", pImpl->myMov }
			};
			pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH/update/" + token, body, [this](Response response, boost::system::error_code ec) {
			});
		}
	}

	// Load inventory if need
	if (pImpl->loadMyInventory && clock() - pImpl->loadMyInventory > 0) {
		this->LoadMyInventory();
		pImpl->loadMyInventory = 0;
	}

	// Request date/time from server
	if (pImpl->timeReq == false) {
		pImpl->timeReq = true;
		pImpl->SetImmediate([this] {
			pImpl->Req(http::verb::get, pImpl->GetTarget(), "/date", {}, [this](Response &resp, boost::system::error_code ec) {
				if (resp.result_int() != 200) return;
				Console_Print("%s", resp.body().data());
				try {
					auto j = SAFE_PARSE(resp.body());
					pImpl->tasks.push_back([j](World & w) {
						w.UpdateDateTime(j);
					});
				}
				catch (std::exception &e) {
					Console_Print("Date Error %s", e.what());
				}
			});
		});
	}
}

void Net::Task(std::function<void()> f) {
	pImpl->SetImmediate(f);
}

void Net::Handshake(const AuthData &auth, const string &version, bool crashed) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->Handshake(auth, version, crashed);
	});
	Console_Print("handshake");

	pImpl->myName = auth.name;
	json nameAndPass = { { "name", auth.name },{ "password", auth.password }, { "version", version } };
	if (auth.type == AuthData::Register) {
		nameAndPass["email"] = auth.email;
		nameAndPass["invite"] = auth.invite;
		pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH", nameAndPass, [=](Response resp, boost::system::error_code ec) {
			if (!ec && resp.result() == http::status::created) {
				auto mpch = SAFE_PARSE(resp.body());
				pImpl->mpch = mpch;
				//this->Spawn(mpch);
				pImpl->isConnected = true;
				Console_Print("Successful register");
			}
			std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
			pImpl->handshake.SetResult(auth, ec, resp);

		});
	}
	else if (auth.type == AuthData::Login) {
		nameAndPass["crashed"] = crashed;
		pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH/login", nameAndPass, [=](Response resp, boost::system::error_code ec) {
			if (!ec && resp.result() == http::status::ok) {
				auto mpch = SAFE_PARSE(resp.body());
				pImpl->mpch = mpch;
				this->Spawn(mpch);
				pImpl->isConnected = true;
				Console_Print("Successful login");
			}
			std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
			pImpl->handshake.SetResult(auth, ec, resp);
		});
	}
	else assert(0);
}

std::unique_ptr<AuthData> Net::GetHandshakeResult() {
	std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
	return std::move(pImpl->handshake.result);
}

void Net::Confirm(string name, string confirmCode) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->Confirm(name, confirmCode);
	});
	json nameAndPass = { { "name", name },{ "confirmCode", confirmCode } };

	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH/confirm", nameAndPass, [=](Response resp, boost::system::error_code ec) {
		int res = 0;
		if (!ec) {
			res = resp.result_int();
		}
		std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
		pImpl->confirmRes = res;
		pImpl->newGameSound = true;
	});
}

int Net::GetConfirmResult() {
	std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
	int res = pImpl->confirmRes;
	pImpl->confirmRes = -1;
	return res;
}

void Net::RetryConfirm(string name) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->RetryConfirm(name);
	});

	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH/confirm/retry", { { "name", name } }, [=](Response resp, boost::system::error_code ec) {
		int res = 0;
		if (!ec) res = resp.result_int();
		std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
		pImpl->retryConfirmRes = res;
		pImpl->newGameSound = true;
	});
}

int Net::GetRetryConfirmResult() {
	std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
	int res = pImpl->retryConfirmRes;
	pImpl->retryConfirmRes = -1;
	return res;
}

void Net::StartResetPassword(string email) {
	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH/resetPass", { { "email", email } }, [=](Response resp, boost::system::error_code ec) {
		std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
		pImpl->startResetPassRes = ec ? 0 : resp.result_int();
	});
}

int Net::GetStartResetPasswordResult() {
	std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
	int res = pImpl->startResetPassRes;
	pImpl->startResetPassRes = -1;
	return res;
}

void Net::FinishResetPassword(string code, string password) {
	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH/resetPass/finish", { { "code", code },{ "password", password } }, [=](Response resp, boost::system::error_code ec) {
		std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
		pImpl->finishResetPassRes = ec ? 0 : resp.result_int();
	});
}

int Net::GetFinishResetPasswordResult() {
	std::lock_guard<std::recursive_mutex> l(pImpl->handshake.resM);
	int res = pImpl->finishResetPassRes;
	pImpl->finishResetPassRes = -1;
	return res;
}

void Net::AddMarker(formid id) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->AddMarker(id);
	});
	auto body = json::object();
	body["fields"] = { { "markers", pImpl->mpch["markers"] } };
	body["fields"]["markers"].push_back(FormInfo::Resolve(id).toJson());
	pImpl->Req(http::verb::put, pImpl->GetTarget(), "/records/MPCH/" + pImpl->GetToken(), body, [this](Response &resp, boost::system::error_code ec) {
	});
}

void Net::RequestData(vector<formid> ids, bool hostable) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->RequestData(ids, hostable);
	});

	if (pImpl->requestDataInProgress[hostable]) return;

	auto body = json::array();
	for (auto id : ids) {
		body.push_back(FormInfo::Resolve(id).toJson());
	}
	body.push_back(FormInfo::Resolve(pImpl->cellOrWorld).toJson());
	body.push_back(pImpl->myMov);

	pImpl->requestDataInProgress[hostable] = true;
	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/deprecated/requestdata", body, [=](Response resp, boost::system::error_code ec) {
		pImpl->requestDataInProgress[hostable] = false;
		if (ec) return;
		if (resp.result() != http::status::ok) return;

		auto results = SAFE_PARSE(resp.body());
		for (int i = 0; i < results.size(); ++i) {
			json refr = results[i];
			Movement mov;
			try {
				auto d = refr.dump();
				mov._pos = refr["DATA"]["pos"];
				mov.SetRotRadians(refr["DATA"]["rot"]);
				if (refr["isOpen"]) mov._flags |= Movement::Open;
				else mov._flags &= ~Movement::Open;

				bool harvested = mov.IsOpen();
				if (!hostable || harvested) pImpl->tasks.push_back([=](World &w) {
					if (refr.at("espm") == "") {
						w.UpdateMovement(0xff000000 + refr["id"].get<formid>(), mov, FormInfo::fromJson(refr["NAME"]).toLocalId(), -1, refr.count("XCNT") ? refr.at("XCNT") : 0);
					}
					else {
						const formid id = FormInfo::fromJson(refr).toLocalId();
						w.UpdateMovement(id, mov, 0, -1, 0);
					}
				});
			}
			catch (std::exception &e) {
				// TODO
			}
		}
	});
}

void Net::Activate(formid caster, formid target, int baseType, bool open) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->Activate(caster, target, baseType, open);
	});
	pImpl->touched.insert(target);

	// 'caster' is unused argument now
	// we think caster is always our MPCH

	auto path = "/records/deprecated/activate/" + pImpl->GetToken();
	json body = FormInfo::Resolve(target).toJson();
	body["setOpen"] = open;
	body["typeId"] = baseType;

	auto s = body.dump();
	pImpl->Req(http::verb::post, pImpl->GetTarget(), path, body, [this, target, baseType](Response &resp, boost::system::error_code ec) {
		if (ec || resp.result_int() >= 400) return;
		auto j = SAFE_PARSE(resp.body());

		if (j.count("entries")) {
			auto entries = j.at("entries");
			for (int i = 0; i < entries.size(); ++i) {
				auto e = entries[i];
				formid id = FormInfo::fromJson(e.at("form")).toLocalId();
				uint32_t count = e.at("count");

				// We need theese lines because
				// World::Activate() is not working with 0xff items
				bool isItem = baseType != 38 && baseType != 39;
				if (target >= 0xff000000 && isItem) {
					pImpl->tasks.push_back([=](World &w) {
						w.AddItem(id, count);
						w.BroadcastEraseEntity(target);
					});
				}
			}
		}

		pImpl->tasks.push_back([=](World &w) {
			if (j.count("activate") && j["activate"]) w.Activate(target);
			if (j.count("isOpen")) w.SetOpen(target, j["isOpen"]);
			if (j.count("targetInv")) {
				auto targetInv = ConvertJsonToInv(j.at("targetInv"));
				w.UpdateInventory(target, targetInv);
			}
		});
	});
}

void Net::UpdateMovement(formid id, const Movement &mov, float uiHealth, float uiMagicka, float uiStamina, formid cellOrWorld, formid cell) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->UpdateMovement(id, mov, uiHealth, uiMagicka, uiStamina, cellOrWorld, cell);
	});

	if (cellOrWorld != pImpl->cellOrWorld) {
		pImpl->cellhostAddrs.clear();
		pImpl->cellOrWorld = cellOrWorld;
	}
	if ((pImpl->cellhostAddrs.empty() || cell != pImpl->cell) && !pImpl->cellhostSearching) {
		pImpl->cell = cell;
		pImpl->cellhostSearching = true;
		pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/MPCH/cellhost/" + pImpl->GetToken(), (Movement)mov, [this](Response response, boost::system::error_code ec) {
			pImpl->cellhostSearching = false;
			if (ec || response.result_int() >= 400) return;
			auto cellhosts = SAFE_PARSE(response.body());
			assert(cellhosts.is_array());

			auto &addrs = pImpl->cellhostAddrs;
			addrs.clear();
			for (int i = 0; i < cellhosts.size(); ++i) {
				addrs.insert(cellhosts.at(i).get<string>());
			}
			Console_Print("connected to %u", addrs.size());

			auto autorecClient = new AutorecClient([] { return new RakClient; });
			if (!addrs.empty() && !pImpl->cellhostClient) {
				const auto addr = *addrs.begin();
				const auto pair = ParseHostAndPort(addr);
				autorecClient->connect(pair.first.c_str(), atoi(pair.second.c_str()));
				pImpl->cellhostClient = std::make_unique<CellhostClient>(autorecClient);
				Console_Print("init cellhostClient");
			}
		});
	}

	static bool srand_ = false;
	if (!srand_) {
		srand_ = true;
		srand(time(0));
	}

	const int r = rand();
	for (auto addr : pImpl->cellhostAddrs) {
		auto packet = json::object();
		packet["movement"] = Movement(mov);
		packet["uiavs"] = json::array({ uiHealth, uiMagicka, uiStamina });
		packet["cellOrWorld"] = cellOrWorld;
		packet["ownerId"] = id ? id : pImpl->GetMyId();
		packet["token"] = pImpl->mpch["token"];
		packet["myId"] = pImpl->GetMyId();
		packet["rand"] = r;
		if (packet["ownerId"] == packet["myId"] && pImpl->myLook != nullptr) {
			packet["look"] = pImpl->myLook;
			packet["name"] = pImpl->myName;
			packet["eq"] = pImpl->myEq;
			pImpl->myMov = mov;
		}
		const CHMessagePrefix pr = {
			(int16_t)floor(mov._pos.x / 4096.f), (int16_t)floor(mov._pos.y / 4096.f), cellOrWorld, packet["ownerId"], pImpl->GetMyId()
		};
		if (pImpl->cellhostClient) {
			const auto dump = packet.dump();
			pImpl->cellhostClient->send(pr, dump);
			pImpl->sentPackets.push_back(clock());
			pImpl->sentMovements.push_back({ packet, clock() });
			if (pImpl->sentMovements.size() > 5) {
				pImpl->sentMovements.pop_front();
			}
		}
	}
}

void Net::UpdateEquipment(const Equipment &eq) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->UpdateEquipment(eq);
	});
	pImpl->myEq = eq;
}

void Net::UpdateLook(const Look &look) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->UpdateLook(look);
	});

	pImpl->myLook = (Look)look;
	json body = json::object();
	body["fields"] = { {"look", (Look)look} };
	pImpl->Req(http::verb::put, pImpl->GetTarget(), "/records/MPCH/" + pImpl->GetToken(), body, [this](Response &resp, boost::system::error_code ec) {
		if (ec) {
			pImpl->tasks.push_back([](World &w) { w.ShowRaceMenu(); });
			return;
		}
		if (resp.result_int() >= 400) {
			pImpl->tasks.push_back([](World &w) { w.ShowRaceMenu(); });
		}
	});
}

void Net::Broadcast(formid ownerId, uint8_t contentType, const string &content, int numDgrams) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->Broadcast(ownerId, contentType, content, numDgrams);
	});

	const int r = rand();
	for (int i = 0; i < numDgrams; ++i) {
		for (auto addr : pImpl->cellhostAddrs) {
			auto packet = json::object();

			auto charArray = json::array();
			for (auto ch : content) charArray.push_back((int)ch);
			packet["content"] = charArray;

			packet["contentType"] = (int)contentType;
			packet["ownerId"] = ownerId;
			packet["rand"] = r;

			enum {
				INVALID_CELL_X = -10000,
				INVALID_CELL_Y = -10000
			};
			if (pImpl->cellhostClient) {
				const auto dump = packet.dump();
				pImpl->cellhostClient->send({ INVALID_CELL_X, INVALID_CELL_Y, 0, ownerId, 0 }, dump);
				pImpl->sentPackets.push_back(clock());
			}
		}
	}
}

void Net::InventoryEvent(const InvEvent &ie) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->InventoryEvent(ie);
	});

	json body = json::object();
	body["type"] = ie.type;
	body["ref"] = FormInfo::Resolve(ie.ref).toJson();
	body["entry"] = json::object();
	body["entry"]["form"] = FormInfo::Resolve(ie.entry.id).toJson();
	body["entry"]["count"] = ie.entry.count;
	body["movement"] = pImpl->myMov;
	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/records/deprecated/invevent/" + pImpl->GetToken(), body, [=](Response resp, boost::system::error_code ec) {

		pImpl->loadMyInventory = clock() + 500;
		pImpl->SetTimer(clock() + 500, [=] {
			if (ie.type != string("drop")) {
				this->Broadcast(pImpl->GetMyId(), CONTENT_CHEST_UPDATE, std::to_string(ie.ref), GetNumDgrams(CONTENT_CHEST_UPDATE));
			}
		});
	});
}

void Net::SendDialogResponse(const string &buttonId, const json &dialogData) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->SendDialogResponse(buttonId, dialogData);
	});

	json body = { {"dialogData", dialogData} };

	if (buttonId == BTNID_ACCEPT_PARTY_INVITE) {
		// Prediction of dialog window closing by the server
		pImpl->tasks.push_back([](World &w) { w.SetDialogData(nullptr); });
	}

	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/dialog/" + pImpl->GetToken() + "/" + buttonId, body, [this](Response resp, boost::system::error_code ec) {
		this->Broadcast(pImpl->GetMyId(), CONTENT_PARTY_REQONCE, "", GetNumDgrams(CONTENT_PARTY_REQONCE));
		if (ec || resp.result_int() >= 400) return;
		auto j = SAFE_PARSE(resp.body());
		pImpl->tasks.push_back([j](World &w) {
			w.SetDialogData(j.count("newDialog") ? j.at("newDialog") : nullptr);
		});
	});
}

void Net::SendChatMessage(const string &messageJsonDump) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->SendChatMessage(messageJsonDump);
	});

	auto message = SAFE_PARSE(messageJsonDump);
	message["sender"] = pImpl->myName;
	message["movement"] = pImpl->myMov;
	if (pImpl->runParty.actualParty != nullptr) {
		if (pImpl->runParty.actualParty.count("leader") == 0) return;
		message["partyLeader"] = pImpl->runParty.actualParty.at("leader");
	}
	this->Broadcast(pImpl->GetMyId(), CONTENT_CHAT, message.dump());
}

void Net::SendInteractionClick(const string &interactionClickJsonDump) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->SendInteractionClick(interactionClickJsonDump);
	});

	enum {
		INDEX_PARTY_MAKELEADER = 1001,
		INDEX_PARTY_KICK_OR_LEAVE = 1000,
		INDEX_WHISPER = 2,
		INDEX_EXCHANGE = 1,
		INDEX_INVITE = 0
	};

	auto path = "/records/deprecated/sendinteractionclick/" + pImpl->GetToken();
	auto body = SAFE_PARSE(interactionClickJsonDump);
	formid targetId = body.at("targetid");
	int index = body.at("index");

	auto &rpa = pImpl->runParty;

	switch (index) {
	case INDEX_PARTY_MAKELEADER:
		if (rpa.actualParty != nullptr) {
			// Predict party leader
			rpa.lastReq = clock();
			rpa.actualParty["leader"] = targetId;
			pImpl->tasks.push_back([this](World &w) { w.SetPartyData(pImpl->runParty.actualParty); });
		}
		break;
	case INDEX_PARTY_KICK_OR_LEAVE:
		if (targetId == pImpl->runParty.actualParty["leader"]) { // leave
			// Predict leave
			rpa.lastReq = clock();
			pImpl->tasks.push_back([](World &w) { w.SetPartyData(nullptr); });
		}
		break;
	case INDEX_WHISPER:
	{
		auto jContent = json::object();
		jContent["targetId"] = targetId;
		jContent["channelId"] = "whisper:" + std::to_string(rand()) + "-" + std::to_string(targetId);
		jContent["targetName"] = body.at("targetName");
		jContent["ownerName"] = pImpl->mpch.at("displayName");
		return this->Broadcast(pImpl->GetMyId(), CONTENT_WHISPER_START, jContent.dump(), 3); 
		// Implemented with cellhost; doesn't need HTTP request
	}
	default:
		break;
	}

	pImpl->Req(http::verb::post, pImpl->GetTarget(), path, body, [this, targetId, index](Response resp, boost::system::error_code ec) {
		if (ec) return Console_Print("SendInteractionClick: system error");
		if (resp.result_int() >= 400) return Console_Print("SendInteractionClick: %d %s", resp.result_int(), resp.body().data());
		pImpl->SetTimer(clock() + 300, [this] { pImpl->runParty.reqOnce = true; });

		json j;
		try {
			j = SAFE_PARSE(resp.body());
		}
		catch (...) {
			j = json::object();
		}

		switch (index) {
		case INDEX_INVITE:
			this->Broadcast(targetId, CONTENT_PARTY_INVITE, pImpl->myName, GetNumDgrams(CONTENT_PARTY_INVITE));
			break;
		default:
			break;
		}

		Console_Print("SendInteractionClick %s", resp.body().data());
	});
}

void Net::LoadMyInventory() {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->LoadMyInventory();
	});
	pImpl->Req(http::verb::get, pImpl->GetTarget(), "/records/MPCH/" + pImpl->GetToken(), {}, [this](Response resp, boost::system::error_code ec) {
		if (ec || resp.result_int() >= 400) return Console_Print("achtung 1");
		auto mpch = SAFE_PARSE(resp.body());
		auto inv = ConvertJsonToInv(mpch.at("inv"));
		pImpl->tasks.push_back([this, inv](World &w) {
			w.UpdateInventory(pImpl->GetMyId(), inv);
		});
	});
}

void Net::SendScriptEvent(const string &name, const string &content, std::function<void(int, string)> callback) {
	if (pImpl->threadId != GetCurrentThreadId()) return pImpl->SetImmediate([=] {
		this->SendScriptEvent(name, content, callback);
	});
	json body = json::object();
	body["content"] = content;
	body["token"] = pImpl->GetToken();
	body["name"] = name;
	pImpl->Req(http::verb::post, pImpl->GetTarget(), "/api/scriptevent", body, [this, callback](Response &resp, boost::system::error_code ec) {
		if (ec) callback(0, "");
		else callback(resp.result_int(), resp.body());
	});
}

bool Net::IsConnected() const {
	return pImpl->isConnected;
}

bool Net::IsRequestDataInProgress(bool hostable) const {
	return pImpl->requestDataInProgress[hostable];
}

void Net::Spawn(json mpch) {
	json DATA = mpch["DATA"];
	const formid spawnId = FormInfo::fromJson(mpch.count("worldspace") ? mpch["worldspace"] : mpch["cell"]).toLocalId();
	float angleZ = DATA["rot"][2];
	NiPoint3 pos = { DATA["pos"][0], DATA["pos"][1], DATA["pos"][2] };

	// Fix Broken Khajiit Tail
	if (mpch.count("look") && mpch.at("look").count("raceID")) {
		enum {
			KhajiitRace = 79685,
			ArgonianRace = 0x00013740
		};
		formid raceId = mpch.at("look").at("raceID");
		if (raceId == KhajiitRace || raceId == ArgonianRace) {
			static bool madeKhajiit = false;
			if (madeKhajiit == false) {
				if (g_thePlayer && (*g_thePlayer)->baseForm && LookupFormByID(raceId)) {
					madeKhajiit = true;
					Sleep(100);
					((TESNPC *)(*g_thePlayer)->baseForm)->race.race = (TESRace *)LookupFormByID(raceId);
				}
			}
		}
	}

	clock_t was = clock();
	sdutil::Spawn(spawnId, pos, angleZ);
	Console_Print("spawned in %d ms", int(clock() - was));

	if (pImpl->newGameSound) sdutil::PlayWav("ui_startnewgame.wav");

	// We want to know our party
	pImpl->runParty.reqOnce = true;

	pImpl->tasks.push_back([this, mpch](World &w) {
		w.SetMyId(0, pImpl->GetMyId());

		vector<formid> markers;
		auto jMarkers = mpch["markers"];
		for (int i = 0; i < jMarkers.size(); ++i) {
			markers.push_back(FormInfo::fromJson(jMarkers[i]).toLocalId());
		}
		w.SetMarkers(markers);

		if (mpch.count("eq")) {
			auto eq = mpch.at("eq");
			if (eq.count("leftHand") && eq.count("rightHand")) { // expect Numbers here
				pImpl->myEq = eq;
				w.UpdateEquipment(pImpl->GetMyId(), pImpl->myEq);
			}
		}
		auto inv = ConvertJsonToInv(mpch.at("inv"));
		w.UpdateInventory(pImpl->GetMyId(), inv);

		if (mpch.count("look") == 0 || mpch.at("look").count("raceID") == 0) return w.ShowRaceMenu();
		w.UpdateLook(pImpl->GetMyId(), mpch.at("look"), "");
		pImpl->myLook = mpch.at("look");
	});
};

void Net::RunCallbacks() {
	// Run HTTP callbacks
	response_data *data = nullptr;
	while (pImpl->results->pop(data)) {
		auto cb = pImpl->request_callbacks[data->requestId];
		cb(data->response, data->err);
		delete data;
	}

	// Run timer callbacks
	decltype(pImpl->timers) newTimers;
	for (auto &tm : pImpl->timers) {
		if (clock() > tm.first) tm.second();
		else newTimers.push_back(tm);
	}
	pImpl->timers = std::move(newTimers);
}

bool Net::RunUDP() {
	if (pImpl->cellhostClient == nullptr) return false;
	string res = pImpl->cellhostClient->receive();

	if (clock() - pImpl->lastPing > 1000) {
		pImpl->lastPing = clock();
		const int r = rand();
		for (auto addr : pImpl->cellhostAddrs) this->Broadcast(pImpl->GetMyId(), CONTENT_MPCH_ALIVE, "");
	}

	if (!res.size()) return false;

	json j = nullptr;
	try {
		j = json::parse(res);
	}
	catch (std::exception &e) {
		// TODO
	}
	if (j == nullptr) return true;

	{
		// Calc ping
		auto client = pImpl->cellhostClient ? pImpl->cellhostClient->getClient() : nullptr;
		pImpl->ping = client ? client->getAvgPing() : 9999;

		for (auto &p : pImpl->sentMovements) {
			if (p.first.dump() == j.dump()) {
				p.first = json::object();
				pImpl->lagFull = clock() - p.second;
			}
		}

		// Calc out packet rate
		while (pImpl->sentPackets.size() > 15) pImpl->sentPackets.pop_front();
		int sum = 0;
		long long lastV = -1;
		for (auto v : pImpl->sentPackets) {
			if (lastV != -1) {
				sum += v - lastV;
			}
			lastV = v;
		}
		pImpl->outPacketRate = sum / (pImpl->sentPackets.size() - 1);
	}

	const int r = j.at("rand");
	if (pImpl->knownMessages.insert(r).second == false) {
		if (j.count("movement")) {
			const string name = j["name"].dump();
			Console_Print("Would loose movement from %s", name.data());
		}
	}
	pImpl->SetTimer(clock() + 500, [this, r] {
		pImpl->knownMessages.erase(r);
	});

	pImpl->tasks.push_back([this, j](World &world) {
		if (j.count("ownerId")) {
			const formid ownerId = j.at("ownerId");
			std::stringstream hexOwnerId; hexOwnerId << std::hex << ((ownerId >= 0xff000000) ? ownerId - 0xff000000 : ownerId);

			if (j.count("movement")) {
				if (j.count("uiavs")) {
					auto uiavs = j.at("uiavs");
					world.UpdateUIAVs(ownerId, { uiavs[0], uiavs[1], uiavs[2] });
				}
				formid hostId = -1;
				if (j.count("host")) {
					if (j["host"] == "you") {
						hostId = pImpl->GetMyId();
					}
				}
				world.UpdateMovement(ownerId, j.at("movement"), 0, hostId, 1);
				if (j.count("look") && j.count("name")) {
					world.UpdateLook(ownerId, j.at("look"), j.at("name"));
				}
				if (j.count("eq")) {
					world.UpdateEquipment(ownerId, j.at("eq"));
				}
			}
			else if (j.count("contentType") && j.count("content")) {
				auto contentType = j.at("contentType").get<int>();

				string content;
				auto charArray = j.at("content");
				auto n = charArray.size();
				for (int i = 0; i < n; ++i) {
					content.push_back((char)charArray[i].get<int>());
				}

				switch (contentType) {
				case CONTENT_CHAT: {
					try {
						auto jContent = SAFE_PARSE(content);
						string ch = jContent.at("channel");
						if (ch == "party") {
							try {
								if (pImpl->runParty.actualParty == nullptr || jContent["partyLeader"] != pImpl->runParty.actualParty.at("leader")) {
									return;
								}
							}
							catch (std::exception &e) {
								throw std::logic_error("Party chat error: " + string(e.what()));
							}
						}
						world.AddChatMessage(ch, jContent.at("message"), jContent.at("sender"), jContent.at("movement"));
					}
					catch (std::exception &e) {
						Console_Print("ChatMessage: %s", e.what());
					}
					break;
				}
				case CONTENT_PARTY_INVITE: {
					try {
						auto buttons = json::array();
						buttons.push_back({
							{ "id", BTNID_ACCEPT_PARTY_INVITE },{ "text", "$yes" }
							});
						buttons.push_back({
							{ "id", BTNID_CLOSE_DIALOG },{ "text", "$no" }
							});
						json data = json{
							{"_type", "party_invite"},
							{"sender", content},
							{"buttons", buttons}
						};
						if (ownerId == pImpl->GetMyId()) world.SetDialogData(data);
					}
					catch (std::exception &e) {
						Console_Print("PartyInvite: %s", e.what());
					}
					break;
				}
				case CONTENT_PARTY_REQONCE:
				{
					pImpl->runParty.reqOnce = true;
					break;
				}
				case CONTENT_CHEST_UPDATE:
				{
					auto ieRef = (formid)atoll(content.data());
					auto fi = FormInfo::Resolve(ieRef);
					pImpl->Req(http::verb::get, pImpl->GetTarget(), "/records/" + fi.espm + "/" + fi.idToHex(), {}, [this, ieRef](Response resp, boost::system::error_code ec) {
						if (ec || resp.result_int() >= 400) return Console_Print("achtung 2");
						auto mpch = SAFE_PARSE(resp.body());
						auto inv = ConvertJsonToInv(mpch.at("inv"));
						pImpl->tasks.push_back([this, inv, ieRef](World &w) {
							if (pImpl->touched.count(ieRef)) {
								w.UpdateInventory(ieRef, inv);
							}
						});
					});
					break;
				}
				case CONTENT_WHISPER_START:
				{
					auto jContent = SAFE_PARSE(content);
					formid targetId = jContent["targetId"];
					string channelId = jContent["channelId"];
					string targetName = jContent["targetName"];
					string ownerName = jContent["ownerName"];

					bool isOwner = ownerId == pImpl->GetMyId();
					bool isTarget = targetId == pImpl->GetMyId();
					if (isOwner || isTarget) {
						pImpl->SetImmediate([=] { // idkw but it doesn't work without be wrapped in SetImmediate
							pImpl->tasks.push_back([=](World &w) {
								ChatChannel ch;
								ch.focus = true;
								ch.id = channelId;
								ch.nameCaps = ch.name = isOwner ? targetName : ownerName;
								w.AddChannel(ch);
							});
						});
					}
					break;
				}
				case CONTENT_MPCH_ALIVE:
				{
					pImpl->SetImmediate([=] {
						pImpl->tasks.push_back([=](World &w) {
							w.MarkAlive(ownerId);
						});
					});
					break;
				}
				default:
					world.BroadcastIn(ownerId, contentType, content);
					break;
				}
			}
		}
	});
	return true;
}

void Net::RunParty() {
	auto &rpa = pImpl->runParty;
	if (rpa.actualParty == nullptr && !rpa.reqOnce) return;
	rpa.reqOnce = false;

	if (!rpa.reqInProgress && clock() - rpa.lastReq > 3000) {
		rpa.reqInProgress = true;
		pImpl->Req(http::verb::get, pImpl->GetTarget(), "/party/" + pImpl->GetToken(), {}, [this](Response resp, boost::system::error_code ec) {
			auto &rpa = pImpl->runParty;
			rpa.lastReq = clock();
			rpa.reqInProgress = false;

			if (resp.result_int() == 404) {
				rpa.actualParty = nullptr;
				pImpl->tasks.push_back([](World &w) { w.SetPartyData(nullptr); });
			}
			if (ec || resp.result_int() >= 400) return;
			auto pa = rpa.actualParty = SAFE_PARSE(resp.body());
			pImpl->tasks.push_back([pa](World &w) { w.SetPartyData(pa); });
		});
	}
}

void Net::RunImmediateCallbacks() {
	std::function<void()> *pCb = nullptr;
	while (pImpl->immediateCallbacks.pop(pCb)) {
		pCb->operator()();
		delete pCb;
	}
}

int Net::GetPing() const {
	return pImpl->ping;
}

int Net::GetLag() const {
	return pImpl->lagFull - pImpl->ping;
}

int Net::GetOutputPacketRate() const {
	return pImpl->outPacketRate;
}