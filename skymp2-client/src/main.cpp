#include "stdafx.h"
#include <filesystem>

namespace fs = std::filesystem;

#include "directx\DirectXHook.h"
#include "dinput\DInput.hpp"
#include "gui/Localization.h"

#include "services\WorldCleaner.h"
#include "services\GUI.h"
#include "services\Controls.h"
#include "services\World.h"

#include "..\skse\Hooks_SaveLoad.h"
#include "..\skse\Hooks_Gameplay.h"
#include "..\skse\PapyrusForm.h"

#include "components/Inventory.h"

#include "sdutil\Spawn.h"
#include "sdutil\Esp.h"

#include "TaskQueue.h"

#include "Net.h"

#pragma comment(lib, "winmm")

#define VERSION_INFO "_version-info.skymp2tmp"
#define PROGRESS "_progress.skymp2tmp"

string encryptDecrypt(string toEncrypt) {
	return toEncrypt;
}

void skymp_putenv(const char *str) {
	int error = _putenv(str);
	if (error != 0) throw std::runtime_error("_putenv returned " + std::to_string(error) + " with parameter " + str);
}

void pcall(std::function<void()> fn, const char *fnName) {
	try {
		fn();
	}
	catch (std::exception &e) {
		auto trace = get_trace(e);
		char buffer[8192];
		sprintf_s(buffer, "Unhandled exception in pcall('%s') \nType: %s\nWhat: %s%s\n", fnName, typeid(e).name(), e.what(), trace.data());
		MessageBoxA(GetForegroundWindow(), buffer, "Unhandled exception", MB_ICONERROR);
		ExitProcess(-1);
	}
}

enum {
	INVENTORY_DELAY = 333
};

extern "C" {
	__declspec(dllexport) void SKYMP2_Print(const char *text) {
		Console_Print("Frida: %s", text);
	};
}

volatile bool g_curs = false;

std::shared_ptr<Text> g_connectionState;
string g_name;
volatile int g_progress = 0; // TODO: need sync access??

WorldCleaner g_wc;
volatile bool g_debug = false;
std::unique_ptr<World> g_world;
clock_t g_voidMainEnter = 0;

std::unique_ptr<GUI> g_gui;
std::shared_ptr<Controls> g_controls;
std::unique_ptr<World::INet> g_net;

#include "components/PersistentRef.h"

string GetVersionFull() {
	string version, buildId;
	std::ifstream(SKYMP2_FILES_DIR "/version.txt") >> version;
	std::ifstream(SKYMP2_FILES_DIR "/build-id.txt") >> buildId;
	return version + '-' + buildId;
}

void SetVoidMain(bool val) {
	std::ofstream(SKYMP2_FILES_DIR "/voidmain.skymp2tmp") << int(val);
}

void SwitchToEnglish() {
	HWND fore = //GetForegroundWindow(); // Every window has it's own current language (keyboard layout)
		FindWindowA("Skyrim", "Skyrim");
	DWORD tpid = GetWindowThreadProcessId(fore, 0);
	HKL hKL = GetKeyboardLayout(tpid);
	auto languageCode = LOWORD(hKL);
	auto english = 1033;
	if (languageCode != english)
	{
		keybd_event(VK_LSHIFT, 0x2a, NULL, NULL);
		keybd_event(VK_LCONTROL, 0x1d, NULL, NULL);

		keybd_event(VK_LSHIFT, 0x2a, KEYEVENTF_KEYUP, NULL);
		keybd_event(VK_LCONTROL, 0x1d, KEYEVENTF_KEYUP, NULL);

		keybd_event(VK_LSHIFT, 0x2a, NULL, NULL);
		keybd_event(VK_MENU, 0x38, NULL, NULL);

		keybd_event(VK_LSHIFT, 0x2a, KEYEVENTF_KEYUP, NULL);
		keybd_event(VK_MENU, 0x38, KEYEVENTF_KEYUP, NULL);
	}
}

const char *AUTH_TXT(int saveId) {
	thread_local char buf[MAX_PATH];
	GetTempPath(sizeof buf, buf);
	sprintf_s(buf, "%s/jjdu%d%s", buf, saveId, "sjqb1mxg.d4s");
	return buf;
}

enum {
	kAuthSaveId_RememberMe = 0,
	kAuthSaveId_CrashDetect = 1
};

void SaveAuth(const AuthData &src, int saveId) {
	auto f = std::ofstream(AUTH_TXT(saveId));
	if (!src.rememberMe) f << "";
	else f << json({ {"password", src.password}, {"name", src.name}, {"rememberMe", (int)src.rememberMe} }).dump();
}

AuthData LoadAuth(int saveId) {
	std::ifstream t(AUTH_TXT(saveId));
	string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());

	AuthData a;
	try {
		auto auth = SAFE_PARSE(str);
		a.password = auth["password"];
		a.name = auth["name"];	
		a.rememberMe = auth["rememberMe"].get<int>();
	}
	catch (...) {
		return {};
	}
	return a;
}

void UpdateFastTravelControls() {
	static void *g_cellWas = nullptr;
	void *cell = (*g_thePlayer)->parentCell;
	if (g_cellWas != cell) {
		g_controls->SetEnabled(Controls::FastTravel, false, "Core");
		g_cellWas = cell;
	}
}

bool RequiresSkymp() {
	bool hasEsp = sdutil::Requires("SkyMP.esp");
	if (!hasEsp) {
		MessageBoxA(0, LW_BAD_ESP_INSTALL, LW_OOPS, MB_ICONERROR);
	}
	return hasEsp;
}

__declspec(dllexport) void main()
{
	if (!RequiresSkymp()) return;
	if (!sdutil::Requires("SkyUI.esp")) {
		// ...
	}

	SetVoidMain(true);
	g_voidMainEnter = clock();

	PersistentRef::SetOnActivate([](auto evn) {
		if (g_world) g_world->OnActivate(evn);
	});

	static std::pair<userid, formid> g_myId = { -1,0 };

	g_wc.Clear();
	g_world.reset(new World(&g_wc, g_name, g_gui.get()));
	g_world->SetMyId(g_myId.first, g_myId.second);

	// Disable camera
	g_controls->SetEnabled(Controls::CamSwitch, false, "spawning");
	sd::ForceFirstPerson();
	auto ctrls = g_controls;
	std::thread([ctrls] {
		Sleep(2000);
		ctrls->SetEnabled(Controls::CamSwitch, true, "spawning");
	}).detach();

	g_controls->RequestApply();

	clock_t worldReset = 0;
	static auto fsConsole = new BSFixedString("Console");
	while (1) {

		static clock_t c = clock();
		if (clock() - c > 200 && c) {
			c = 0;
			papyrusGame::SaveGame(0, "skymp");
			sd::Wait(1000);
		}
		static clock_t c1 = clock();
		if (clock() - c1 > 2750 && c1) {
			c1 = 0;
			g_controls->SetEnabled(Controls::SaveGame, false, "Core");
		}

		try {
			g_wc.Update();
			g_controls->Update();

			bool consoleOpen = MenuManager::GetSingleton()->IsMenuOpen(fsConsole);
			// Show we are here for crash-detect
			static clock_t g_last = 0;
			if (clock() - g_last > (consoleOpen ? 100 : 1000)) {
				g_last = clock();
				char path[MAX_PATH] = { 0 };
				if (path[0] == 0) {
					GetTempPath(MAX_PATH, path);
					sprintf_s(path, "%s/%s", path, "SkympTick.tmp");
				}
				std::ofstream(path) << (consoleOpen ? 0 : time(0)); // Ignore crashes in console (qqq)
			}

			bool pressed = sd::GetKeyPressed(0x39) && sd::GetKeyPressed('O') && sd::GetKeyPressed('L');
			static bool was = false;
			if (pressed != was) {
				was = pressed;
				if (pressed) g_debug = !g_debug;
			}

			if (sd::IsDead(*g_thePlayer)) {
				if (!worldReset) worldReset = clock() + 3000;
			}
			if (worldReset && clock() > worldReset) g_world.reset();
			const bool gameSaved = !c;
			if (g_world && gameSaved) {
				g_myId = g_world->GetMyIDs();
				g_world->Update(g_debug, *g_net);
				UpdateFastTravelControls();
			}
		}
		catch (std::exception &e) {

			auto trace = get_trace(e);
			sd::PrintNote("Unhandled exception (see console)");
			Console_Print("Unhandled exception\nType: %s\nWhat: %s%s\n", typeid(e).name(), e.what(), trace.data());

		}
		catch (...) {
			sd::PrintNote("Unhandled exception: Unknown");
			throw;
		}

		static auto fs = new BSFixedString("InventoryMenu");
		static auto fs2 = new BSFixedString("ContainerMenu");
		bool inv = MenuManager::GetSingleton()->IsMenuOpen(fs) || MenuManager::GetSingleton()->IsMenuOpen(fs2);

		sd::Wait(inv ? INVENTORY_DELAY : 0);
	}
}

constexpr auto SHOW_LOGIN_TXT = "__showlogin.txt";

void ProcessAuthResult(AuthData &lastHandshake, bool &authInProgress) {
	auto res = g_gui->GetAuthResult();
	if (res) {
		if (res->type == AuthData::Confirm) {
			if (res->forceRetryConfirm) {
				// Request resend e-mail
				g_net->RetryConfirm(g_name);
			}
			else {
				// Request confirmation
				g_net->Confirm(g_name, res->confirmCode);
			}
		}
		else if (res->type == AuthData::Login && res->forceResetPassword) {
			res->type = AuthData::ResetPassword_Email;
			g_gui->ShowAuth(*res);
		}
		else if (res->type == AuthData::ResetPassword_Email) {
			g_net->StartResetPassword(res->email);
			res->systemError = L_SEARCH + (' ' + res->email);
			g_gui->ShowAuth(*res);
		}
		else if (res->type == AuthData::ResetPassword_NewPass) {
			g_net->FinishResetPassword(res->confirmCode, res->password);
			res->systemError = "...";
			g_gui->ShowAuth(*res);
		}
		else {
			if (res->name.size() > 0) g_name = res->name;
			g_net->Handshake(*res, GetVersionFull());
			lastHandshake = *res;
			authInProgress = false;
		}
		SaveAuth(*res, kAuthSaveId_RememberMe);

		auto crashDetectAuth = *res;
		crashDetectAuth.rememberMe = true;
		SaveAuth(crashDetectAuth, kAuthSaveId_CrashDetect);
	}
}

void KeepCursorOn(bool authInProgress) {
	// Keep cursor while auth in progress
	if (authInProgress) {
		g_curs = true;
		g_gui->SetCursor(true);
	}
}

void ProcessFinishResetPass() {
	// Process result of FINISH pass reset
	if (int res = g_net->GetFinishResetPasswordResult(); res != -1) {
		AuthData a;
		a.type = AuthData::Login;
		if (res == 204) {
			a.systemError = L_PASSWORD_CHANGED;
			g_gui->ShowAuth(a);
		}
		else {
			a.systemError = L_ERROR + (' ' + std::to_string(res));
			a.type = AuthData::ResetPassword_NewPass;
			g_gui->ShowAuth(a);
		}
		g_gui->ShowAuth(a);
	}
}

void ProcessStartResetPass() {
	// Process result of START pass reset
	if (int res = g_net->GetStartResetPasswordResult(); res != -1) {
		AuthData a;
		a.type = AuthData::ResetPassword_Email;
		if (res == 404) {
			a.systemError = L_CHARACTER_NOT_FOUND;
			g_gui->ShowAuth(a);
		}
		else if (res == 204) {
			a.type = AuthData::ResetPassword_NewPass;
			g_gui->ShowAuth(a);
		}
		else {
			a.systemError = L_ERROR + (' ' + std::to_string(res));
			g_gui->ShowAuth(a);
		}
	}
}

void ProcessRetryConfirm() {
	// Process retry confirm result
	if (int res = g_net->GetRetryConfirmResult(); res != -1) {
		AuthData a;
		a.type = AuthData::Confirm;
		if (res == 204) {
			a.systemError = L_EMAIL_RESENT;
			PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_maprollover_flyout_01.wav", 0, SND_FILENAME | SND_ASYNC);
		}
		else {
			a.systemError = L_ERROR + ' ' + std::to_string(res);
			PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_activatefail.wav", 0, SND_FILENAME | SND_ASYNC);
		}
		g_gui->ShowAuth(a);
	}
}

void ProcessConfirm(AuthData &lastHandshake) {
	// Process confirm result
	if (int res = g_net->GetConfirmResult(); res != -1) {
		if (res >= 200 && res <= 299) {
			lastHandshake.type = AuthData::Login;
			g_net->Handshake(lastHandshake, GetVersionFull());
		}
		else if (res == 403) {
			AuthData a;
			a.type = AuthData::Confirm;
			a.systemError = L_INCORRECT_CONFIRM_CODE;
			g_gui->ShowAuth(a);
			PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_activatefail.wav", 0, SND_FILENAME | SND_ASYNC);
		}
		else {
			AuthData a;
			a.type = AuthData::Login;
			a.systemError = L_ERROR + ' ' + std::to_string(res);
			g_gui->ShowAuth(a);
			PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_activatefail.wav", 0, SND_FILENAME | SND_ASYNC);
		}
	}
}

void ProcessHandshake() {
// Process handshake result
	if (auto res = g_net->GetHandshakeResult(); res) {
		auto str = std::to_string(res->responseStatus) + " " + res->responseBody;
		//g_connectionState->SetText(str);
		res->finished = false;
		if (res->responseStatus == 0) {
			auto a = *res;
			a.systemError = L_SERVER_IS_NOT_RESPONDING;
			return g_gui->ShowAuth(a);
		}
		if (res->responseStatus == 201 || res->responseStatus == 428) {
			AuthData confirm;
			confirm.type = AuthData::Confirm;
			g_gui->ShowAuth(confirm);
			return;
		}
		if (res->responseStatus >= 400) {
			res->systemError = str;
			if (res->responseStatus == 404) res->systemError = L_CHARACTER_NOT_FOUND;
			else if (res->responseStatus == 403) res->systemError = L_BAD_PASSWORD;
			else if (res->responseStatus == 409) res->systemError = L_ALREADY_EXIST;
			else if (res->responseStatus == 402) res->systemError = L_BAD_INVITE;
			else if (res->responseStatus == 411) res->systemError = L_BAD_LENGTH;
			else if (res->responseStatus == 426) {
				res->systemError = L_NEED_UPDATE;
				std::thread([] {
					Sleep(1000);
					char buf[MAX_PATH] = { 0 };
					if (!GetTempPathA(MAX_PATH, buf)) throw std::logic_error("GetTempPathA failed");
					sprintf_s(buf, "%s/%s", buf, "zzzrep_6.bin");
					if (fs::exists(buf)) fs::remove(buf);
				}).detach();
			}
			g_gui->ShowAuth(*res);
			return;
		}
		g_gui->HideAuth();

	}
}

int g_percentage = 0;

bool FinishUpdate() {
	struct Deleter {
		Deleter() {
			if (std::ifstream("SkyMP.exe.tmp").good()) {
				good = true;
				fs::remove("SkyMP.exe");
				fs::rename("SkyMP.exe.tmp", "SkyMP.exe");
			}
			if (std::ifstream("ukusil.tmp").good()) {
				good = true;
				fs::remove("ukusil.tmp");
			}
		}
		bool good = false;
	};
	static Deleter deleter;

	skymp_putenv(deleter.good ? "SKYMP2_LOADING=1" : "SKYMP2_LOADING=0");
	string s = "LOADING_PERCENTAGE=" + std::to_string(g_percentage);
	skymp_putenv(s.data());
	return deleter.good;
}

bool RequiresSkympLauncher() {
	static bool *g_res = nullptr;
	if (g_res == nullptr) {
		string timeStr;
		std::ifstream("_skymp_launch_moment_.txt") >> timeStr;
		auto ll = atoll(timeStr.data());
		if (ll == 108) {
			std::ofstream("_skymp_launch_moment_.txt") << "septim";
			g_res = new bool(true);
		}
		else g_res = new bool(time(0) - ll <= 10);
	}
	return *g_res;
}

void LoadFrida() {
	std::thread([] {
		pcall([] {
			auto skymp2hooks = LoadLibraryA(SKYMP2_HOOKS_DLL);
			if (!skymp2hooks) throw std::runtime_error("LoadLibrary " SKYMP2_HOOKS_DLL " failed");
			auto setUpHooks = GetProcAddress(skymp2hooks, "SetUpHooks");
			setUpHooks();
		}, "LoadFrida");
	}).detach();
}

// https://stackoverflow.com/questions/2414828/get-path-to-my-documents
// To prevent using F9 
#pragma comment(lib, "shell32.lib")
#include <shlobj.h>
void RenameQuickSave() {
	PWSTR   ppszPath;    // variable to receive the path memory block pointer.
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_Documents, 0, NULL, &ppszPath);
	std::wstring myPath;
	if (SUCCEEDED(hr)) {
		myPath = ppszPath;      // make a local copy of the path
		myPath += L"\\My Games\\Skyrim\\Saves\\quicksave.ess";
		if (fs::exists(myPath)) {
			fs::rename(myPath, myPath + L".ess");
			Console_Print("renamed");
		}
	}
	CoTaskMemFree(ppszPath);    // free up the path memory block
}

void TickBlockConsole() {
	// Block console (O + L to unblock)
	static auto fsConsole = new BSFixedString("Console");
	static bool allowConsole = false;
	if (sd::GetKeyPressed('O') && sd::GetKeyPressed('L')) {
		allowConsole = true;
	}
	bool open = MenuManager::GetSingleton()->IsMenuOpen(fsConsole);
	if (!allowConsole && open) {
		UIManager::GetSingleton()->CloseMenu(fsConsole);
	}
}

void helper_thread_main() {
	// Delete temporary files from Skyrim folder
	for (auto &p : fs::directory_iterator(".")) {
		if (p.is_directory()) continue;
		if (p.path().string().find(".skymp2tmp") != string::npos) {
			fs::remove(p);
		}
		else if (p.path().string().find("CrashDetect") != string::npos) {
			try {
				fs::remove(p);
			}
			catch (...) {
				// CrashDetect*.exe may be executed now
			}
		}
	}

	RenameQuickSave();

	skymp_putenv("SKYMP2_LOADING=0");

	static auto fsMainMenu = new BSFixedString("Main Menu");
	static auto fsCursorMenu = new BSFixedString("Cursor Menu");

	if (!RequiresSkymp()) return;
	if (!RequiresSkympLauncher()) {
		MessageBoxA(0, "Error: Lox razrab", "WTF", MB_ICONERROR);
		return;
	}

	SetVoidMain(false);
	SwitchToEnglish();

	g_controls.reset(new Controls);
	//g_controls->SetEnabled(Controls::SaveGame, false, "Core");
	g_controls->SetEnabled(Controls::JournalTabs, false, "Core");

	json cfg;
	{
		std::ifstream t(SKYMP2_CONFIG);

		std::string str((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());
		if (str.empty()) throw std::logic_error(SKYMP2_CONFIG " not found");
		cfg = SAFE_PARSE(str);

		std::string ipAndPort = cfg["server-address"], ip, port;
		bool readIp = true;
		for (auto ch : ipAndPort) {
			if (ch == ':') readIp = false;
			else (readIp ? ip : port) += ch;
		}
		cfg["_server-ip"] = ip;
		cfg["_server-port"] = atoi(port.data());
	}

	assert(cfg.is_object());
	assert(cfg.count("server-address"));
	assert(cfg.count("password"));
	g_net.reset(new Net(
		cfg.at("server-address").get<string>()
	));

	volatile auto &addr1 = (size_t &)g_pIDirect3D9;
	volatile auto &addr2 = (size_t &)g_pIDirect3DDevice9;
	volatile auto &addr3 = (size_t &)TheIInputHook;
	bool wait = true;
	while (wait) {
		wait = !addr1 || !addr2 || !addr3;
		Sleep(1000);
	}

	g_gui.reset(new GUI(g_controls.get()));
	AuthData firstAuth = LoadAuth(kAuthSaveId_RememberMe);
	{
		if (!fs::exists(SHOW_LOGIN_TXT)) firstAuth.type = AuthData::Register;
		std::ofstream(SHOW_LOGIN_TXT) << 1;
	}


	g_connectionState = (g_gui->CreateText("Connecting", { 10.f, 10.f }));

	LoadFrida();

	while (1) {
		if (MenuManager::GetSingleton()->IsMenuOpen(fsMainMenu)) break;
		Sleep(1000);
	}

	Sleep(1000); // Fix crash

	auto ui = UIManager::GetSingleton();
	bool authInProgress = true;
	AuthData lastHandshake;
	while (1) {
		if (sd::GetKeyPressed(VK_F4) && sd::GetKeyPressed(0x35)) {
			std::thread([] {
				int *p = 0;
				while (1) {
					p++;
					*p = 0;
				}
			}).detach();
		}

		static clock_t lastAdd = 0;
		static int delay = 0;
		if (delay != -1 && clock() - lastAdd > delay) {
			lastAdd = clock();
			g_percentage += rand() % 5;
			g_percentage = min(g_percentage, 200);
			delay = rand() % 133;
			if (!FinishUpdate()) {
				delay = -1;
			}
		}

		bool upToDate = (delay == -1 || g_percentage >= 120);
		if (upToDate && !firstAuth.finished) {
			AuthData cacheAuth = LoadAuth(kAuthSaveId_CrashDetect);
			// Skip Auth if CrashDetect cache found && '_crashed.tmp' exist
			if (cacheAuth.password.size() && fs::exists("_crashed.tmp")) {
				fs::remove("_crashed.tmp");
				g_net->Handshake(cacheAuth, GetVersionFull(), true);
				authInProgress = false;
				g_name = cacheAuth.name;
			}
			else if (cacheAuth.password.size() && fs::exists("_updated.tmp")) {
				fs::remove("_updated.tmp");
				g_net->Handshake(cacheAuth, GetVersionFull(), false);
				authInProgress = false;
				g_name = cacheAuth.name;
			}
			else {
				g_gui->ShowAuth(firstAuth);
			}
			firstAuth.finished = true;
		}

		ProcessAuthResult(lastHandshake, authInProgress);
		KeepCursorOn(authInProgress);
		ProcessFinishResetPass();
		ProcessStartResetPass();
		ProcessRetryConfirm();
		ProcessConfirm(lastHandshake);
		ProcessHandshake();
		TickBlockConsole();

		Sleep(10);
		g_net->FlushToWorld(g_world.get()); // ok for 'g_world' to be nullptr here

		if (static auto fs = new BSFixedString("Loading Menu"); MenuManager::GetSingleton()->IsMenuOpen(fs)) {
			g_curs = false;
			g_gui->SetCursor(false);
		}
		else {
			// Moved from void main():
			g_connectionState->SetText(""); // Remove connected label
			g_connectionState->SetPreferredTextH(0); // must be invisible during gameplay
		}

		// Black screen on spawn
		{
			static bool mainMenuWasOpen = false;
			static bool attached = false; // TODO: write 'false' to 'attached' on PC death
			if (MenuManager::GetSingleton()->IsMenuOpen(fsMainMenu) == false)
			{
				if (mainMenuWasOpen)
				{
					if (!attached)
					{
						sd::ExecuteConsoleCommand("player.aps AAAMPFade Main", 0);
						/*
						Scriptname AAAMPFade extends ObjectReference

						function Main()
							Game.FadeOutGame(false, true, 2, 1.0)
						endFunction
						*/
						attached = true;
					}
				}
			}
			else {
				mainMenuWasOpen = true;
			}
		}

		if (g_curs && MenuManager::GetSingleton()->IsMenuOpen(fsMainMenu)) {
			mouse_event(NULL, 1000000, 1000000, NULL, NULL);
			if (MenuManager::GetSingleton()->IsMenuOpen(fsCursorMenu)) {
				ui->CloseMenu(fsCursorMenu);
			}
		}

		static bool pressedWas = false;
		if (bool pressed = sd::GetKeyPressed(VK_F6); pressed != pressedWas) {
			pressedWas = pressed;
			if (pressed) {
				static uint32_t i = 0;
				bool c = bool(++i % 2);
				g_gui->SetCursor(c);
				g_curs = c;
			}
		}
	}
}

// https://msdn.microsoft.com/ru-ru/library/kdzttdcb.aspx
void __cdecl helper_thread(void *) {
	if (!RequiresSkymp()) return;
	pcall(helper_thread_main, "helper_thread");
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD fdwReason, LPVOID lpReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
	{
		try {
			if (!RequiresSkympLauncher()) {
				STARTUPINFO cif;
				ZeroMemory(&cif, sizeof(STARTUPINFO));
				PROCESS_INFORMATION pi;
				std::ofstream("SkyMP.exe.inf") << "1";
				if (!CreateProcessA("SkyMP.exe", NULL,
					NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &cif, &pi) == TRUE)
				{
				}
				ExitProcess(-1);
				return TRUE;
			}
			{
				STARTUPINFO cif;
				ZeroMemory(&cif, sizeof(STARTUPINFO));
				PROCESS_INFORMATION pi;
				string randName = "CrashDetect" + std::to_string(rand()) + std::to_string(time(0)) + std::to_string(clock()) + ".exe";
				CopyFile("SkyMP.exe", randName.data(), 0);
				string cmdLine = randName + " -crashdetect";
				if (!CreateProcessA(randName.data(), cmdLine.data(),
					NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &cif, &pi) == TRUE)
				{
				}
			}

			HookDirect3D9();
			HookDirectInput();

			Hooks_SaveLoad_Commit();
			Hooks_Gameplay_Commit();

			if (_beginthread(helper_thread, 1024 * 1024, nullptr) == -1L) {
				throw std::logic_error("_beginthread(...) failed");
			}
			sd::DragonPluginInit(hModule);
		}
		catch (std::exception &e) {
			MessageBoxA(0, e.what(), "Unhandled exception in DllMain", MB_ICONERROR);
			ExitProcess(-1);
		}
		break;
	}
	case DLL_PROCESS_DETACH:
	{
		break;
	}
	}
	return TRUE;
}
