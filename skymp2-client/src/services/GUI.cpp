#include "../stdafx.h"
#include "GUI.h"

#include "../libs/imgui.h"
#include "../libs/imgui/imgui_internal.h"
#include "../gui/ImguiUtils.h"
#include "../TaskQueue.h"
#include "../gui/imgui_multicolor.h"
#include "../sdutil/PlayWav.h"

#include "../directx/DirectXHook.h"
#include "../dinput/DInput.hpp"

#include "../gui/Localization.h"

void ExitSkymp(int code) {
	auto h = OpenProcess(PROCESS_TERMINATE, false, GetCurrentProcessId());
	TerminateProcess(h, code);
}

#define ImSin std::sin
#define ImCos std::cos

int g_spinnerFull = 0;

// https://github.com/ocornut/imgui/issues/1901
namespace ImGui {

	void BufferingBar(const char* label, float value, const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col) {
		ImGuiWindow* window = GetCurrentWindow();
		if (window->SkipItems)
			return;

		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);

		ImVec2 pos = window->DC.CursorPos;
		ImVec2 size = size_arg;
		size.x -= style.FramePadding.x * 2;

		const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
		ItemSize(bb, style.FramePadding.y);
		if (!ItemAdd(bb, id))
			return;

		// Render
		const float circleStart = size.x * 0.7f;
		const float circleEnd = size.x;
		const float circleWidth = circleEnd - circleStart;

		window->DrawList->AddRectFilled(bb.Min, ImVec2(pos.x + circleStart, bb.Max.y), bg_col);
		window->DrawList->AddRectFilled(bb.Min, ImVec2(pos.x + circleStart * value, bb.Max.y), fg_col);

		const float t = g.Time;
		const float r = size.y / 2;
		const float speed = 1.5f;

		const float a = speed * 0;
		const float b = speed * 0.333f;
		const float c = speed * 0.666f;

		const float o1 = (circleWidth + r) * (t + a - speed * (int)((t + a) / speed)) / speed;
		const float o2 = (circleWidth + r) * (t + b - speed * (int)((t + b) / speed)) / speed;
		const float o3 = (circleWidth + r) * (t + c - speed * (int)((t + c) / speed)) / speed;

		window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o1, bb.Min.y + r), r, bg_col);
		window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o2, bb.Min.y + r), r, bg_col);
		window->DrawList->AddCircleFilled(ImVec2(pos.x + circleEnd - o3, bb.Min.y + r), r, bg_col);
	}

	void Spinner(const char* label, float radius, int thickness, const ImU32& color) {
		ImGuiWindow* window = GetCurrentWindow();
		if (window->SkipItems)
			return;

		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);

		ImVec2 pos = window->DC.CursorPos;
		ImVec2 size((radius) * 2, (radius + style.FramePadding.y) * 2);

		const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
		ItemSize(bb, style.FramePadding.y);
		if (!ItemAdd(bb, id))
			return;

		// Render
		window->DrawList->PathClear();

		int num_segments = 30;
		int start = abs(ImSin(g.Time*1.8f)*(num_segments - 5));

		float a_minRaw = IM_PI * 2.0f * ((float)start) / (float)num_segments;
		float a_max = IM_PI * 2.0f * ((float)num_segments - 3) / (float)num_segments;

		static std::map<string, float> aminTrans;

		aminTrans[label] += 0.2 * (a_minRaw - aminTrans[label]);

		if (g_spinnerFull) {
			aminTrans[label] = -IM_PI * 0.5f;
			a_max = IM_PI * 2.0f;
		}

		auto a_min = aminTrans[label];

		const ImVec2 centre = ImVec2(pos.x + radius, pos.y + radius + style.FramePadding.y);

		for (int i = 0; i < num_segments; i++) {
			const float a = a_min + ((float)i / (float)num_segments) * (a_max - a_min);
			window->DrawList->PathLineTo(ImVec2(centre.x + ImCos(a + g.Time * 8) * radius,
				centre.y + ImSin(a + g.Time * 8) * radius));
		}

		window->DrawList->PathStroke(color, false, thickness);
	}

}

enum {
	INTERACTION_LABEL_WIDTH = 230,
	LABEL_Y_OFFSET = -38,
	IMAGE_SPACE_MODIFIER = 0x434bb
};

volatile bool g_isPartyInteractionMenu = false;

const float BTN_TEXT_OPA_MIN = 0.0;
const float BTN_TEXT_OPA_MAX = 1;

std::recursive_mutex g_cursorPosM;
ImVec2 g_cursorPos;
void *g_fontsNickname = nullptr;
std::vector<std::shared_ptr<Text>> globalHolder;
ImVec2 g_labelOverCalcBase; // TODO: thread sync

static std::unordered_map<void *, float> g_cacheOpacity;

struct InteractionMenu
{
	std::vector<std::shared_ptr<Text>> texts;
};

struct GUI::Impl
{
	Controls *controls = nullptr;
	std::atomic<bool> visibleCursor;

	// temporary
	clock_t lastUpd = 0;
	bool menuOpen = false;


	std::set<Text *> texts;
	std::unique_ptr<InteractionMenu> interactionMenu;
	Text *chat = nullptr;
	Text *party = nullptr;
	Text *dialog = nullptr;
	volatile bool dialogPresent = 0;
	std::recursive_mutex mutex;
	std::unique_ptr<AuthData> auth, authOut;
	bool authWasActive = false;

	// ScriptDragon thread only:
	json lastDia = nullptr;
};

GUI::GUI(Controls *controls) {
	pImpl.reset(new Impl);
	pImpl->controls = controls;
}

GUI::~GUI() {
}

class Window
{
public:
	Window(std::string name, ImGuiWindowFlags flags, bool cursor, std::function<void()> vnutri) {
		if (!cursor) {
			flags |= ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoCollapse;
		}
		flags |= ImGuiWindowFlags_NoNavInputs | ImGuiWindowFlags_NoNavFocus;
		ImGui::Begin(name.data(), nullptr, flags);

		vnutri();
	}
	~Window() {
		ImGui::End();
	}
};

void GUI::WorldPointToScreen(const NiPoint3 &pos3d, void *imvec2Out) {
	ImVec2 p;
	float screenZ;
	auto pcam = PlayerCamera::GetSingleton();
	pcam->WorldPtToScreenPt3(pos3d, p.x, p.y, screenZ);
	if (screenZ < 0) p = ImVec2{ -10000000.f,-10000000.f };
	else {
		p.x *= this->GetWindowWidth();
		p.y = (1 - p.y) * this->GetWindowHeight();
	}
	*(ImVec2 *)(imvec2Out) = p;
}


static volatile bool g_getKeyPressed[256];

void UpdatePressedKeys() {
	for (int i = 0; i != 256; ++i) {
		g_getKeyPressed[i] = sd::GetKeyPressed(i);
	}
}

bool GUI::GetKeyPressed(int vkCode) {
	assert(vkCode > -1);
	assert(vkCode < 256);
	return g_getKeyPressed[vkCode];
}

TaskQueue g_bloorTaskQ;
TaskQueue g_SetScrollHereQ;
TaskQueue g_addTaskQ;

void Link(std::function<void()> f, float *pColPercent) {
	assert(pColPercent);
	auto &colPercent = *pColPercent;

	const ImVec4 _white = { 1,1,1,1 };
	const ImVec4 _33ccff = { 0x33 / 256.f,0xcc / 256.f,0xff / 256.f,1 };

	colPercent += (colPercent >= 100) ? 0.11f : -0.09f;
	if (colPercent < 0.f) colPercent = 0.f;
	else if (colPercent > 0.99f) colPercent = 0.99f;

	auto p = colPercent - (int)colPercent;
	for (auto c : { ImGuiCol_Button, ImGuiCol_ButtonActive, ImGuiCol_ButtonHovered }) ImGui::PushStyleColor(c, { 0,0,0,0 });
	ImGui::PushStyleColor(ImGuiCol_Text, {
		p * _33ccff.x + (1 - p) * _white.x,
		p * _33ccff.y + (1 - p) * _white.y,
		p * _33ccff.z + (1 - p) * _white.z,
		p * _33ccff.w + (1 - p) * _white.w
		});
	f();

	if (ImGui::IsItemHovered()) colPercent = p + 100;
	else colPercent = p;

	for (int i = 0; i != 4; ++i) ImGui::PopStyleColor();
}

void WhiteBorder(std::function<void()> f) {
	ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_FrameBorderSize, 1);
	ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowBorderSize, 0);
	ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_ChildBorderSize, 0);
	ImGui::PushStyleColor(ImGuiCol_Border, { 1,1,1,0.8f });
	ImGui::PushStyleColor(ImGuiCol_FrameBg, { 0,0,0,0 });
	ImGui::PushStyleColor(ImGuiCol_FrameBgActive, { 0,0,0,0 });
	ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, { 0,0,0,0 });
	f();
	ImGui::PopStyleColor(4);
	ImGui::PopStyleVar(3);
}

void TextHeight(int h, void *fonts, std::function<void()> f) {
	auto fontsNick = *(std::unordered_map<int, ImFont*> *)fonts;
	ImGui::PushFont(fontsNick[h]);
	f();
	ImGui::PopFont();
}

void ItemWidth(int w, std::function<void()> f) {
	ImGui::PushItemWidth(w);
	f();
	ImGui::PopItemWidth();
}

void NiceButton(std::function<void()> f, float *pOpa, bool vkRet) {
	assert(pOpa);
	auto &opa = *pOpa;

	if (vkRet) opa = 1.f;

	for (auto col : { ImGuiCol_Button, ImGuiCol_ButtonHovered, ImGuiCol_ButtonActive }) ImGui::PushStyleColor(col, { opa, opa, opa, 1 });
	for (auto col : { ImGuiCol_Text }) ImGui::PushStyleColor(col, { 1 - opa, 1 - opa, 1 - opa, 1 });
	f();
	ImGui::PopStyleColor(4);
	
	ImGui::IsItemHovered() ? opa += 0.1 : opa -= 0.1;
	if (opa < 0) opa = 0.f;
	else if (opa > 1) opa = 1.f;
}

class GUI::Row
{
public:

	enum class Type {
		Input, Any
	};

	Row() = default;
	Row(string text_, int inputFlags_, char *buf_) :
		text(text_), 
		inputFlags(inputFlags_ | ImGuiInputTextFlags_CallbackAlways /*for ctrl+c ctrl+v*/),
		inputLabel("###" + text_)
	{
		defaultBuf[0] = 0;
		buf = buf_ ? buf_ : defaultBuf;
	}

	Row(std::function<void()> render_) :
		type(Type::Any),
		render(render_)
	{
	}

	static constexpr size_t bufSize = 64;

	string text;
	int inputFlags = 0;
	char *buf;
	bool taskFocus = false;
	bool wasActive = false;
	const string inputLabel;
	const Type type = Type::Input;
	const std::function<void()> render = nullptr;

private:
	char defaultBuf[bufSize];
};

int cb(ImGuiTextEditCallbackData *data);
int cbb(ImGuiTextEditCallbackData *data) {
	cb(data);
	for (int i = 0; i < data->BufTextLen; ++i) {
		if (data->Buf[i] == '"') {
			data->Buf[i] = '@';
			data->BufDirty = 1;
		}
		if (data->Buf[i] == '<' || data->Buf[i] == '>') {
			data->Buf[i] = '.';
			data->BufDirty = 1;
		}
	}
	return 0;
}

const char *GetNiceButtonText(AuthData::Type t) {
	if (t == AuthData::Login) return L_ENTER_BUTTON;
	if (t == AuthData::Register) return L_REGISTER_BUTTON;
	if (t == AuthData::Confirm) return L_READY;
	return L_OK;
}

enum { ZAZOR = 66 };

void GUI::Auth(void *fontsNickname, vector<Row> &rows, AuthData &auth, bool *outFinished) {
	assert(outFinished);

	constexpr auto width = 300.f;
	constexpr auto zazor = ZAZOR;

	ImGui::SetNextWindowFocus();
	ImGui::Begin("###Auth", nullptr, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_::ImGuiWindowFlags_NoTitleBar);

	auto mult = (1.1f + 0.275f * ((int)rows.size() - 2.f));
	if (auth.type == AuthData::Confirm)
		mult = 0.92f;
	else if (auth.type == AuthData::ResetPassword_Email)
		mult = 0.7f;
	const ImVec2 sz = ImVec2{ width + zazor, (width + zazor) * mult };
	const ImVec2 screenSz = { this->GetWindowWidth(), this->GetWindowHeight() };
	ImGui::SetWindowSize(sz);
	const ImVec2 winPos = { (screenSz.x - sz.x) / 2, (screenSz.y - sz.y) / 2 };
	ImGui::SetWindowPos(winPos);
	static bool linkHovered = false;

	bool invokeOk = false;
	if (this->GetKeyPressed(VK_RETURN) && auth.rememberMe) invokeOk = true;

	//ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_Alpha);
	for (size_t i = 0; i < rows.size(); ++i) {
		auto &r = rows[i];
		switch (r.type) {
		case Row::Type::Input:
			WhiteBorder([&] {
				TextHeight(30, fontsNickname, [&] {
					ItemWidth(ImGui::GetWindowWidth() - zazor, [&] {
						ImGui::SetCursorPosX(zazor / 2 + width / 2 - ImGui::CalcTextSize(r.text.data()).x / 2);
						TextHeight(27, fontsNickname, [&] { ImGui::Text(r.text.data()); });
						ImGui::SetCursorPosX(zazor / 2);
						if (r.wasActive) this->ProcessCtrlCCtrlV();
						ImGui::InputText(r.inputLabel.data(), r.buf, r.bufSize, r.inputFlags,
							(r.inputFlags & ImGuiInputTextFlags_CallbackAlways) ? cbb : nullptr,
							(void *)0);
					});
				});
				ImGui::NewLine();
			});
			if (r.wasActive != ImGui::IsItemActive()) {
				if (r.wasActive) {
					if (i + 1 < rows.size()) {
						// Focus to next input
						if (!auth.rememberMe) rows[i + 1].taskFocus = true;
					}
					else {
						if (this->GetKeyPressed(VK_RETURN)) invokeOk = true;
					}
				}
				r.wasActive = ImGui::IsItemActive();
			}
			if (r.taskFocus) {
				r.taskFocus = false;
				if (!linkHovered && auth.type == AuthData::Login) {
					ImGui::SetKeyboardFocusHere(1);
				}
			}
			break;
		case Row::Type::Any:
			r.render();
			break;
		default:
			assert(0);
			break;
		}
	}

	if (auth.type == AuthData::Login) {
		ImGui::NewLine();
		WhiteBorder([&] { 
			if (ImGui::Checkbox(L_REMEMBER, &auth.rememberMe)) {
				PlaySound(
					auth.rememberMe ? SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_select_on.wav" : SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_select_off.wav", 
					0, SND_FILENAME | SND_ASYNC
				);
			}
		});
		ImGui::SameLine(0.0f, zazor);
		static float linkPercent = 0;
		Link([&] { 
			if (ImGui::Button(L_FORGOT_PASS)) {
				auth.forceResetPassword = true;
				*outFinished = true;
			}
		}, &linkPercent);
		ImGui::NewLine();
	}
	else if (auth.type == AuthData::Confirm) {
		static float linkPercent[2] = { 0.f,0.f };
		// TODO: Change email link?
	}
	else if (auth.type == AuthData::ResetPassword_Email) {
		static float linkPercent[2] = { 0.f,0.f };
		// TODO: Go back link?
	}
	else {
		ImGui::NewLine();
		ImGui::NewLine();
		ImGui::NewLine();
	}

	ImVec2 enterButtonSize = { ImGui::GetWindowSize().x - 16, 50.f };
	static float opa;
	TextHeight(30, fontsNickname, [&] {
		NiceButton([&] { 
			if (ImGui::Button(GetNiceButtonText(auth.type) , enterButtonSize) || invokeOk) {
				if (auth.finished) return;
				if (auth.type != AuthData::Confirm) PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_menu_ok.wav", 0, SND_FILENAME | SND_ASYNC);
				auth.systemError.clear();
				if (auth.type == AuthData::Login) {
					auth.name = rows[0].buf;
					auth.password = rows[1].buf;
					*outFinished = true;
					auth.finished = true;
				}
				else if (auth.type == AuthData::Register) {
					auth.name = rows[0].buf;
					auth.password = rows[1].buf;
					auto &repeat = rows[2].buf;
					auth.email = rows[3].buf;
					auth.invite = rows[4].buf;
					if (auth.password != repeat) {
						auth.systemError = L_PASSWORDS_MATCH_ERROR;
						PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_activatefail.wav", 0, SND_FILENAME | SND_ASYNC);
						return;
					}
					if (auth.email.find('@') == string::npos) {
						auth.systemError = L_INCORRECT_EMAIL;
						PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_activatefail.wav", 0, SND_FILENAME | SND_ASYNC);
						return;
					}
					*outFinished = true;
					auth.finished = true;
				}
				else if (auth.type == AuthData::Confirm) {
					assert(rows[1].buf);
					assert(rows[1].type == Row::Type::Input);
					auth.confirmCode = rows[1].buf;
					*outFinished = true;
					auth.finished = true;
				}
				else if (auth.type == AuthData::ResetPassword_Email) {
					assert(rows[1].buf);
					assert(rows[1].type == Row::Type::Input);
					auth.email = rows[1].buf;
					*outFinished = true;
					auth.finished = true;
				}
				else if (auth.type == AuthData::ResetPassword_NewPass) {
					auth.confirmCode = rows[0].buf;
					auth.password = rows[1].buf;
					auto &repeat = rows[2].buf;
					if (auth.password != repeat) {
						auth.systemError = L_PASSWORDS_MATCH_ERROR;
						PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_activatefail.wav", 0, SND_FILENAME | SND_ASYNC);
						return;
					}
					*outFinished = true;
					auth.finished = true;
				}
			}
		}, &opa, this->GetKeyPressed(VK_RETURN));
	});

	ImGui::PushStyleColor(ImGuiCol_Text, { 1.0f, 0xe5 / 256.f, 0xc1 / 256.f , 1.0f});
	string currentText;
	const auto sysError = auth.systemError + '\n';
	for (auto ch : sysError) {
		if (ch == '\n') {
			ImGui::SetCursorPosX(zazor / 2 + width / 2 - ImGui::CalcTextSize(currentText.data()).x / 2);
			ImGui::Text("%s", currentText.data());
			currentText.clear();
		}
		else {
			currentText += ch;
		}
	}
	ImGui::PopStyleColor();

	static float linkPercent2 = 0;
	Link([&] { 
		if (auth.type == AuthData::Register && ImGui::Button(L_AUTH_LINK, { ImGui::GetWindowWidth(), 20 })) {
			auth.type = AuthData::Login;
			auth.systemError.clear();
			PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_menu_cancel.wav", 0, SND_FILENAME | SND_ASYNC);
		}
		if (auth.type == AuthData::Login && ImGui::Button(L_CREATE_ACCOUNT_LINK, { ImGui::GetWindowWidth(), 20 })) {
			auth.type = AuthData::Register;
			auth.systemError.clear();
			PlaySound(SKYMP2_FILES_DIR"\\sound-fx-ui\\ui_menu_cancel.wav", 0, SND_FILENAME | SND_ASYNC);
		}
		if (auth.type == AuthData::Confirm && ImGui::Button(L_NET_PISMA, { ImGui::GetWindowWidth(), 20 })) {
			static clock_t lastClick = 0; // TODO: non-static
			int intervalMs = 6000;
			if (clock() - lastClick > intervalMs) {
				lastClick = clock();
				auth.forceRetryConfirm = true;
				*outFinished = true;
				auth.systemError.clear();
			}
			else {
				auth.systemError = L_WAIT_PLEASE;
			}
		}
		linkHovered = ImGui::IsItemHovered();
	}, &linkPercent2);

	ImGui::End();
}

void GUI::OnRender(void *fontsNickname) {

	static bool bloor = false;

	//ImGui::ShowDemoWindow();

	{
		// TODO: exit thread after auth?
		static bool started = false;
		if (!started) {
			started = true;
			std::thread([] {
				while (1) {
					Sleep(5);
					UpdatePressedKeys();
				}
			}).detach();
		}
	}

	{
		std::lock_guard<std::recursive_mutex> l(pImpl->mutex);

		bool nowActive = !!pImpl->auth;
		bool onShow = false;
		if (nowActive != pImpl->authWasActive) {
			pImpl->authWasActive = nowActive;
			if (nowActive) onShow = true;
		}

		if (pImpl->auth) {

			static char loginBuf[Row::bufSize] = "",
				passBuf[Row::bufSize] = "",
				passRepeatBuf[Row::bufSize] = "",
				emailBuf[Row::bufSize] = "",
				inviteBuf[Row::bufSize] = "",
				confirmCodeBuf[Row::bufSize] = "",
				confirmCodeBuf2[Row::bufSize] = "";
			if (onShow) {
				strcpy_s(passBuf, pImpl->auth->password.data());
				strcpy_s(loginBuf, pImpl->auth->name.data());
			}
			static std::array<vector<Row>, AuthData::Type::COUNT> rows;
			if (auto &r = rows[AuthData::Login]; r.empty()) {
				r.push_back(Row(L_LOGIN, 0, loginBuf));
				r.push_back(Row(L_PASSWORD, ImGuiInputTextFlags_Password, passBuf));
				r[0].taskFocus = true;
			}
			if (auto &r = rows[AuthData::Register]; r.empty()) {
				r.push_back(Row(L_LOGIN, 0, loginBuf));
				r.push_back(Row(L_PASSWORD, ImGuiInputTextFlags_Password, passBuf));
				r.push_back(Row(L_PASSWORD_REPEAT, ImGuiInputTextFlags_Password, passRepeatBuf));
				r.push_back(Row(L_EMAIL, ImGuiInputTextFlags_CallbackAlways, emailBuf));
				r.push_back(Row(L_INVITE_CODE, 0, inviteBuf));
			}
			if (auto &r = rows[AuthData::Confirm]; r.empty()) {
				r.push_back(Row([fontsNickname] {
					char buf[1024];
					sprintf_s(buf, L_FMT_CONFIRM_EMAIL_TEXT, strlen(emailBuf) > 1 ? emailBuf : L_YOUR_EMAIL);
					auto cp = ImGui::GetCursorPos();
					ImGui::NewLine();
					TextHeight(20, fontsNickname, [&] { TextWithColors(buf); });
					ImGui::NewLine();
				}));
				r.push_back(Row(L_CONFIRM_CODE, 0, confirmCodeBuf));
			}
			if (auto &r = rows[AuthData::ResetPassword_Email]; r.empty()) {
				r.push_back(Row([fontsNickname] {
					TextHeight(20, fontsNickname, [&] { ImGui::Text(L_ENTER_ACCOUNT_EMAIL); });
				}));
				r.push_back(Row(L_EMAIL, ImGuiInputTextFlags_CallbackAlways, emailBuf));
			}
			if (auto &r = rows[AuthData::ResetPassword_NewPass]; r.empty()) {
				r.push_back(Row(L_CONFIRM_CODE, 0, confirmCodeBuf2));
				r.push_back(Row(L_NEW_PASSWORD, ImGuiInputTextFlags_Password, passBuf));
				r.push_back(Row(L_PASSWORD_REPEAT, ImGuiInputTextFlags_Password, passRepeatBuf));
			}

			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);
			auto &auth = *pImpl->auth;
			bool finished = false;


			static float authOpa = -1.f;
			authOpa += 0.025;
			if (authOpa > 1 || auth.rememberMe) authOpa = 1.f;
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, authOpa > 0 ? authOpa : 0.f);
			Auth(fontsNickname, rows[auth.type], auth, &finished);
			ImGui::PopStyleVar();

			if (finished) {
				pImpl->auth->finished = true;
				pImpl->authOut.reset(new AuthData(*pImpl->auth));
			}

			ImGui::PopStyleVar();
		}
	}

	{
		static bool pWas = false;
		static int render = 0;

		static auto menus = {
			new BSFixedString("ContainerMenu"),
			new BSFixedString("InventoryMenu"),
			new BSFixedString("MagicMenu"),
			new BSFixedString("MapMenu"),
			new BSFixedString("Console"),
			new BSFixedString("Book Menu"),
			new BSFixedString("TweenMenu"),
			new BSFixedString("Main Menu"),
			new BSFixedString("Loading Menu")
		};
		bool open = false;
		for (auto menu : menus) {
			open = open || MenuManager::GetSingleton()->IsMenuOpen(menu);
		}
		static clock_t lastOpen = 0;
		if (open) lastOpen = clock();

		bool p = clock() - lastOpen > 250 && this->GetKeyPressed(VK_ESCAPE);


		static auto fsJournalMenu = new BSFixedString("Journal Menu");
		static auto fsTutorialMenu = new BSFixedString("Tutorial Menu");
		static auto fsSleepMenu = new BSFixedString("Sleep/Wait Menu");
		static bool thrStarted = 0;
		if (!thrStarted) {
			std::thread([] {
				while (1) {
					Sleep(30);
					UIManager::GetSingleton()->CloseMenu(fsJournalMenu);
					UIManager::GetSingleton()->CloseMenu(fsTutorialMenu);
					UIManager::GetSingleton()->CloseMenu(fsSleepMenu);
				}
			}).detach();
			thrStarted = 1;
		}
		if (p != pWas) {
			pWas = p;
			if (p) {
				if (render || !this->IsBlocked("escapeMenuBlock")) {
					render = !render;
					UIManager::GetSingleton()->CloseMenu(fsJournalMenu);
					sd::ExecuteConsoleCommand("tm", 0);
					this->SetCursor(render);
				}
			}
		}
		if (open && render) {
			sd::ExecuteConsoleCommand("tm", 0);
			render = false;
			this->SetCursor(false);
		}
		if (render) {
			g_bloorTaskQ.Add([] {
				auto mod = (TESImageSpaceModifier *)LookupFormByID(IMAGE_SPACE_MODIFIER);
				if(bloor == false) sd::Apply(mod, 1.0f);
				bloor = 1;
			});
			auto fontsNick = *(std::unordered_map<int, ImFont*> *)fontsNickname;
			ImGui::PushFont(fontsNick[18]);

			ImGui::SetNextWindowPosCenter();

			const bool F6 = pImpl->visibleCursor.load();
			struct PushVar {
				PushVar(bool F6) { 
					int flags = ImGuiWindowFlags_NoCollapse;
					ImVec4 bgCol, titleBgCol, btnCol, btnActiveCol;

					const auto ACTIVE_BUTTON = ImVec4(0.9, 0.9, 0.9, 1);
					if (!F6) {
						flags |= ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoResize;
						bgCol = ImVec4(0.07, 0.07, 0.07, 0.6);
						titleBgCol = bgCol;
						btnCol = ImVec4(0, 0, 0, 0.7);
						btnActiveCol = btnCol;
					}
					else {
						bgCol = ImVec4(0.07, 0.07, 0.07, 0.9);
						titleBgCol = bgCol;
						btnCol = ImVec4(0, 0, 0, 0.7);
						btnActiveCol = ACTIVE_BUTTON;
					}
					ImGui::PushStyleVar(ImGuiStyleVar_WindowTitleAlign, ImVec2(0.5f, 0.5f)); 
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, bgCol);
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_TitleBg, titleBgCol);
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_TitleBgActive, titleBgCol);
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Button, btnCol);
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonHovered, btnActiveCol);
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonActive, btnActiveCol);
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ScrollbarGrabActive, { 0.92f,0.92f,0.92f,0.7f });
				}
				~PushVar() { 
					ImGui::PopStyleVar(); 
					ImGui::PopStyleColor(7);
				};
			} pushVar(F6);
			ImGui::Begin(L_MENU_CAPS, 0, ImGuiWindowFlags_::ImGuiWindowFlags_NoNav | ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_::ImGuiWindowFlags_NoMove | ImGuiWindowFlags_::ImGuiWindowFlags_NoResize);

			ImVec2 size = { 280 * 1.4, 310 * 1.4 - 17};
			ImVec2 bsz = { 280 *1.3 ,40};

			auto texts = { L_PROFILE ,L_HELP , (L_REPORT), L_ORGANIZATION ,L_SETTINGS ,L_EXIT };


			for (auto v : texts) {
				ImGui::SetCursorPosX((0.5*size.x) - (0.5*bsz.x));

				auto p = ImGui::GetCursorPosY();
				p += 16;
				ImGui::SetCursorPosY(p);

				//ImGui::Button(v, bsz);
				static string sendHov = "";
				auto uiuu = ImGui::IsAnyItemHovered() && sendHov == v;
				if (uiuu) ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, { 0,0,0,1 });
				if (ImGui::Button(v, bsz)) {
					if (string(L_EXIT) == v) g_bloorTaskQ.Add([=] {
						std::thread([] {
							ExitSkymp(0);
						}).detach();
					});
				}
				if (uiuu) ImGui::PopStyleColor();
				if (ImGui::IsItemHovered()) {
					sendHov = v;
				}
			}

			//size.y = ImGui::GetWindowHeight();
			ImGui::SetWindowSize(size);
			ImGui::End();

			ImGui::PopFont();
		}
		else {
			g_bloorTaskQ.Add([] {
				auto mod = (TESImageSpaceModifier *)LookupFormByID(IMAGE_SPACE_MODIFIER);
				if(bloor)sd::Remove(mod);
				bloor = 0;
			});
		}
	}

	g_fontsNickname = fontsNickname;

	{
		auto &io = ImGui::GetIO();
		std::lock_guard<std::recursive_mutex> l(g_cursorPosM);
		g_cursorPos = { io.MousePos.x - 8.f, io.MousePos.y - 12.f };
	}

	bool cursor = pImpl->visibleCursor.load();

	if (clock() - pImpl->lastUpd > 100) {
		static auto fsJournalMenu = new BSFixedString("Journal Menu");
		pImpl->menuOpen = MenuManager::GetSingleton()->IsMenuOpen(fsJournalMenu);
	}

	if (!pImpl->menuOpen) {
		int i = 0;
		for (auto t : pImpl->texts) {
			if (t->GetText().empty()) continue;
			if (t->GetPreferredTextH() == 0) continue;

			++i;
			auto p = ImVec2(t->GetPos().first, t->GetPos().second);

			auto pos3d = t->GetPos3D();
			if (pos3d.Length() > 0) {
				WorldPointToScreen(pos3d, &p);
			}

			if (pos3d.Length() > 0) {
				const string uniqueKey = std::to_string(reinterpret_cast<size_t>(&*t)) + " " + std::to_string(i) + " " + t->GetText();
				static std::unordered_map<string, ImVec2> lastPos2d;
				if (lastPos2d.count(uniqueKey) == 0)lastPos2d[uniqueKey] = p;
				auto &lastP2d = lastPos2d[uniqueKey];

				ImVec2 difference = { p.x - lastP2d.x, p.y - lastP2d.y };
				ImVec2 speed = { difference.x / 60, difference.y / 60 };

				p.x += speed.x;
				p.y += speed.y;

				lastP2d = p;

			}

			auto dopRender = t->GetPrePostRender();

			if (dopRender.first) dopRender.first();
			switch (t->GetType()) {
			case Text::Type::Default:
				Window(std::to_string(i) + "text", ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar, false, [&] {
					if (t->HasFlag("Mini")) {
						auto sz = ImGui::CalcTextSize(t->GetText().data());

						if (sz.x > 60) {
							auto txtCursor = ImGui::GetCursorPos();
							ImGui::SetCursorPos({ txtCursor.x,5 });
						}

						static std::unordered_map<int, float> cacheTranslation;
						if (cacheTranslation.count(i) == 0) cacheTranslation[i] = 0;
						if (g_cacheOpacity.count(&*t) == 0) g_cacheOpacity[&*t] = 0.75;

						if (t->HasFlag("Hovered")) {
							cacheTranslation[i] += 1;
							if (cacheTranslation[i] > 20) cacheTranslation[i] = 20;

							g_cacheOpacity[&*t] += 0.05f;
							if (g_cacheOpacity[&*t] > 1.f) g_cacheOpacity[&*t] = 1.f;
						}
						else {
							cacheTranslation[i] -= 1;
							if (cacheTranslation[i] < 0) cacheTranslation[i] = 0;

							g_cacheOpacity[&*t] -= 0.05f;
							if (g_cacheOpacity[&*t] < 0.70f) g_cacheOpacity[&*t] = 0.70f;
						}
						ImGui::Text("%s", t->GetText().data());
						sz.x += 7.5;
						sz.y += 17;
						if (sz.x > 65) {
							sz.x = INTERACTION_LABEL_WIDTH / 2;// is interaction menu label
							sz.y = 20; // ne robit
						}
						sz.x *= 1.25;
						ImGui::SetWindowSize(sz);
						p.y += t->GetOffsetY();
						p.x += cacheTranslation[i] / 2;
						ImGui::SetWindowPos(p);
					}
					else {
						auto sz = ImGui::CalcTextSize(t->GetText().data());

						if (sz.x > 115) {
							auto txtCursor = ImGui::GetCursorPos();
							ImGui::SetCursorPos({ txtCursor.x,5 });
						}

						static std::unordered_map<int, float> cacheTranslation;
						if (cacheTranslation.count(i) == 0) cacheTranslation[i] = 0;
						if (g_cacheOpacity.count(&*t) == 0) g_cacheOpacity[&*t] = 0.75;

						if (t->HasFlag("Hovered")) {
							cacheTranslation[i] += 1;
							if (cacheTranslation[i] > 20) cacheTranslation[i] = 20;

							g_cacheOpacity[&*t] += 0.05f;
							if (g_cacheOpacity[&*t] > 1.f) g_cacheOpacity[&*t] = 1.f;
						}
						else {
							cacheTranslation[i] -= 1;
							if (cacheTranslation[i] < 0) cacheTranslation[i] = 0;

							g_cacheOpacity[&*t] -= 0.05f;
							if (g_cacheOpacity[&*t] < 0.70f) g_cacheOpacity[&*t] = 0.70f;
						}
						ImGui::Text("%s", t->GetText().data());
						sz.x += 15;
						sz.y += 17;
						if (sz.x > 130) {
							sz.x = INTERACTION_LABEL_WIDTH;// is interaction menu label
							sz.y = 10; // ne robit
						}
						ImGui::SetWindowSize(sz);
						p.y += t->GetOffsetY();
						p.x += cacheTranslation[i];
						ImGui::SetWindowPos(p);
					}
				});
				break;
			case Text::Type::ChatBubble:
			case Text::Type::Nickname:
			{
				int maxx = -1000000, min = 1000000;
				auto fontsNick = *(std::unordered_map<int, ImFont*> *)fontsNickname;
				for (auto v : fontsNick) {
					if (v.first > maxx) maxx = v.first;
					if (v.first < min) min = v.first;
				}
				assert(fontsNick.size());
				assert(maxx > 0);

				int h = t->GetPreferredTextH();
				if (h > maxx) h = maxx;
				if (h < min) h = min;

				if (fontsNick[h]) ImGui::PushFont(fontsNick[h]);
				auto sz = ImGui::CalcTextSize(t->GetText().data());
				p.x -= sz.x / 2 *1.3;
				p.y += h - maxx + 30;

				float nicknameAlpha = 0.7f;
				static auto fs = new BSFixedString("InventoryMenu");
				if (MenuManager::GetSingleton()->IsMenuOpen(fs)) nicknameAlpha = 0.f;
				if (t->GetType() == Text::Type::Nickname) ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_Alpha, nicknameAlpha);
				ImguiUtils::DrawText_(t->GetText(), p, true, std::to_string(reinterpret_cast<size_t>(&*t)));
				if (t->GetType() == Text::Type::Nickname) ImGui::PopStyleVar();
				if (fontsNick[h]) ImGui::PopFont();
				break;
			}
			case Text::Type::Empty:
				break;
			default:
				assert(0);
				break;
			}
			if (dopRender.second) dopRender.second();
		}
	}
}

void GUI::SetCursor(bool visible) {
	// Fixes Release
	this->ImguiManager::SetCursorVisible(visible);

	if (pImpl->visibleCursor.load() != visible) {
		pImpl->visibleCursor = visible;
		auto &c = pImpl->controls;
		c->SetEnabled(Controls::Movement, !visible, "GUI");
		c->SetEnabled(Controls::Looking, !visible, "GUI");
		c->SetEnabled(Controls::Activate, !visible, "GUI");
		c->SetEnabled(Controls::Menu, !visible, "GUI");
		c->SetEnabled(Controls::Fighting, !visible, "GUI");
		c->SetEnabled(Controls::CamSwitch, !visible, "GUI");
	}
}

std::shared_ptr<Text> GUI::CreateText(const string &text, std::pair<float, float> pos) {
	auto newText = new Text;
	newText->SetPos(pos);
	newText->SetText(text);
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->texts.insert(newText);
	return std::shared_ptr<Text>(newText, [this](Text *txt) {
		std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
		pImpl->texts.erase(txt);
		delete txt;
	});
}

std::shared_ptr<Text> GUI::CreateText(const string &text, const NiPoint3 &pos) {
	auto res = CreateText(text, { 0.f,0.f });
	res->SetPos(pos);
	return res;
}

std::pair<float, float> GUI::WorldPointToScreen(const NiPoint3 &pos3d) {
	ImVec2 res;
	this->WorldPointToScreen(pos3d, &res);
	return { res.x, res.y };
}

std::pair<float, float> GUI::GetCursorPos() const {
	std::lock_guard<std::recursive_mutex> l(g_cursorPosM);
	return { g_cursorPos.x, g_cursorPos.y };
}

auto posSrcFn = [](formid id) {
	try {
		// TODO: is it safe
		NiPoint3 res;
		auto ref = (TESObjectREFR *)LookupFormByID(id);
		if (ref && ref->formType == kFormType_Character) {
			res = ref->pos;
			res.z += 96;
		}
		return res;

	}
	catch (...) {
		assert(0);
	}
	return NiPoint3();
};

std::unordered_map<string, bool> g_SDTHRpartyOver;
TaskQueue g_partyOverQ;

// Returns null or object
// {
// targetid: formid,
// index: int
// }

formid g_targetid = 0;
formid g_targetidRemote = 0;

json GUI::UpdateInteractionMenu(const vector<formid> &selectable, json partyData) {
	g_partyOverQ.Update();
	json ret;

	{
		bool pr = sd::GetKeyPressed(VK_RBUTTON);
		static std::unordered_map<formid, bool> prWas; // TODO: bool instead of map<bool>
		if (pr != prWas[0]) {
			prWas[0] = pr;

			std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
			if (pr && pImpl->interactionMenu) {
				closeInteractionMenu();
			}
		}
	}

	// When not F6 mode close interaction menu
	if (pImpl->visibleCursor.load() == false) {
		std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
		closeInteractionMenu();
	}


	bool over = false;
	int overLabel = -1;
	const auto cursorPos = this->GetCursorPos();
	// calc 'overLabel'
	if (pImpl->visibleCursor.load()) {
		std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
		if (pImpl->interactionMenu) {
			for (size_t i = 0; i != pImpl->interactionMenu->texts.size(); ++i) {
				std::pair<float, float> rectBegin = { g_labelOverCalcBase.x, g_labelOverCalcBase.y - 5 };
				rectBegin.second += i * (float)LABEL_Y_OFFSET;
				auto rectEnd = rectBegin;
				rectEnd.first += INTERACTION_LABEL_WIDTH;
				rectEnd.second += 30; // ???
				if (cursorPos.first >= rectBegin.first && cursorPos.second >= rectBegin.second) {
					if (cursorPos.first <= rectEnd.first && cursorPos.second <= rectEnd.second) {
						overLabel = i;
					}
				}
			}
			for (int i = 0; i != pImpl->interactionMenu->texts.size(); ++i) {
				if (i != overLabel) pImpl->interactionMenu->texts[i]->RemoveFlag("Hovered");
			}
			if (overLabel != -1 && overLabel < pImpl->interactionMenu->texts.size()) {
				pImpl->interactionMenu->texts[overLabel]->AddFlag("Hovered");
			}
		}
		//skip "" labels
		if (overLabel > -1) {
			int i = 0;
			while (1) {
				if (pImpl->interactionMenu->texts.size() > i && 
					pImpl->interactionMenu->texts[i]->GetText() == "") {
					overLabel--;
				}
				else {
					break;
				}
				++i;
			}
		}
	}

	static clock_t interMenuOk = 0;

	// handle label selection
	{
		static bool pwas = false;
		bool p = sd::GetKeyPressed(VK_LBUTTON);
		bool tap = 0;
		if (pwas != p) {
			pwas = p;
			if (p) tap = 1;
		}
		if (overLabel != -1) {
			if (tap) {
				interMenuOk = clock() + 666;
				closeInteractionMenu();
				ret = json::object();
				ret["targetid"] = g_targetid;
				ret["server_targetid"] = g_targetidRemote;
				ret["index"] = g_isPartyInteractionMenu ? 1000 + overLabel : overLabel;
			}
		}
	}

	for (formid id : selectable) {
		over = false;
		auto ref = (Actor *)LookupFormByID(id);
		if (ref && ref->formType == kFormType_Character) {

			// calc 'over'
			if (pImpl->visibleCursor.load()) {
				auto refScreenPos = WorldPointToScreen(sd::GetPosition(ref));
				auto refScreenPosUp = WorldPointToScreen(sd::GetPosition(ref) + NiPoint3(0, 0, 128));
				if (cursorPos.second <= refScreenPos.second && cursorPos.second >= refScreenPosUp.second) {
					if (abs(cursorPos.first - refScreenPos.first) <= 100) {
						over = true;
					}
				}
			}

			over = over && (!pImpl->interactionMenu || pImpl->interactionMenu->texts.empty());
			over = over && pImpl->dialogPresent == false; // TODO: atomic

			bool visualOver = 0;
			string name = papyrusObjectReference::GetDisplayName(ref).data;
			visualOver = g_SDTHRpartyOver[name];

			// light the reference
			static std::unordered_map<formid, float> cacheAlpha;
			float alpha = (over || visualOver) ? 0.75 : 1.0;
			if (cacheAlpha[id] != alpha) {
				sd::SetAlpha(ref, alpha, false);
				cacheAlpha[id] = alpha;
			}

			{
				bool pr = sd::GetKeyPressed(VK_LBUTTON);
				static std::unordered_map<formid, bool> prWas;
				if (pr != prWas[id]) {
					prWas[id] = pr;
					if (pr && over && clock() > interMenuOk) {
						std::lock_guard<std::recursive_mutex> l(pImpl->mutex);

						this->NewInteractionMenu(id, { PARTY_INVITE, EXCHANGE, WHISPER }, {}, partyData);
						g_targetid = id;
						g_targetidRemote = 0;
						g_isPartyInteractionMenu = false;
					}
				}
			}
		}
	}
	return ret;
}

volatile bool g_waitForSend = false; // TODO: any sync
std::list<string> g_outMessages;
int g_outMessageChannel = -1;
TaskQueue g_outMessagesQ;
bool g_ctrlcTask = false;
bool g_ctrlV = false;

void GUI::ProcessCtrlCCtrlV() {
	// Ctrl+C
	{
		bool pressed(this->GetKeyPressed(VK_CONTROL) && this->GetKeyPressed('C'));
		static bool pressedWas = false;
		bool trigger = false;
		if (pressed != pressedWas) {
			if (pressed) trigger = true;
			pressedWas = pressed;
		}
		if (trigger) {
			g_ctrlcTask = 1;
		}
	}
	// Ctrl+V
	{
		bool pressed(this->GetKeyPressed(VK_CONTROL) && this->GetKeyPressed('V'));
		static bool pressedWas = false;
		bool trigger = false;
		if (pressed != pressedWas) {
			if (pressed) trigger = true;
			pressedWas = pressed;
		}
		g_ctrlV = trigger;
	}
}

int cb(ImGuiTextEditCallbackData *data) {
	const bool no_cursor = data->UserData;
	if (no_cursor) {
		data->BufDirty = 1;
		data->Buf[0] = 0;
		data->BufTextLen = 0;
		data->CursorPos = 0;
		return 0;
	}
	if (g_ctrlcTask) {
		string selected;

		int start = data->SelectionStart;
		int end = data->SelectionEnd;
		if (start > end) std::swap(start, end);

		for (int i = start; i < end; ++i) {
			selected += data->Buf[i];
		}
		///selected += " " + std::to_string(data->SelectionStart);
		///selected += " " + std::to_string(data->SelectionEnd);
		bool bad = data->SelectionStart == data->SelectionEnd;
		if (!bad) {
			//MessageBoxA(0, selected.data(), "pizda", MB_ICONERROR);
			ImGui::SetClipboardText(selected.data());
			g_ctrlcTask = false;
		}
	}
	if (g_ctrlV) {
		string text = (string)data->Buf + ImGui::GetClipboardText();
		memcpy(data->Buf, text.data(), text.size() + 1);
		data->BufTextLen = text.size();
		data->BufDirty = true;
		data->CursorPos = text.size();
	}
	if (g_waitForSend) {
		g_waitForSend = false;
		string msg = data->Buf;
		g_outMessagesQ.Add([msg] {
			g_outMessages.push_back(msg);
		});
		data->BufDirty = 1;
		data->Buf[0] = 0;
		data->BufTextLen = 0;
		data->CursorPos = 0;
	}
	return 0;
}

ImVec4 ConvertColor(uint32_t col) {
	union {
		uint32_t col;
		uint8_t colChars[4];
	} u;

	u.col = col;
	ImVec4 res;
	for (int i = 0; i != 4; ++i) {
		auto v = u.colChars[4 - i - 1] / 256.f;
		switch (i) {
		case 0: res.x = v; break;
		case 1: res.y = v; break;
		case 2: res.z = v; break;
		case 3: res.w = v; break;
		}
	}
	return res;
}

std::set<int> g_pulse;
static int chSel = -1; // unsafe
string g_scriptPress;

volatile bool g_noChat = false;

json GUI::UpdateChat(const vector<ChatChannel> &channels) {

	bool press = this->GetKeyPressed(VK_F7);
	static bool pressWas = false;
	if (press != pressWas) {
		pressWas = press;
		if (press) g_noChat = !g_noChat;
	}

	string scriptPress = g_scriptPress;
	if (scriptPress.empty()) {
		for(int i = 0; i != 20; ++i) scriptPress += "JAOISDJOIASOJD";
	}
	g_scriptPress.clear();

	g_outMessagesQ.Update();
	json ret = json::object();
	if (!g_outMessages.empty()) {
		ret["message"] = g_outMessages.front();
		assert(g_outMessageChannel >= 0);
		ret["channel"] = channels[g_outMessageChannel].id;
		g_outMessages.pop_front();
	}

	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);

	bool chChange = false;
	std::set<int> changedChannels;

	auto mm = MenuManager::GetSingleton();
	static auto fsConsole = new BSFixedString("Console");
	static auto bad = {
		new BSFixedString("InventoryMenu"),
		new BSFixedString("Journal Menu"),
		new BSFixedString("LevelUp Menu"),
		new BSFixedString("Lockpicking Menu"),
		new BSFixedString("Loading Menu"),
		new BSFixedString("MagicMenu"),
		new BSFixedString("MapMenu"),
		new BSFixedString("TweenMenu"),
		new BSFixedString("Dialogue Menu"),
		new BSFixedString("GiftMenu"),
		new BSFixedString("Book Menu"),
		new BSFixedString("BarterMenu"),
		new BSFixedString("Crafting Menu"),
		new BSFixedString("ContainerMenu"),
		fsConsole
	};
	bool open = false;
	for (auto b : bad)
		open = open || mm->IsMenuOpen(b);
	if (mm->IsMenuOpen(fsConsole) && pImpl->visibleCursor.load()) {
		UIManager::GetSingleton()->CloseMenu(fsConsole);
	}

	if (pImpl->chat) pImpl->chat->SetText(open ? "" : "...");

	static vector<ChatChannel> channelsWas;
	if (channelsWas.size() != channels.size()) {
		chChange = true;
	}
	else {
		for (int i = 0; i != channelsWas.size(); ++i) {
			if (channelsWas[i].messages.size() != channels[i].messages.size()) {
				changedChannels.insert(i);
				g_pulse.insert(i);
			}
		}
	}
	channelsWas = channels;

	
	if (pImpl->chat == nullptr) {
		static std::vector<std::shared_ptr<Text>> storage;
		auto chat = this->CreateText("...", { 0.f, 0.f });
		storage.push_back(chat);
		pImpl->chat = &*chat;
	}

	if (pImpl->chat) {
		pImpl->chat->SetType(Text::Type::Empty);
		if (g_noChat) {
			pImpl->chat->SetPrePostRender([] {}, [] {});
		}
		else pImpl->chat->SetPrePostRender([=] {
			
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);
			ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 0);
			ImGui::PushStyleVar(ImGuiStyleVar_ChildRounding, 0);
			ImGui::PushStyleVar(ImGuiStyleVar_GrabRounding, 0);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowTitleAlign, ImVec2(0.5f, 0.5f));
			ImGui::PushStyleVar(ImGuiStyleVar_ScrollbarRounding, 0);

			int flags = ImGuiWindowFlags_NoCollapse;
			ImVec4 bgCol, titleBgCol, btnCol, btnActiveCol;

			const auto ACTIVE_BUTTON = ImVec4(0.9, 0.9, 0.9, 1);

			const bool F6 = pImpl->visibleCursor.load();
			if (!F6) {
				flags |= ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoResize;
				bgCol = ImVec4(0.07, 0.07, 0.07, 0.6);
				titleBgCol = bgCol;
				btnCol = ImVec4(0, 0, 0, 0.7);
				btnActiveCol = btnCol;
			}
			else {
				bgCol = ImVec4(0.07, 0.07, 0.07, 0.9);
				titleBgCol = bgCol;
				btnCol = ImVec4(0, 0, 0, 0.7);
				btnActiveCol = ACTIVE_BUTTON;
			}
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, bgCol);
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_TitleBg, titleBgCol);
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_TitleBgActive, titleBgCol);
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Button, btnCol);
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonHovered, btnActiveCol);
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonActive, btnActiveCol);
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ScrollbarGrabActive, { 0.92f,0.92f,0.92f,0.7f });

			static string title = L_CHAT_CAPS "###chat";

			if (chSel == -1 && channels.size() > 0) {
				chSel = 0;
				title = L_CHAT_CAPS "###chat";
			}
			if (chSel >= channels.size()) {
				chSel = 0;
				title = L_CHAT_CAPS "###chat";
			}

			ImGui::Begin(title.data(), nullptr, flags);
			/*
			Pos=40,77
			Size=467,312
			*/
			ImGui::SetWindowPos({ 40.f,77.f }, ImGuiCond_::ImGuiCond_FirstUseEver);
			ImGui::SetWindowSize({ 467.f,312.f }, ImGuiCond_::ImGuiCond_FirstUseEver);

			// Draw channel buttons
			auto channels_ = channels;
			channels_.push_back({});
			for (int i = 0; i != channels_.size(); ++i) {
				ImVec2 btnSize;
				btnSize.x = ImGui::GetWindowSize().x / channels.size() - 7.5;
				btnSize.y = 26;

				if (channels_[i].name == "") btnSize = { 0,0 };

				static int hovIndex = -2;
				if (ImGui::IsItemHovered()) {
					hovIndex = i - 1;
				}
				if (ImGui::IsAnyItemHovered() == false) hovIndex = -2;

				const auto mousePosKey = std::to_string(int(ImGui::GetMousePos().x) / 15) + " " + std::to_string(int(ImGui::GetMousePos().y) / 15);

				static std::unordered_map<string, int> cacheHover;
				if (hovIndex < 0) {
					if (cacheHover.count(mousePosKey)) {
						hovIndex = cacheHover[mousePosKey];
					}
				}

				bool hovNow = i == hovIndex;
				static clock_t lastHov = 0;
				if (hovNow) lastHov = clock();
				///ImGui::Text("%d", int(hov));

				static std::unordered_map<int, float> cursCache;
				if (cursCache.count(i) == 0) cursCache[i] = 40;

				if (clock() - lastHov > 1000 || pImpl->visibleCursor.load() == false) {
					cacheHover.clear();
				}
				if (hovNow) {
					cacheHover[mousePosKey] = i;
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, { 0,0,0,1 });
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Button, ACTIVE_BUTTON);
				}
				else {

					static float opa = 1;
					static bool plus = true;
					static clock_t lastChange = 0;
					if (clock() - lastChange > 50) {
						if (opa >= BTN_TEXT_OPA_MAX) {
							opa = BTN_TEXT_OPA_MAX;
							plus = false;
						}
						if (opa <= BTN_TEXT_OPA_MIN) {
							opa = BTN_TEXT_OPA_MIN;
							plus = true;
						}
						opa += plus ? 0.0333333 : -0.0333333;
						lastChange = clock();
					}


					// WTF?
					static std::set<formid> cacheChangedChannels;
					for (auto ch : changedChannels) cacheChangedChannels.insert(ch);
					cacheChangedChannels.erase(chSel);
					g_pulse.erase(chSel);

					const bool allowPulse = !channels_[i].noBtnPulse;
					ImVec4 thisColor = { 1.f,1.f,1.f,0.75f };
					if (allowPulse && cacheChangedChannels.count(i) && g_pulse.count(i)) {
						thisColor.w = opa;
						if (!memcmp(channels_[i].id.data(), "whisper", 7)) {
							thisColor.x = 0xff/256.f;
							thisColor.y = 0xb7 / 256.f;
							thisColor.z = 0xb7 / 256.f;
						}
					}
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, thisColor);
				}
				cursCache[i] += hovNow ? -0.3 : 0.2;
				if (cursCache[i] > 40) cursCache[i] = 40;
				if (cursCache[i] < 35) cursCache[i] = 35;
				ImGui::SetCursorPosY(cursCache[i]);
				bool pressed = ImGui::Button(channels_[i].name.data(), btnSize) || scriptPress == channels_[i].id;
				if (hovNow) {
					ImGui::PopStyleColor(2);
				}
				else {
					ImGui::PopStyleColor(1);
				}
				if (pressed) {
					title = channels_[i].nameCaps + "###chat";
					chSel = i;
					g_outMessagesQ.Add([i] {
						g_outMessageChannel = i;
					});
				}
				if (i != channels_.size() - 1) ImGui::SameLine();
			}

			auto sz = ImGui::GetWindowSize();
			sz.y -= 110;
			sz.x -= 11;
			ImGui::BeginChild("###chat_child", sz);
			ImGui::SetWindowSize(sz);
			if (chSel >= 0 && chSel < channels.size()) {
				for (auto text : channels[chSel].messages) {
					ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, ConvertColor(text.color));
					ImGui::PushTextWrapPos(ImGui::GetWindowWidth() - 13);
					TextWithColors("%s", text.text.data());
					ImGui::PopTextWrapPos();
					ImGui::PopStyleColor();
				}
			}
			ImGui::Text("");
			ImGui::Text("");
			g_SetScrollHereQ.Update();
			
			//ImGui::SetScrollHere();
			ImGui::EndChild();

			const bool readOnly = chSel >= 0 && channels_[chSel].readOnly;
			if (!readOnly) {
				static char buf[300] = "";


				ImGui::BeginChild("###chat_child2");

				this->ProcessCtrlCCtrlV();
				ImGui::InputText("", buf, sizeof buf, ImGuiInputTextFlags_CallbackAlways, cb, (void*)(pImpl->visibleCursor.load() ? 0 : 1));


				static BOOL sendPress = 0;
				// https://github.com/ocornut/imgui/issues/455
				if ((ImGui::IsRootWindowOrAnyChildFocused() && !ImGui::IsAnyItemActive() && !ImGui::IsMouseClicked(0)) 
					|| ImGui::GetIO().KeysDown[VK_RETURN] || sendPress) 
				{
					sendPress = 0;
					pImpl->visibleCursor.load() ? ImGui::SetKeyboardFocusHere(0) : void();
				}

				ImGui::SameLine();

				static bool sendHov = false;
				if (sendHov) ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, { 0,0,0,1 });
				if (ImGui::Button(L_SEND)) {
					sendPress = 1;
				}
				if (sendHov) ImGui::PopStyleColor();
				sendHov = ImGui::IsItemHovered();

				static bool pwas = false;
				int returnPress = 0;
				if (pwas != ImGui::GetIO().KeysDown[VK_RETURN]) {
					pwas = ImGui::GetIO().KeysDown[VK_RETURN];
					if (ImGui::GetIO().KeysDown[VK_RETURN]) returnPress = 1;
				}
				bool sendMessage = sendPress || returnPress;
				if (sendMessage) {
					g_waitForSend = true;
				}

				ImGui::EndChild();
			}

		}, [=] {
			ImGui::End();
			ImGui::PopStyleVar(6);
			ImGui::PopStyleColor(7);
		});
	}

	return ret;
}

static json g_party;
static std::recursive_mutex g_partyM;

void GUI::UpdateParty(json partyData, string localPlName) {
	{
		std::lock_guard<std::recursive_mutex> l(g_partyM);
		g_party = std::move(partyData);
	}

	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);

	if (pImpl->party == nullptr) {
		static vector<std::shared_ptr<Text>> partyHolder;
		auto text = this->CreateText("...", { 1.f,1.f });
		partyHolder.push_back(text);
		pImpl->party = &*text;

		text->SetType(Text::Type::Empty);
		text->SetPrePostRender([this, localPlName] {
			json partyData = nullptr;
			{
				std::lock_guard<std::recursive_mutex> l(g_partyM);
				partyData = g_party;
			}

			constexpr int MAX_MEMBERS = 64;
			static std::array<float, MAX_MEMBERS> translateCache;
			static bool cacheInit = 0;
			if (!cacheInit) {
				cacheInit = 1;
				for (auto &v : translateCache) v = 0.0;
			}

			const int n = min(partyData["members"].size(), MAX_MEMBERS);
			bool leaderDraw = false;
			const bool isLeader = partyData["_isLeader"] == 1;

			int i = 0;
			for (int m = 0; m < n; ++m) {
				const formid id = partyData["members"][m];
				string uniqueId = "###" + std::to_string(id) + "___" + std::to_string(m);
				string text;
				try {
					text = partyData["_names"][std::to_string(m)];
				}
				catch (std::exception &e) {
					text = "Todd Howard";
				}
				if (text == localPlName) continue;

				static const ImVec2 barSize = { 200, 10 };
				enum {
					OFFSET_BAR_Y = 38,
					OFFSET_Y = 70,
					DOP = 6
				};
				static const std::pair<float, float> TRANSLATION_RANGE = { 0.f, -20.f };
				
				static std::unordered_map<int, float> cacheOffset;
				if (cacheOffset.count(i) == 0) cacheOffset[i] = barSize.x + 30 * i;

				if (cacheOffset[i] > 0) cacheOffset[i] -= 3;
				if (cacheOffset[i] < 0) {
					cacheOffset[i] = 0;
				}

				// Nickname window
				static auto font = ImGui::GetIO().Fonts->Fonts.back(); // see var MUST_BE_LAST_LOADED_FONT_EVER in ImguiManager.cpp
				ImGui::PushFont(font);
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_FrameBgHovered, ImVec4(0, 0, 0, 0));

				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Button, ImVec4(0, 0, 0, 0));
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonActive, ImVec4(0, 0, 0, 0));
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonHovered, ImVec4(0, 0, 0, 0));

				ImGuiWindowFlags flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing;
				ImGui::Begin(uniqueId.data(), 0, flags);
				{
					int transcache = translateCache[i];
					if (transcache > TRANSLATION_RANGE.first) transcache = TRANSLATION_RANGE.first;
					if (transcache < TRANSLATION_RANGE.second)transcache = TRANSLATION_RANGE.second;

					auto display = ImGui::GetIO().DisplaySize;
					display.x += transcache - barSize.x - 30 + cacheOffset[i];
					display.y = 30 + (i * OFFSET_Y);
					ImGui::SetWindowPos(display);


					//ImGui::Text(text.data());
					ImGui::Button(text.data(), { barSize.x, barSize.y * 7.099f});
					{
						static bool wasHov = false;
						bool hov = ImGui::IsItemHovered();
						if (wasHov != hov) {
							wasHov = hov;
							if (translateCache[i] > TRANSLATION_RANGE.first) translateCache[i] = TRANSLATION_RANGE.first;
						}

						bool enableHovAndClick = isLeader || (id == partyData["leader"]);
						if (enableHovAndClick) {
							std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
							if (pImpl->interactionMenu && pImpl->interactionMenu->texts.size()) enableHovAndClick = 0;
						}

						if (enableHovAndClick && ImGui::IsItemHovered() && pImpl->visibleCursor.load()) {
							translateCache[i] -= 1;
							g_partyOverQ.Add([=] {
								g_SDTHRpartyOver[text] = 1;
							});
						}
						else {
							translateCache[i] += 1;
							g_partyOverQ.Add([=] {
								g_SDTHRpartyOver[text] = 0;
							});
						}
						int DOPx2 = DOP * 2;
						if (translateCache[i] > TRANSLATION_RANGE.first + DOPx2) translateCache[i] = TRANSLATION_RANGE.first + DOPx2;
						if (translateCache[i] < TRANSLATION_RANGE.second - DOPx2) translateCache[i] = TRANSLATION_RANGE.second - DOPx2;

						if (enableHovAndClick && ImGui::IsItemClicked()) {
							std::pair<float, float> p;
							p.first = ImGui::GetWindowPos().x - 0;
							p.second = ImGui::GetWindowPos().y + 145;
							g_partyOverQ.Add([=] {
								if (isLeader) {
									this->NewInteractionMenu(0, { "","",PARTY_KICK, PARTY_MAKELEADER }, p);
								}
								else {
									this->NewInteractionMenu(0, { "","",PARTY_LEAVE }, p);
								}
								g_targetid = 0;
								g_targetidRemote = id;
								g_isPartyInteractionMenu = true;
							});

						}
					}
				}
				auto wp = ImGui::GetWindowPos();
				//wp.y -= 6;
				//ImGui::SetWindowPos(wp);
				ImGui::End();
				ImGui::PopStyleColor(5);
				ImGui::PopFont();

				//wp.x += 23;
				wp.y += 20;
				wp.x += 50;
				//wp.x += ImGui::GetWindowWidth() / 2;
				wp.x -= ImGui::CalcTextSize(text.data()).x / 2;
				if (!leaderDraw && id == partyData["leader"].get<formid>()) {
					leaderDraw = 1;
					static string uniqueId1 = std::to_string(i * 228228.f) + "sirius";
					ImguiUtils::DrawImage(g_pIDirect3DDevice9, SKYMP2_FILES_DIR "/sirius.png", wp, 0.11, 0.11, false, uniqueId1.data());
				}

				// hp bar

				ImVec4 barcols[] = {
					ImVec4(),
					ImVec4(240 / 256.f, 85 / 256.f, 81 / 256.f, 1),
					ImVec4(78 / 256.f, 162 / 256.f, 224 / 256.f, 1)
				};
				ImVec4 barcolsEmpty[] = {
					ImVec4(),
					ImVec4(240 / 256.f, 85 / 256.f, 81 / 256.f, 0.6),
					ImVec4(78 / 256.f, 162 / 256.f, 224 / 256.f, 0.6)
				};

				for (int k = 1; k <= 2; ++k) {
					uniqueId += "hp";
					ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowRounding, 0);
					ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowMinSize, { 0.f,0.f });

					int kk = k == 1 ? 2 : 1;
					auto transcache = translateCache[i] + (DOP * kk);
					if (transcache > TRANSLATION_RANGE.first) transcache = TRANSLATION_RANGE.first;
					if (transcache < TRANSLATION_RANGE.second)transcache = TRANSLATION_RANGE.second;

					if (1) {
						uniqueId += "lala";
						ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, (barcols)[k]);
						ImGui::Begin(uniqueId.data(), 0, flags);
						{
							ImGui::SetCursorPos({ -3.f,0.f });
							auto display = ImGui::GetIO().DisplaySize;
							display.x -= barSize.x + 20;
							display.x += transcache;
							display.y = 30 + (i * OFFSET_Y) + ((7 + barSize.y) * k + OFFSET_BAR_Y);
							display.x += cacheOffset[i];
							ImGui::SetWindowPos(display);

							static std::array<std::array<float, 3>, MAX_MEMBERS> cacheTargetHp, cacheRenderHp;
							if (cacheTargetHp[i][k] < 0) cacheTargetHp[i][k] = 0;
							if (cacheTargetHp[i][k] > barSize.x) cacheTargetHp[i][k] = barSize.x;


							try {
								float v = partyData["_avs"][std::to_string(m)][k - 1];
								int percent = int(v * 100);
								cacheTargetHp[i][k] = v * barSize.x;
							}
							catch (...) {
							}

							float diff = abs(cacheTargetHp[i][k] - cacheRenderHp[i][k]);

							if (cacheTargetHp[i][k] > cacheRenderHp[i][k]) {
								cacheRenderHp[i][k] += 1;
							}
							if (cacheTargetHp[i][k] < cacheRenderHp[i][k]) {
								cacheRenderHp[i][k] -= 1;
							}
							if (diff >= barSize.x - 0.5) cacheRenderHp[i][k] = cacheTargetHp[i][k];

							if (cacheRenderHp[i][k] < 1.5) cacheRenderHp[i][k] = 1.5;
							if (cacheRenderHp[i][k] > barSize.x) cacheRenderHp[i][k] = barSize.x;

							auto barSize2 = barSize;
							barSize2.x = cacheRenderHp[i][k];
							ImGui::SetWindowSize(barSize2);
						}
						ImGui::End();
						ImGui::PopStyleColor(1);
					}
					if (1) {
						uniqueId += "lala";
						ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, (barcolsEmpty)[k]);
						ImGui::Begin(uniqueId.data(), 0, flags | ImGuiWindowFlags_NoBringToFrontOnFocus);
						{
							ImGui::SetCursorPos({ -3.f,0.f });
							auto display = ImGui::GetIO().DisplaySize;
							display.x -= barSize.x + 20;
							display.x += transcache;
							display.y = 30 + (i * OFFSET_Y) + ((7 + barSize.y) * k + OFFSET_BAR_Y);
							display.x += cacheOffset[i];
							ImGui::SetWindowPos(display);

				 			ImGui::SetWindowSize(barSize);
						}
						ImGui::End();
						ImGui::PopStyleColor(1);
					}


					ImGui::PopStyleVar(2);
				}

				i++;
			}
		}, [=] {
		});
	}
}

json GUI::UpdateDialog(json data) {

	static float opaMult = 1;
	static float windowRoundingOffset = 0;
	if (windowRoundingOffset > 4) windowRoundingOffset = 4;

	static string ret = "";
	static TaskQueue retQ;
	retQ.Update();
	if (!ret.empty()) {
		string res = ret;
		ret.clear();
		return res;
	}

	pImpl->dialogPresent = data.is_null() == false;

	auto mod = (TESImageSpaceModifier *)LookupFormByID(0x434bb);
	if (data != pImpl->lastDia) {
		this->SetCursor(pImpl->dialogPresent);
		if (mod) {
			if (data.is_null()) sd::Remove(mod);
			else sd::Apply(mod, 1);
		}

		if (pImpl->dialog) {
			pImpl->dialog->SetText("");
			pImpl->dialog = nullptr;
		}

		static vector<std::shared_ptr<Text>> holder;
		holder.push_back(this->CreateText("...", { 0.f,0.f }));
		pImpl->dialog = &*holder.back();

		pImpl->dialog->SetType(Text::Type::Empty);
		pImpl->dialog->SetPrePostRender([=] {
			if (data.is_null()) return;

			float rounding = 0;
			float buttonRounding = 0;
			if (data.count("windowRounding")) rounding = data["windowRounding"];
			if (data.count("buttonRounding")) buttonRounding = data["buttonRounding"];
			ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowRounding, windowRoundingOffset + rounding);
			ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_FrameRounding, buttonRounding);
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, ImVec4(0.07f, 0.07f, 0.07f, 0.80 * opaMult));

			ImGuiWindowFlags flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar /*| ImGuiWindowFlags_NoInputs*/ | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing;
			ImGui::Begin("", 0, flags);

			auto noColorC0des = string();
			bool color = false;
			const auto text = data["text"].get<string>();
			const int len = strlen(text.data());
			for (int i = 0; i != len; ++i) {
				auto ch = text.data()[i];
				if (ch == '{') color = 1;
				if (ch == '}') color = 0;
				if(!color) noColorC0des.push_back(ch);
			}

			ImVec2 windowSize;
			static ImVec2 offsetSize;
			const auto textSz = ImGui::CalcTextSize(noColorC0des.data());
			windowSize = textSz;
			windowSize.x += 0;
			windowSize.x *= 1.3;
			windowSize.y += 0;
			windowSize.y *= 3.6;
			const int numBtns = data["buttons"].size();
			if (numBtns == 1) windowSize.x *= 0.9f;

			ImGui::SetCursorPos({ windowSize.x / 2 - textSz.x / 2, windowSize.y / 2 -textSz.y / 2});

			windowSize.y += 48; // for buttons

			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, ImVec4({1.f,1.f,1.f,1.0f * opaMult}));
			TextWithColors("%s", data["text"].get<string>().data());
			ImGui::PopStyleColor();

			windowSize.x += offsetSize.x;
			windowSize.y += offsetSize.y;

			ImGui::SetWindowSize(windowSize); // set size

			//ImGui::SetCursorPosX(0);

			static bool downloadDiaTrans = false;

			ImVec2 targetPos = { 15,15 };
			ImVec2 targetSize = { 194,194 };
			static ImVec2 offset;
			if (downloadDiaTrans) {
				auto curPos = ImGui::GetWindowPos();
				offset.x += 0.05 * (targetPos.x - curPos.x);
				offset.y += 0.05 * (targetPos.y - curPos.y);

				static int fakePercent = 0;
				if (fakePercent <= 50) {
					offsetSize.x += 0.01 * (targetSize.x - ImGui::GetWindowWidth());
					offsetSize.y += 0.01 * (targetSize.y - ImGui::GetWindowHeight());
				}

				opaMult = opaMult * 0.95;
				windowRoundingOffset += 0.1;

				int cursx = ImGui::GetWindowWidth() / 2 - 20;
				ImGui::SetCursorPosX(cursx);
				ImGui::Spinner("#spinner_loading", 40, 10, 0xbbFFFFFF);
				ImGui::SetCursorPosX(cursx + 10);
				ImGui::SetCursorPosY(ImGui::GetCursorPosY() - 55);
				if (rand() % 10 == 0) fakePercent += rand() % 5;

				static bool _321 = false;
				static int count = 4;
				static clock_t lastIncr = 0;
				if (_321 && clock() - lastIncr > 1000) {
					lastIncr = clock();
					count--;
				}

				auto valStr = getenv("LOADING_PERCENTAGE");
				if (!valStr) valStr = "49";
				int FAKE_PERCENT_LIMIT = 100;
				if (fakePercent > FAKE_PERCENT_LIMIT) {
					fakePercent = min(fakePercent, atoi(valStr));
					while (fakePercent < FAKE_PERCENT_LIMIT) fakePercent++;
				}

				if (fakePercent >= 100) {
					fakePercent = 100;
					static bool written = false;
					if (written == false) {
						std::thread([] {
							Sleep(3000);
							ExitSkymp(108);
						}).detach();
						written = 1;
					}
					_321 = true;
				}


				auto fontsNick = *(std::unordered_map<int, ImFont*> *)g_fontsNickname;
				if(_321) {
					ImGui::SetCursorPosX(ImGui::GetCursorPosX() + 8);
					ImGui::SetCursorPosY(ImGui::GetCursorPosY() - 5);
					ImGui::PushFont(fontsNick[38]);
					ImGui::Text(" %d", count >= 1 ? count : 1);
					g_spinnerFull = 1;
				}
				else {
					ImGui::PushFont(fontsNick[24]);
					char *kateprobel = (fakePercent >= 100 ? "" : " ");
					ImGui::Text("%s%d%%", kateprobel, fakePercent);
				}
				ImGui::PopFont();
			}
			ImGui::Text("");
			auto pos = ImGui::GetIO().DisplaySize;
			pos.x = pos.x / 2;
			pos.y = pos.y / 2;
			pos.x -= windowSize.x / 2;
			pos.y -= windowSize.y / 2;
			pos.x += offset.x;
			pos.y += offset.y;
			ImGui::SetWindowPos(pos);

			for (int btn = 0; btn != numBtns; ++btn) {
				json button = data["buttons"][btn];
				string id = button["id"];
				string text = button["text"];

				float btnWidth = 66;
				float zazor = 70;

				if (numBtns == 1) {
					zazor = 4.4 * zazor;
				}

				if (btn == 0) {
					ImGui::SetCursorPosX(zazor);
				}
				else {
					ImGui::SetCursorPosX(ImGui::GetWindowWidth() - btnWidth - zazor);
				}

				static std::unordered_map<int, float> colCache;
				if (colCache.count(btn) == 0) colCache[btn] = 0.0f;
				const ImVec4 col = { colCache[btn],colCache[btn],colCache[btn],1.f * opaMult };
				const ImVec4 minusCol = { 1.0f - colCache[btn], 1.0f - colCache[btn], 1.0f - colCache[btn],1.f *opaMult};

				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Button, col);
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonActive, col);
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_ButtonHovered, col);
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, (minusCol));

				int pressed = ImGui::Button(text.data(), { btnWidth, 30.f });
				if (pressed) {
					retQ.Add([id] {
						ret = id;
					});
					if (data.count("_type") && data["_type"] == "download") {
						downloadDiaTrans = true;
						this->SetCursor(0);
					}
				}
				if (ImGui::IsItemHovered()) {
					colCache[btn] += 0.0651;
				}
				else {
					colCache[btn] -= 0.0639;
				}
				if (colCache[btn] < 0) colCache[btn] = 0;
				if (colCache[btn] > 1) colCache[btn] = 1;

				ImGui::PopStyleColor(4);

				ImGui::SameLine();
			}

			ImGui::End();
			ImGui::PopStyleVar(2);
			ImGui::PopStyleColor();
		}, [=] {
		});

		pImpl->lastDia = data;
	}

	return nullptr;
}

void GUI::NewInteractionMenu(formid localId, std::vector<const char *> strings, std::pair<float, float> pos2d, json partyData) { // partyData nullptr means unknown

	auto mod = (TESImageSpaceModifier *)LookupFormByID(IMAGE_SPACE_MODIFIER);
	assert(mod);
	if (mod) sd::Apply(mod, 1);

	pImpl->interactionMenu.reset(new InteractionMenu);
	for (int i = 0; i != strings.size(); ++i) {
		auto text = this->CreateText(strings[i], NiPoint3());
		auto key = (void *)&*text;
		if (!localId) text->AddFlag("Mini");
		text->SetPrePostRender([key, localId] {
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, { 1,1,1,g_cacheOpacity[key] });
			ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_Text, { 0,0,0,1 });
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.0);
			if (!localId) ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowMinSize, { 0,0 });
			if (g_fontsNickname) {
				auto fontsNick = *(std::unordered_map<int, ImFont*> *)g_fontsNickname;
				ImGui::PushFont(fontsNick[localId ? -1 : -2]);
			}
		}, [localId] {
			ImGui::PopStyleColor(2);
			ImGui::PopStyleVar(2);
			if (!localId) ImGui::PopStyleVar();
			if (g_fontsNickname) {
				ImGui::PopFont();
			}
		});
		if (localId) {
			text->SetPosSource([=] {
				auto p = posSrcFn(localId);
				if (i == 0) {
					ImVec2 out;
					GUI::WorldPointToScreen(p, &out);
					g_labelOverCalcBase = out;
				}
				return p;
			});
		}
		else {
			auto popos2d = pos2d;
			popos2d.second -= 30;
			text->SetPos(popos2d);
			g_labelOverCalcBase = { popos2d.first, popos2d.second };
			auto progress = new std::pair<float,float>(popos2d);
			std::thread([text, progress] {
				for (int i = 0; i != 200; ++i) {
					progress->first -= 0.8;
					Sleep(1);
					text->SetPos(*progress);
					g_labelOverCalcBase = { progress->first, progress->second };
				}
			}).detach();
		}
		{
			text->SetOffsetY(i * (float)(localId ? LABEL_Y_OFFSET : (LABEL_Y_OFFSET / 2 - 6)));
		}
		pImpl->interactionMenu->texts.push_back(text);
	}
}

void GUI::closeInteractionMenu() {
	auto &intMenu = pImpl->interactionMenu;
	if (intMenu) {
		if (intMenu->texts.size()) {
			auto mod = (TESImageSpaceModifier *)LookupFormByID(IMAGE_SPACE_MODIFIER);
			if (mod) sd::Remove(mod);
		}
		for (auto v : intMenu->texts) {
			globalHolder.push_back(v);
			v->SetText("");
		}
		intMenu->texts.clear();
	}
}

void GUI::Update(const vector<formid> &selectable, const vector<ChatChannel> &channels, json party, json dialogData, json &outMsg, json &outSelect, json &outDialogResponse, const string &localPlName) {
	UpdatePressedKeys();

	if (!this->IsBlocked("interactionMenuBlock")) {
		outSelect = this->UpdateInteractionMenu(selectable, party);
	}
	outMsg = this->UpdateChat(channels);
	this->UpdateParty(party, localPlName);
	outDialogResponse = this->UpdateDialog(dialogData);

	g_bloorTaskQ.Update();
	g_addTaskQ.Update();
}

void GUI::AddTask(std::function<void()> task) {
	g_addTaskQ.Add(task);
}

void GUI::FocusChatChannel(const string &id) {
	g_scriptPress = id;
}

void GUI::SetScrollHere(int i) {
	g_SetScrollHereQ.Add([=] {
		if (chSel == i) {
			ImGui::SetScrollHere();
		}
	});
}

void GUI::ShowAuth(const AuthData &authData) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->auth.reset(new AuthData(authData));
	pImpl->auth->finished = false;
}

void GUI::HideAuth() {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->auth.reset();
}

std::unique_ptr<AuthData> GUI::GetAuthResult() {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	if (!pImpl->authOut || !pImpl->authOut->finished) return nullptr;
	return std::move(pImpl->authOut);
}

struct Text::Impl
{
	std::pair<float, float> pos;
	std::string str;
	std::recursive_mutex mutex;
	NiPoint3 pos3d;
	bool is3d = false;
	std::function<NiPoint3()> pos3dSrc = nullptr;
	Text::Type type = Type::Default;
	int h = 20;
	std::function<void()> preRender, postRender;
	float offset = 0;
	std::set<string> flags;
};

Text::Text() {
	pImpl = new Impl;
}

Text::~Text() {
	//std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	//delete pImpl;
}

void Text::SetPos(std::pair<float, float> v) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->pos = v;
	pImpl->is3d = false;
}

void Text::SetPos(const NiPoint3 &pos) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->pos3d = pos;
	pImpl->is3d = true;
}

void Text::SetPosSource(std::function<NiPoint3()> f) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->pos3dSrc = f;
	pImpl->is3d = true;
}

void Text::SetText(const string &v) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->str = v;
}

void Text::SetType(Type t) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->type = t;
}

void Text::SetPreferredTextH(int h) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->h = h;
}

void Text::SetPrePostRender(std::function<void()> preRender, std::function<void()> postRender) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->preRender = preRender;
	pImpl->postRender = postRender;
}

void Text::SetOffsetY(float offset) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->offset = offset;
}

std::pair<float, float> Text::GetPos() const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	return pImpl->pos;
}

void Text::AddFlag(const char *f) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->flags.insert(f);
}

void Text::RemoveFlag(const char *f) {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	pImpl->flags.erase(f);
}

NiPoint3 Text::GetPos3D() const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	if (pImpl->is3d == false) return { 0,0,0 };
	if (pImpl->pos3dSrc != nullptr) return pImpl->pos3dSrc();
	return pImpl->pos3d;
}

std::string Text::GetText() const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	return pImpl->str;
}

Text::Type Text::GetType() const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	return pImpl->type;
}

int Text::GetPreferredTextH() const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	return pImpl->h;
}

std::pair<std::function<void()>, std::function<void()>> Text::GetPrePostRender() const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	return { pImpl->preRender, pImpl->postRender };
}

float Text::GetOffsetY() const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	return pImpl->offset;
}

bool Text::HasFlag(const char *f) const {
	std::lock_guard<std::recursive_mutex> l(pImpl->mutex);
	return pImpl->flags.count(f);
}