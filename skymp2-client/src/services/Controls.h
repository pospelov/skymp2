#pragma once

// All methods must be threadsafe
class Controls
{
public:
	enum Control
	{
		Movement = 0,
		Fighting = 1,
		CamSwitch = 2,
		Looking = 3,
		Sneaking = 4,
		Menu = 5,
		Activate = 6,
		JournalTabs = 7,
		SaveGame = 8,
		Wait = 9,
		FastTravel = 10,
		//Console = 11,
		BeastForm = 12,
		COUNT = 13,
	};

	void SetEnabled(Control control, bool enabled, std::string srcsystem);
	void RequestApply() { std::lock_guard<std::mutex> l(m); needApply = true; }

	void Update();

private:
	enum {
		MIN_ENABLED_STATE = 0
	};
	std::mutex m;
	std::vector<int64_t> state = std::vector<int64_t>(Control::COUNT, MIN_ENABLED_STATE);
	std::map<std::string, std::set<Control>> disabled;
	bool needApply = true;
	bool needPressWasd = false;
};