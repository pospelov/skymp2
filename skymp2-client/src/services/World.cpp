#include "../stdafx.h"
#include "World.h"

#include "../TaskQueue.h"
#include "../sdutil/FindRefs.h"

#include "../components/SmartActor.h"
#include "../components/MIPlayer.h"
#include "../components/IILocalPlayer.h"
#include "../components/PersistentRef.h"

#include "../gui/Localization.h"

#include <unordered_set>

enum : uint8_t {
	CONTENT_ANIMEVENT = 1,
	CONTENT_MOVEMENT = 2, // only internal use
	CONTENT_ERASE_FOREVER = 3,
	CONTENT_FORCE_ANIMEVENT = 4,

	// >= 10 && <= 99 - reserved for Net.cpp
	// >= 100 - reserved for Lua scripts
};

std::recursive_mutex g_aeListMutex;
vector<string> g_aeList;
vector<std::pair<formid, string>> g_otherAeList;
std::unordered_set<void *> g_blockingPlayers;

TaskQueue g_tasks;

std::atomic<DWORD> idleOkThreadId = 0;

bool IsSyncBackAE(const char *data) {
	return !strcmp(data, "NPC_BumpedFromBack")
		|| !strcmp(data, "NPC_BumpFromFront")
		|| !strcmp(data, "NPC_BumpedFromRight")
		|| !strcmp(data, "NPC_BumpedFromLeft")
		|| !strcmp(data, "staggerStart")
		|| !strcmp(data, "blockAnticipateStart")
		|| !strcmp(data, "blockHitStart")
		|| !strcmp(data, "StaggerPlayer")
		|| !strcmp(data, "FlinchStart");
}

void KnockEffect(const NiPoint3 &pos) {
	Actor *ac = nullptr;
	for (int i = 0; i < 10; ++i) {
		ac = sd::FindRandomActor(pos.x, pos.y, pos.z, 256);
		if (ac && ac != sd::GetPlayer()) break;
	}
	if (ac) sd::KnockAreaEffect(ac, 1, 30);
}

void OnSyncBackAE(Actor *actor, const string &aeName) {
	if (aeName == "NPC_BumpedFromBack" || aeName == "NPC_BumpFromFront" || aeName == "NPC_BumpedFromRight" || aeName == "NPC_BumpedFromLeft") {
		bool isFirstPerson = papyrusGame::GetCameraState(nullptr) == 0;
		if (isFirstPerson) {
			KnockEffect(actor->pos);
		}
		static std::map<string, float> damage = {
			{ "NPC_BumpedFromBack", 10.f }, { "NPC_BumpFromFront", 7.5f }, { "NPC_BumpedFromRight", 5.f }, { "NPC_BumpedFromLeft", 5.f }
		};
		sd::DamageActorValue(actor, "Stamina", damage[aeName]);
	}
	else if (aeName == "staggerStart") {
		if (!sd::Obscript::IsBlocking(actor)) {
			KnockEffect(actor->pos);
		}
	}
}

extern "C" {
	__declspec(dllexport) BSFixedString *SKYMP2_SendAnimationEvent(TESObjectREFR *refr, BSFixedString *animEventName) {
		string lower = animEventName->data;
		std::transform(lower.begin(), lower.end(), lower.begin(), tolower);

		static std::set<string> badAnims = { "movestart", "movestop", "sprintstart", "sprintstop", "turnright", "turnleft", "turnstop", "cyclicfreeze", "cycliccrossblend" };
		if (badAnims.count(lower)) return animEventName;

		formid id = refr ? refr->formID : 0;
		if (id == 0x14) {
			std::lock_guard<std::recursive_mutex> l(g_aeListMutex);
			g_aeList.push_back(animEventName->data);
		}
		if (id > 0xff000000) {

			const bool notBlockSneakOrSprint = strcmp(animEventName->data, "SneakStop") 
				&& strcmp(animEventName->data, "blockStop") 
				&& strcmp(animEventName->data, "SprintStop");
			if (notBlockSneakOrSprint) {
				//Console_Print("%x %s", id, animEventName->data);

				if (IsSyncBackAE(animEventName->data)) {
					//Console_Print("kek");

					if (animEventName->data == string("staggerStart")) {
						bool isBlocking = false;
						{
							std::lock_guard<std::recursive_mutex> l(g_aeListMutex);
							isBlocking = g_blockingPlayers.count(refr) != 0;
						}
						Console_Print("isBlocking = %d", isBlocking);
						if (isBlocking) {
							static BSFixedString doNothing = "";
							return &doNothing;
						}
						else {
							// Can't sync stagger properly. Disable
							static BSFixedString doNothing = "";
							return &doNothing;
						}
					}

					std::lock_guard<std::recursive_mutex> l(g_aeListMutex);
					g_otherAeList.emplace_back(id, animEventName->data);
				}
			}
			if (!memcmp(animEventName->data, "IdleCombat*", 10)) {
				static BSFixedString doNothing = "";
				return &doNothing;
			}

			static std::set<formid> g_banLight; // TODO: non-static

			if (string(animEventName->data) == "IdleChairVar1") {
				g_banLight.insert(id);
			}

			auto ae = string(animEventName->data);
			if (idleOkThreadId != GetCurrentThreadId() && ae != "IdleStop" && ae != "IdleForceDefaultState" && (!strcmp(animEventName->data, "MotionDrivenIdle") || !memcmp(animEventName->data, "Idle*", 4))) {
				static BSFixedString defState = "IdleForceDefaultState";
				static BSFixedString emptyFs = "";
				if (g_banLight.count(id)) {
					return &emptyFs;
				}
				else {
					return &defState;
				}
			}
		}
		return animEventName;
	}
}

enum {
	CHAT_BUBBLE_DISPELL_MS = 4000,
	NICKNAME_DISTANCE_METRES = 10,
	NICKNAME_DISTANCE_METRES_SNEAKING = 3,
};

inline NiAVObject * ResolveNode(TESObjectREFR * obj, BSFixedString nodeName, bool firstPerson) {
	if (!obj) return NULL;
	NiAVObject	* result = (NiAVObject	*)obj->GetNiNode();
	// special-case for the player, switch between first/third-person
	PlayerCharacter * player = obj == *g_thePlayer ? *g_thePlayer : nullptr;
	if (player && player->loadedState)
		result = firstPerson ? player->firstPersonSkeleton : player->loadedState->node;
	// name lookup
	if (nodeName.data[0] && result)
		result = result->GetObjectByName(&nodeName.data);
	return result;
}
inline NiPoint3 GetNodeWorldPosition(TESObjectREFR *obj, BSFixedString nodeName, bool firstPerson) {
	NiAVObject	*object = ResolveNode(obj, nodeName, firstPerson);
	return object ? object->m_worldTransform.pos : NiPoint3(0, 0, 0);
}

inline bool IsHostable(uint8_t formType) {
	switch (formType) {
	case kFormType_Ammo:
	case kFormType_Armor:
	case kFormType_Book:
	case kFormType_Ingredient:
	case kFormType_Key:
	case kFormType_Misc:
	case kFormType_Potion:
	case kFormType_SoulGem:
	case kFormType_Weapon:
	case kFormType_MovableStatic:
	case kFormType_ScrollItem:
	case kFormType_Light:
		return true;
	default:
		return false;
	}
}

inline bool IsTorch(TESForm *form) {
	return form->formID == 0x0001D4EC;
}

class EntityFactory
{
public:
	explicit EntityFactory(WorldCleaner *wc_) : wc(wc_) {}

	SmartRef *CreatePlayer() {
		auto mi = std::shared_ptr<IMovementInterpolator>(new MIPlayer);
		return new SmartActor({ 0,0,0 }, 4096, *wc, mi);
	}

	SmartRef *CreateObject() {
		auto mi = std::shared_ptr<IMovementInterpolator>(new MIPlayer);
		auto base = LookupFormByID(ID_AlchemyItem::FoodCabbage);
		assert(base);
		return new SmartRef(base, { 0,-100000000,0 }, 4096, *wc, mi);
	}

	WorldCleaner * const wc;
};

struct Cache
{
	clock_t lastSendEq = 0;
	std::map<formid, clock_t> lastSend;
	Equipment lastEq;
	Look lastLook;
	// TODO: unordered_map ?
	std::map<formid, Movement> lastMov;
	bool contWasOpen = false;
	std::map<string, int> usedChannelNames;
	std::map<formid, string> names;
	std::map<formid, Look> lookNonEmpty;
	std::set<formid> addedMarkers;
	vector<formid> lastMarkersFromServer = { formid(-1) };
	bool neverResurrected = true;
};

struct BubbleDispellTask
{
	string text;
	formid authorId = 0;
	clock_t dispellMoment = 0;
};

// Set Texts to "" and move here in World::Impl dtor if you want Skyrim to keep working!
vector<std::shared_ptr<Text>> g_textStorage;

json g_dateTime;
clock_t g_dateTimeRecvMoment = 0;

struct UpdData
{
	clock_t movement = 0, markAlive = 0;

	clock_t any() const noexcept {
		return max(movement, markAlive);
	}
};

struct World::Impl
{
	Impl(WorldCleaner *wc) : factory(wc) {}

	std::map<formid, std::shared_ptr<IRef>> entities, debugEntities;

	std::set<formid> receivedMovement;
	std::unordered_map<formid, UpdData> lastUpd;
	std::unordered_set<formid> updMovIgnore;
	std::set<formid> afkPlayers;
	clock_t lastMovUpdG = 0;
	clock_t lastMovUpdGFF = 0;
	TaskQueue tasks, tasks1000msDelay, beforeErasePlayers;
	std::list<EmitTask> emitTasks;
	std::pair<userid, formid> myIds = { ~0,0 };
	EntityFactory factory;
	bool debug = false;
	clock_t ctorMoment = clock();
	Look pcLook;
	std::list<InvEvent> ies;
	bool pcEquipmentUpdated = false;
	std::string localPlName;
	formid activatedCont = 0;
	bool downloadOk = false;
	clock_t lastReq = 0;
	json &dateTime = g_dateTime;
	clock_t &dateTimeRecvMoment = g_dateTimeRecvMoment;
	bool hourSet = false;
	float minTimescale = 1000000;
	string lastPcAe;
	std::set<formid> nicknameInvisible;
	std::unordered_map<formid, std::function<void()>> afkDispellFn;
	uint64_t invNumOpens = 0;
	bool invOpenWas = false;
	void *lastPcCell = nullptr;

	void clearAfk(formid owner, Actor *ac) {
		auto it = afkDispellFn.find(owner);
		if (it != afkDispellFn.end()) {
			if (it->second) it->second.operator()();
			afkDispellFn.erase(it);
			if (ac) sd::EnableAI(ac, true);
		}
	}

	std::unordered_map<formid, clock_t> spawnMoment;

	std::unordered_map<formid, std::shared_ptr<Text>> nicknames;
	std::unordered_map<formid, std::shared_ptr<Text>> chatBubbleTexts; // Always exist (if entity present) but text set to "" when no message
	std::unordered_map<formid, string> chatBubbleSrc;
	vector<BubbleDispellTask> bubbleDispellTasks;
	json partyData = nullptr;
	json dialogData = nullptr;

	std::unique_ptr<IILocalPlayer> pcInventoryControl;

	Cache cache;
	GUI *gui = nullptr;

	vector<ChatChannel> chatChannels;
	std::set<string> whisperPlayers;

	~Impl() {
		for (auto p : nicknames) {
			p.second->SetText("");
			g_textStorage.push_back(p.second);
		}
		for (auto p : chatBubbleTexts) {
			p.second->SetText("");
			g_textStorage.push_back(p.second);
		}
	}
};

void World::UpdateMovement(formid id, Movement mov, formid baseId, formid hostId, uint32_t count) {
	this->BroadcastIn(id, CONTENT_MOVEMENT, 
		std::to_string(mov._pos.x) + ' ' + 
		std::to_string(mov._pos.y) + ' ' + 
		std::to_string(mov._pos.z));

	auto c = clock();
	pImpl->beforeErasePlayers.Add([=] {
		if (!pImpl->debug && id == pImpl->myIds.second) return;
		pImpl->lastUpd[id].movement = c;
	});
	pImpl->tasks1000msDelay.Add([=] {
		if (pImpl->updMovIgnore.count(id)) return;
		pImpl->lastMovUpdG = clock();
		auto mov_ = mov;
		bool isFF = id >= 0xFF000000;
		if (isFF) pImpl->lastMovUpdGFF = clock();

		assert(id > 0);
		if (id == pImpl->myIds.second) {
			if (!pImpl->debug) return;
			mov_.SetPos(mov.Pos() += {128, 128, 0});
		}

		if (isFF) {
			if (pImpl->entities.count(id) == 0) {
				SmartRef *newRef = nullptr;
				if (baseId) {
					newRef = pImpl->factory.CreateObject();
					auto baseForm = LookupFormByID(baseId);
					assert(baseForm);
					if (baseForm) newRef->SetBaseForm(baseForm);
					newRef->SetCount(count);
				}
				else {
					if (pImpl->cache.lookNonEmpty.count(id) == 0) return;
					newRef = pImpl->factory.CreatePlayer();
					if (mov._lastAe.size()) {
						std::thread([=] {
							Sleep(300);
							const string ae = mov._lastAe;
							g_tasks.Add([=] {
								this->BroadcastIn(id, CONTENT_ANIMEVENT, "SKYMP2_Invisible");
							});
							for (int i = 0; i != 4; ++i) {
								g_tasks.Add([=] {
									this->BroadcastIn(id, CONTENT_ANIMEVENT, ae);
								});
								Sleep(100);
							}
							Console_Print("Create with anim %s => %s", mov._lastAe.data(), ae.data());
							Sleep(2600);
							g_tasks.Add([=] {
								this->BroadcastIn(id, CONTENT_ANIMEVENT, "SKYMP2_Visible");
							});
						}).detach();
					}
				}
				assert(newRef);
				pImpl->entities[id].reset(newRef);
				pImpl->spawnMoment[id] = clock();

				assert(pImpl->gui);
				if (pImpl->gui) {
					pImpl->nicknames[id] = pImpl->gui->CreateText("", NiPoint3());
					pImpl->nicknames[id]->SetType(Text::Type::Nickname);
					pImpl->chatBubbleTexts[id] = pImpl->gui->CreateText("", NiPoint3());
					pImpl->chatBubbleTexts[id]->SetType(Text::Type::ChatBubble);
				}
			}
		}
		auto it = pImpl->entities.find(id);
		if (it != pImpl->entities.end()) {
			auto &entity = it->second;

			bool isHarvested = mov.IsOpen();
			auto outPos = entity->GetMovement()._pos;
			auto base = entity->FindBaseForm();

			bool freeze = (id >= 0xff000000 && baseId); // Freeze all 0xFF items
			if (freeze) {
				auto ref = entity->FindForm();
				if (ref && ref->formType == kFormType_Reference) {
					constexpr int keyframed = 4;
					sd::SetMotionType(ref, keyframed, false);
				}
			}

			bool out = !freeze && !isHarvested
				&& hostId != (formid)-1
				&& (outPos - mov._pos).Length() < 200.0
				&& (!hostId || pImpl->myIds.second == hostId)
				&& !isFF
				&& base && base->formType != kFormType_Flora && base->formType != kFormType_Tree
				&& clock() - pImpl->spawnMoment[id] > 1500
				;
			if (out)
			{
				entity->SetMode(EntityMode::Out);
			}
			else {
				entity->SetMode(EntityMode::In);
				auto &movListener = dynamic_cast<IListener<Movement> &>(*entity);
				movListener.Push(mov_);
				pImpl->lastUpd[id].movement = clock();
			}
		}

		if (pImpl->debug && id != pImpl->myIds.second) {
			const bool startDebugForObj =
				(mov.IsGrabbed() || (mov._pos - sd::GetPosition(sd::GetPlayer())).Length() < 512)
				&& pImpl->entities.count(id) && pImpl->entities.at(id)->FindBaseForm() && pImpl->entities.at(id)->FindBaseForm()->formType == kFormType_Misc;
			if (pImpl->debugEntities.count(id) == 0 
				&& startDebugForObj) {
				auto tmp = (pImpl->factory.CreateObject());

				auto f = pImpl->entities[id]->FindBaseForm();
				assert(f);
				tmp->SetBaseForm(f);
				pImpl->debugEntities[id].reset(tmp);
			}
			if (pImpl->debugEntities.count(id)) {
				mov_.SetPos(mov.Pos() += {128, 128, 0});
				auto &movListener = dynamic_cast<IListener<Movement> &>(*pImpl->debugEntities.at(id));
				movListener.Push(mov_);
			}
		}
	});
}

void World::MarkAlive(formid id) {
	auto c = clock();
	pImpl->beforeErasePlayers.Add([=] {
		if (!pImpl->debug && id == pImpl->myIds.second) return;
		pImpl->lastUpd[id].markAlive = c;
	});
}

void World::UpdateEquipment(formid id, Equipment eq) {
	pImpl->tasks.Add([=] {
		if (pImpl->entities.count(id) > 0) {
			auto &eqListener = dynamic_cast<IListener<Equipment> &>(*pImpl->entities.at(id));
			eqListener.Push(eq);
		}
		if (id == pImpl->myIds.second && !pImpl->pcEquipmentUpdated) {
			if (Equipment_::ApplyToPlayer(eq)) {
				pImpl->pcEquipmentUpdated = true;
			}
		}
	});
}

void World::UpdateLook(formid id, Look look, string name) {
	pImpl->tasks.Add([=] {
		if (look.tints.size()) {
			pImpl->cache.lookNonEmpty[id] = look;
		}
		if (pImpl->entities.count(id) > 0) {
			auto &entity = pImpl->entities[id];
			const auto smartActor = dynamic_cast<SmartActor *>(&*entity);
			if (!smartActor) throw with_trace(std::logic_error("must be Actor"));
			smartActor->Push(look);
			if (name.size()) smartActor->PushName(name);
			assert(pImpl->nicknames[id]);
		}

		if (id == pImpl->myIds.second) {
			if (pImpl->pcLook != look) {
				pImpl->pcLook = look;
				Look_::ApplyToPlayer(look);
				if (pImpl->cache.neverResurrected) {
					//sd::Resurrect(sd::GetPlayer()); // Crash after death
					pImpl->cache.neverResurrected = 1;
				}
			}
		}
	});
}

void World::UpdateInventory(formid id, Inventory inv) {
	pImpl->tasks.Add([=] {
		assert(id > 0);
		if (id == pImpl->myIds.second) {
			assert(pImpl->pcInventoryControl);
			pImpl->pcInventoryControl->PushInventory(Inventory(inv));
		}
		if (pImpl->entities.count(id)) {
			auto &invListener = dynamic_cast<IListener<Inventory> &>(*pImpl->entities[id]);
			invListener.Push(inv);
		}
	});
}

void World::SetMyId(userid id, formid fid) {
	pImpl->tasks.Add([=] {
		pImpl->myIds = { id, fid };
		pImpl->entities.erase(fid);
	});
}

void World::Activate(formid target) {
	pImpl->tasks.Add([=] {
		auto refr = (TESObjectREFR *)LookupFormByID(target);
		if (refr && refr->formType == kFormType_Reference) {
			sd::Activate(refr, sd::GetPlayer(), 1);
		}
	});
}

void World::AddItem(formid id, uint32_t count) {
	pImpl->tasks.Add([=] {
		auto form = LookupFormByID(id);
		if (form) sd::AddItem(sd::GetPlayer(), form, count, true);
	});
}

void World::EraseEntity(formid id) {
	pImpl->tasks.Add([=] {
		if (pImpl->entities.erase(id)) {
			pImpl->updMovIgnore.insert(id);
		}
	});
}

void World::BroadcastEraseEntity(formid id) {
	pImpl->emitTasks.push_back([id](INet &net) { 
		net.Broadcast(id, CONTENT_ERASE_FOREVER, "");
	});
}

void World::SetOpen(formid target, bool open) {
	pImpl->tasks.Add([=] {
		auto refr = (TESObjectREFR *)LookupFormByID(target);
		if (refr && refr->formType == kFormType_Reference) {
			sd::SetOpen(refr, open);
		}
	});
}

void World::ShowRaceMenu() {
	pImpl->tasks.Add([=] {
		pImpl->cache.lastLook = {};
		sd::ShowRaceMenu();
	});
}

void World::AddChannel(ChatChannel ch, bool allowOverride) {// addchatchannel
	pImpl->tasks.Add([=] {

		// Do not show doubling whisper chat channels
		bool isWhisper = !memcmp(ch.id.data(), "whisper", 7);
		if (isWhisper) {
			if (pImpl->whisperPlayers.count(ch.name) > 0) return;
			pImpl->whisperPlayers.insert(ch.name);
		}

		for (auto &thisch : pImpl->chatChannels) {
			if (thisch.id == ch.id) {
				if (allowOverride) {
					thisch = ch;
					// TODO: focus?
				}
				return; // Prevent doubling
			}
		}

		pImpl->chatChannels.push_back(ch);
		if (ch.focus) {
			assert(pImpl->gui);
			if (pImpl->gui)pImpl->gui->FocusChatChannel(ch.id);
		}
		else {
		}
	});
}

void World::RemoveChannel(string id) {
	pImpl->tasks.Add([=] {

		decltype(pImpl->chatChannels) newVec;
		for (auto ch : pImpl->chatChannels) {
			if (ch.id != id) { // Save channel
				newVec.push_back(ch);
			}
			else { // Delete channel
				// Forget player whisper
				bool isWhisper = !memcmp(id.data(), "whisper", 7);
				if (isWhisper) {
					pImpl->whisperPlayers.erase(ch.name);
				}
			}
		}
		pImpl->chatChannels = newVec;
	});
}

void World::AddChatMessage(string chId, string msg, string author, Movement mov) {
	if (msg.empty()) return;
	pImpl->tasks.Add([=] {
		Message toCommon;

		for (int i = 0; i != pImpl->chatChannels.size(); ++i) {
			auto &ch = pImpl->chatChannels[i];
			if (ch.id == chId) {
				if (chId == "rp" || chId == "nonrp" || chId == "trade") {
					float distanceMetres = (sd::GetPosition(sd::GetPlayer()) - mov._pos).Length() / 70;
					if (distanceMetres >= 0 && distanceMetres <= 10'000) {

						string formatted = author + ": " + msg;
						if (chId == "nonrp") {
							formatted = "(( " + formatted + " ))";
						}
						if (chId == "trade") {
							formatted = "{FFFFFF}" + author + ": {ED9B28}" + msg;
						}

						// Add chat bubble
						const formid eid = this->LookupEntityByName(author);
						if (eid != 0) {
							if (chId == "rp") this->AddChatBubble(eid, msg);
							else if (chId == "trade") this->AddChatBubble(eid, "        " "        " "        " "        " "{ED9B28}" + msg);
						}

						Message m;
						m.text = formatted;
						if (distanceMetres < 2) m.color = -1;
						else if (distanceMetres < 4) m.color = 0xe8e8e8ff;
						else if (distanceMetres < 6) m.color = 0xadadadff;
						else if (distanceMetres < 12) m.color = 0x878787ff;
						else if (distanceMetres < 15) m.color = 0x5e5e5eff;
						else return; // ignore if too big distance

						if (chId == "nonrp") {
							m.color = 0xbfbfbfff;
						}

						ch.messages.push_back(m);
						toCommon = m;
					}
					pImpl->gui->SetScrollHere(i);
				}
				if (!memcmp(chId.data(), "whisper:", (sizeof "whisper:") - 1)) {
					Message m;
					m.text = author + ": " + msg;
					m.color = 0xffb7b7ff;
					ch.messages.push_back(m);
					pImpl->gui->SetScrollHere(i);
				}
				if (chId == "party") {
					Message m;
					m.text = author + ": " + msg;
					m.color = 0x287affff;
					ch.messages.push_back(m);
					pImpl->gui->SetScrollHere(i);
				}
			}
		}

		if (toCommon.text.empty() == false) {
			for (auto &ch : pImpl->chatChannels) {
				if (ch.id == "common") {
					ch.messages.push_back(toCommon);
				}
			}
		}
	});
}

void World::SetPartyData(json data) {
	ChatChannel partyCh;
	partyCh.id = "party";
	partyCh.name = L_PARTY;
	partyCh.nameCaps = L_PARTY_CAPS;
	Message m; m.text = L_PARTY_WELCOME;
	partyCh.messages = { m };
	partyCh.focus = 1;
	if (data.is_object() && data["members"].size() > 1) {
		data["_isLeader"] = 0;
		if (data.count("leader") && data.at("leader").get<formid>() == this->GetMyIDs().second) {
			data["_isLeader"] = 1;
		}
		this->AddChannel(partyCh, false);
	}
	else {
		this->RemoveChannel(partyCh.id);
	}
	pImpl->tasks.Add([=] {
		pImpl->partyData = data;
	});
}

void World::SetDialogData(json dat) {
	if (dat.is_object()) {
		if (dat["_type"] == "party_invite") {
			dat["text"] = L_WANT_U_ENTER1 + dat["sender"].get<string>() + L_WANT_U_ENTER2;
		}
	}
	pImpl->tasks.Add([=] {
		pImpl->dialogData = dat;
	});
}

void World::SetMarkers(vector<formid> markers) {
	pImpl->tasks.Add([=] {
		if (pImpl->cache.lastMarkersFromServer != markers) {
			pImpl->cache.lastMarkersFromServer = markers;
			for (auto id : markers) {
				auto ref = (TESObjectREFR *)LookupFormByID(id);
				assert(ref);
				assert(ref->formType == kFormType_Reference);
				if (ref && ref->formType == kFormType_Reference) {
					sd::AddToMap(ref, 1);
				}
			}
		}
	});
}

BSFixedString *GetBSFixedString(const string &content) {
	static std::unordered_map<string, BSFixedString *> fsCache;
	if (!fsCache[content]) fsCache[content] = new BSFixedString((new string(content))->data());
	return fsCache[content];
}

void World::BroadcastIn(formid owner, uint8_t contentType, string content) {
	pImpl->tasks.Add([=] {
		if (pImpl->gui) pImpl->gui->api_OnBroadcastIn(owner, contentType, content);
		if (contentType == CONTENT_ANIMEVENT) {
			TESObjectREFR *refr = nullptr;
			if (owner == this->GetMyIDs().second && 0) refr = sd::GetPlayer(); // TODO
			else {
				auto it = pImpl->entities.find(owner);
				if (it == pImpl->entities.end() || !it->second) return;
				refr = it->second->FindForm();
			}
			if (!refr) return;

			auto contentLower(content);
			for (int i = 0; i != contentLower.size(); ++i) contentLower[i] = tolower(content[i]);

			Actor *ac = nullptr;
			if (refr->formType == kFormType_Character) ac = (Actor *)refr;

			if (content == "SKYMP2_Invisible") {
				pImpl->nicknameInvisible.insert(owner);
				if (ac) sd::SetAlpha(ac, 0.01f, false);
			}
			else if (content == "SKYMP2_Visible") {
				pImpl->nicknameInvisible.erase(owner);
				if (ac) sd::SetAlpha(ac, 100.f, false);
			}
			else if (content == "SKYMP2_AfkEnter" || content == "SKYMP2_AfkExit") {
				// Do nothing. Won't be supported since 2.1.8
			}
			else {
				pImpl->clearAfk(owner, ac);
				if (contentLower.find("idle") != string::npos) {
					idleOkThreadId = GetCurrentThreadId();
				}
				if (contentLower.find("equip") == string::npos) {
					refr->animGraphHolder.SendAnimationEvent(GetBSFixedString(content));
				}
				idleOkThreadId = 0;
			}
		}
		else if (contentType == CONTENT_ERASE_FOREVER) {
			this->EraseEntity(owner);
		}
		else if (contentType == CONTENT_FORCE_ANIMEVENT) {
			static clock_t g_lastForceAnimEvent = 0;
			if (clock() - g_lastForceAnimEvent <= 1000) return;
			g_lastForceAnimEvent = clock();
			if (owner == pImpl->myIds.second) {
				sd::GetPlayer()->animGraphHolder.SendAnimationEvent(GetBSFixedString(content));
				OnSyncBackAE(sd::GetPlayer(), content);
			}
		}
	}); 
}

void World::UpdateDateTime(json dateTime) {
	pImpl->tasks.Add([=] {
		pImpl->dateTime = dateTime;
		pImpl->dateTimeRecvMoment = clock();
	});
}

void World::UpdateUIAVs(formid id, vector<float> uiavs) {
	if (uiavs.size() != 3) throw std::logic_error("bad UIAVs size");
	pImpl->tasks.Add([=] {
		auto &mems = pImpl->partyData["members"];
		for (int i = 0; i != mems.size(); ++i) {
			if (mems[i] == id) {
				pImpl->partyData["_avs"][std::to_string(i)] = json::array({ uiavs[0], uiavs[1], uiavs[2] });
				break;
			}
		}
	});
}

bool World::PCEquipmentUpdated() const {
	return pImpl->pcEquipmentUpdated;
}

formid World::LookupEntityByName(string name) const {
	for (auto &p : pImpl->entities) {
		auto smartActor = dynamic_cast<SmartActor *>(&*p.second);
		if (smartActor) {
			if (smartActor->GetName() == name) {
				return p.first;
			}
		}
	}
	return 0;
}

formid World::CastToServerFormat(formid id) const {
	if (id >= 0xFF000000) {
		for (auto e : pImpl->entities) {
			assert(e.second);
			if (e.second && e.first >= 0xff000000) {
				auto ref = e.second->FindForm();
				if (ref && ref->formID == id) {
					return e.first;
				}
			}
		}
		return formid(0);
	}
	return id;
}

InvEvent World::NextEvent() {
	InvEvent res;
	if (!pImpl->ies.empty()) {
		res = pImpl->ies.front();
		pImpl->ies.pop_front();
	}

	// Handle drop
	if (!strcmp(res.type, "drop")) {
		auto obj = pImpl->factory.CreateObject();
		auto refr = (TESObjectREFR *)LookupFormByID(res.ref);
		sd::Delete(refr);
	}

	return res;
}

World::World(WorldCleaner *wc, string localPlayerName, GUI *gui) {
	pImpl.reset(new Impl(wc));
	pImpl->localPlName = localPlayerName;
	pImpl->gui = gui;

	// Player inventory
	sd::RemoveAllItems(*g_thePlayer, nullptr, false, true);
	pImpl->pcInventoryControl.reset(new IILocalPlayer);

	pImpl->emitTasks.push_back([](INet &net) { net.LoadMyInventory(); });

	auto globalTimescale = (TESGlobal *)LookupFormByID(ID_TESGlobal::TimeScale);
	sd::SetValue(globalTimescale, 0);


	// sd::SetValue doesn't work here
	// You can check this by 'help killmoverandom' command
	sd::ExecuteConsoleCommand("set killmove to 0", nullptr);
	sd::ExecuteConsoleCommand("set killmoverandom to 0", nullptr);
}

World::~World() {
}

void World::Update() {
	g_tasks.Update();

	// Shitfix of inventory items doubling
	static auto fsInvMenu = new BSFixedString("InventoryMenu");
	const bool invOpen = MenuManager::GetSingleton()->IsMenuOpen(fsInvMenu);
	const bool openJustNow = invOpen && !pImpl->invOpenWas;
	if (openJustNow || (clock() - pImpl->ctorMoment > 1500 && pImpl->invNumOpens == 0)) {
		++pImpl->invNumOpens;
		if (pImpl->invNumOpens <= 2) {
			pImpl->emitTasks.push_back([](INet &net) { net.LoadMyInventory(); });
		}
	}
	pImpl->invOpenWas = invOpen;

	if (pImpl->dateTime != nullptr) {
		this->UpdateTime();
		this->UpdateWeather();
	}

	// chat hotkeys
	for (int key = VK_F1; key <= VK_F12; ++key) {
		if (sd::GetKeyPressed(key)) {
			int i = key - VK_F1;
			if (i < pImpl->chatChannels.size()) {
				pImpl->gui->FocusChatChannel(pImpl->chatChannels.at(i).id);
			}
			else if (i == pImpl->chatChannels.size()) {
				// TODO: hide/show chat
			}
		}
	}

	// request objects data
	pImpl->emitTasks.push_back([this](INet &net) {
		if (clock() - pImpl->lastReq > 0 && (!net.IsRequestDataInProgress(1) || !net.IsRequestDataInProgress(0))) {
			pImpl->lastReq = clock();
			auto refs = sdutil::FindRefs();
			vector<TESObjectREFR *> neededRefs, neededHostableRefs;
			neededRefs.reserve(refs.size());
			neededHostableRefs.reserve(refs.size());
			for (auto ref : refs) {
				const bool isHostable = IsHostable(ref->baseForm->formType);
				if (ref->formType == kFormType_Character) continue;
				if (!isHostable && !sd::HasLOS(sd::GetPlayer(), ref)) continue;
				switch (ref->baseForm->formType) {
				case kFormType_Container:
				case kFormType_Door:
					neededRefs.push_back(ref);
					break;
				case kFormType_Flora:
					if (((TESFlora *)ref->baseForm)->produce.produce) neededRefs.push_back(ref);
					break;
				case kFormType_Tree:
					if (((TESObjectTREE *)ref->baseForm)->produce.produce) neededRefs.push_back(ref);
					break;
				default:
					if (isHostable) {
						if (ref->baseForm->formType != kFormType_Light || IsTorch(ref->baseForm)) {
							neededHostableRefs.push_back(ref);
						}
					}
					if (ref->baseForm->formID == ID_TESObjectSTAT::MapMarker) {
						if (sd::CanFastTravelToMarker(ref)) {
							formid id = ref->formID;
							if (pImpl->cache.addedMarkers.count(id) == 0) {
								pImpl->cache.addedMarkers.insert(id);
								pImpl->emitTasks.push_back([id](INet &net) {
									net.AddMarker(id);
								});
							}
						}
					}
				}
			}
			vector<formid> ids, idsHostable;
			for (auto ref : neededRefs) {
				if (ref->formID >= 0xFF000000) continue; // TODO
				ids.push_back(ref->formID);
			}
			for (auto ref : neededHostableRefs) {
				if (ref->formID >= 0xFF000000) continue; // TODO
				idsHostable.push_back(ref->formID);
			}
			net.RequestData(ids, false);
			net.RequestData(idsHostable, true);
		}
	});

	// Add static chat channels
	if (pImpl->chatChannels.empty()) {
		this->AddChannel(json{
			{ "id", "common" },
			{ "name", "" },
			{ "caps", "" },
			{ "readOnly", 1 },
			{ "common", 1 }
			});
		this->AddChannel(json{
			{ "id", "rp" },
			{ "name", "" },
			{ "caps", "" },
			{ "readOnly", 0 },
			{ "common", 0 }
			});
		this->AddChannel(json{
			{ "id", "nonrp" },
			{ "name", "" },
			{ "caps", "" },
			{ "readOnly", 0 },
			{ "common", 0 }
			});
		this->AddChannel(json{
			{ "id", "trade" },
			{ "name", "" },
			{ "caps", "" },
			{ "readOnly", 0 },
			{ "common", 0 }
			});
	}

	// Update download dialog
	static volatile bool loading = false;
	static bool startedThr = false;
	if (!startedThr) {
		DeleteFileA("check_res.tmp");
		std::thread([] {
			while (1) {
				Sleep(1000);

				PROCESS_INFORMATION pi; STARTUPINFO cif; ZeroMemory(&cif, sizeof(STARTUPINFO));
				std::ofstream("only_check.tmp") << 1;
				if (!CreateProcessA("skymp2-upd.exe", NULL,
					NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &cif, &pi) == TRUE) {
					static char buf[1024] = { 0 };
					sprintf_s(buf, "%s\nCreateProcess('skymp2-upd.exe') failed\nError %u", LW_UPDATE_NOT_WORKING, GetLastError());
					g_tasks.Add([] {
						sd::ShowMessageBox(buf);
					});
					return;
				}
				WaitForSingleObject(pi.hProcess, 0);
				auto resF = std::ifstream("check_res.tmp");
				if (resF.good()) {
					string resStr; resF >> resStr;
					resF.close();
					DeleteFileA("check_res.tmp");

					int res = atoi(resStr.data());
					if (res) {
						loading = true;
						return;
					}
				}
			}
		}).detach();
		startedThr = true;
	}

	static bool wasLoading = false;
	if (wasLoading != loading) {
		wasLoading = loading;
		if (loading) {
			json dat = json::object();
			dat["text"] = L_NEW_UPDATE;
			dat["_type"] = "download";
			dat["buttons"] = json::array();
			dat["buttons"].push_back(json{ {"id", "local_download_ok"}, {"text", L_HOROSHECHNO} });
			dat["windowRounding"] = 12.5f;
			dat["buttonRounding"] = 5.f;
			this->SetDialogData(dat);
		}
	}

	static clock_t last = 0;
	if (clock() - last > 1000) {
		last = clock();
		static int wasVal = 0;
		int waitForConfirm = 0;
		std::ifstream("SKYMP2/wait-for-confirm.skymp2tmp") >> waitForConfirm;

		if (waitForConfirm != wasVal) {
			wasVal = waitForConfirm;
		}
		if (waitForConfirm && pImpl->downloadOk) {
			pImpl->downloadOk = false;

			Console_Print("force update");
		}
	}

	RunBubbleDispellTasks();
	RemoveWhisperChannelsIfNeed();

	if (clock() - pImpl->ctorMoment > 2000) {
		UpdateGUI();
	}
	//ReActivateContainer();

	for (auto &p : pImpl->entities) {
		assert(p.second);
		p.second->Update();

		if (p.first >= 0xFF000000 && rand() % 3 == 0) {
			UpdateNicknameAndBubble(p.first, &*p.second);
		}
	}
	for (auto &p : pImpl->debugEntities) {
		assert(p.second); 
		p.second->Update();
	}

	DeleteExitedPlayers();
	UpdateAfkPlayers();
	UpdateBlockingPlayers();

	// Run all tasks
	pImpl->tasks.Update();
	if (clock() - pImpl->ctorMoment > 1000)
		pImpl->tasks1000msDelay.Update();

	GenInvEvents();
	RegisterPersistentRefs();
}

void World::SetDebugMode(bool debug) {
	pImpl->debug = debug;
}

std::pair<userid, formid> World::GetMyIDs() const {
	return pImpl->myIds;
}

void World::OnActivate(ActiEvent evn) {
	assert(evn.target > 0);
	if (evn.caster == 0x14) {

		auto refr = (TESObjectREFR *)LookupFormByID(evn.target);
		if (!refr || (refr->formType != kFormType_Reference && refr->formType != kFormType_Character)) return;
		auto baseType = refr->baseForm->formType;
		auto openState = sd::GetOpenState(refr);

		evn.target = this->CastToServerFormat(evn.target);
		if (evn.target == 0) return;

		auto myFormId = pImpl->myIds.second;
		pImpl->emitTasks.push_back([=](INet &net) {
			net.Activate(myFormId, evn.target, baseType, !!(openState >= 3));
			pImpl->lastReq = 0;
		});

		if (evn.target < 0xff000000) { // TODO: FF containers :)
			auto ref = (TESObjectREFR *)LookupFormByID(evn.target);
			assert(ref);
			assert(ref->formType == kFormType_Reference || ref->formType == kFormType_Character);
			if (ref && ref->baseForm->formType == kFormType_Container) {
				pImpl->activatedCont = evn.target;
			}
		}
	}
}

void World::EmitMovement(INet &net, Movement mov, formid id) {
	
	const bool isMe = (id == 0 || id == pImpl->myIds.second);
	
	auto rate = 200;
	if (isMe) {
		switch (mov.GetRunMode()) {
		case Movement::RunMode::Standing:
		case Movement::RunMode::Walking:
			rate = 300;
			break;
		default:
			rate = 200;
			break;
		}
	}

	if (clock() - pImpl->cache.lastSend[id] > rate) {
		pImpl->cache.lastSend[id] = clock();
		assert(Movement(mov)._pos.Length() > 0);
		float uiHealth = -1, uiMagicka = -1, uiStamina = -1;
		if (isMe) {
			uiHealth = sd::GetActorValuePercentage(*g_thePlayer, "health");
			uiMagicka = sd::GetActorValuePercentage(*g_thePlayer, "magicka");
			uiStamina = sd::GetActorValuePercentage(*g_thePlayer, "stamina");
		}

		formid cellOrWorld = 0;
		auto cell = sd::GetPlayer()->parentCell;
		if (!cell || !cell->IsInterior()) {
			auto world = sd::GetPlayer()->GetWorldspace();
			if (world) cellOrWorld = world->formID;
		}
		else cellOrWorld = cell->formID;

		if (isMe) {
			auto idleForm = sd::GetPlayer()->processManager->middleProcess->currentIdle;
			if (!idleForm) pImpl->lastPcAe.clear();
			mov._lastAe = pImpl->lastPcAe;
		}
		net.UpdateMovement(id, mov, uiHealth, uiMagicka, uiStamina, cellOrWorld, cell ? cell->formID : 0);
	}
}

void World::EmitEquipment(INet &net) {
	//if (this->PCEquipmentUpdated()) // TODO: remove that unused function and var
	{
		enum { EqRateMs = 2000 };
		const auto eq = Equipment_::GetFrom(*g_thePlayer);
		if (pImpl->cache.lastEq != eq /*|| clock() - pImpl->cache.lastSendEq > EqRateMs*/) {
			pImpl->cache.lastEq = eq;
			pImpl->cache.lastSendEq = clock();
			net.UpdateEquipment(eq);
		}
	}
}

void World::EmitLook(INet &net) {
	const auto look = Look_::GetFromPlayer();
	if (look != pImpl->cache.lastLook) {
		pImpl->cache.lastLook = look;
		if (look.tints.size() > 0) {
			net.UpdateLook(look);
		}

		// Apply name
		auto buf = new char[pImpl->localPlName.size() + 1];
		memcpy(buf, pImpl->localPlName.data(), pImpl->localPlName.size() + 1);
		papyrusForm::SetName(sd::GetPlayer()->baseForm, *new BSFixedString(buf));
	}
}

void World::EmitAll(INet &net) {
	if (pImpl->gui) pImpl->gui->EmitAll(&net);

	// Send my anim events
	{
		decltype(g_aeList) aeList;
		{
			std::lock_guard<std::recursive_mutex> l(g_aeListMutex);
			aeList = g_aeList;
			g_aeList.clear();
		}
		for (auto ae : aeList) {
			net.Broadcast(this->GetMyIDs().second, CONTENT_ANIMEVENT, ae);
			auto aeLower = ae;
			for (auto &ch : aeLower) ch = tolower(ch);
			if (aeLower.find("idle") != string::npos) {
				pImpl->lastPcAe = ae;
			}
		}
	}

	// Send other player anims
	{
		decltype(g_otherAeList) otherAeList;
		{
			std::lock_guard<std::recursive_mutex> l(g_aeListMutex);
			otherAeList = g_otherAeList;
			g_otherAeList.clear();
		}
		for (const auto &pair : otherAeList) {
			auto id = this->CastToServerFormat(pair.first);
			if (!id) continue;
			auto &ae = pair.second;
			net.Broadcast(id, CONTENT_FORCE_ANIMEVENT, ae);
		}
	}

	EmitMovement(net, Movement_::GetFrom(sd::GetPlayer()), 0);
	EmitLook(net);
	EmitEquipment(net);

	for (auto task : pImpl->emitTasks) task(net);
	pImpl->emitTasks.clear();

	while (1) {
		auto ie = this->NextEvent();
		if (ie.IsEmpty()) break;
		net.InventoryEvent(ie);
		pImpl->lastReq = 0;
	}
}

void World::RunBubbleDispellTasks() {
	decltype(pImpl->bubbleDispellTasks) tasks;
	for (auto task : pImpl->bubbleDispellTasks) {
		bool run = clock() > task.dispellMoment;
		if (run) {
			auto &bubsrc = pImpl->chatBubbleSrc[task.authorId];
			if (bubsrc == task.text) {
				bubsrc = "";
			}
		}
		else {
			tasks.push_back(task);
		}
	}

	pImpl->bubbleDispellTasks = tasks;
}

std::function<void()> World::AddChatBubble(formid eid, string msg, bool autoDispell) {
	bool pops = 0;
	while (msg.size() > 50) {
		msg.pop_back();
		pops = 1;
	}
	if (pops) msg += "...";

	pImpl->chatBubbleSrc[eid] = msg;

	BubbleDispellTask task;
	task.authorId = eid;
	task.dispellMoment = clock() + (autoDispell ? CHAT_BUBBLE_DISPELL_MS : 0);
	task.text = msg;
	if (autoDispell) {
		pImpl->bubbleDispellTasks.push_back(task);
		return nullptr;
	}
	else {
		return [this, task] {
			pImpl->bubbleDispellTasks.push_back(task);
		};
	}
}

void World::RemoveWhisperChannelsIfNeed() {
	for (auto ch : pImpl->chatChannels) {
		if (!memcmp(ch.id.data(), "whisper:", (sizeof "whisper:") - 1)) {
			// Requires ch.name to be equal real player name!
			auto id = this->LookupEntityByName(ch.name);
			if (!id || pImpl->entities.count(id) == 0) {
				this->RemoveChannel(ch.id);
				break;
			}
			auto entity = pImpl->entities.at(id);
			if (!entity->FindForm()) {
				this->RemoveChannel(ch.id);
				break;
			}
			auto distance = sd::GetPosition(entity->FindForm()) - sd::GetPosition(sd::GetPlayer());
			if (distance.Length() > 280) {
				this->RemoveChannel(ch.id);
				break;
			}
		}
	}
}



void World::UpdateGUI() {
	// Update GUI
	assert(pImpl->gui);
	if (pImpl->gui) {
		std::vector<formid> sel;
		for (auto &p : pImpl->entities) {
			auto ref = p.second->FindForm();
			if (ref && (ref->formType == kFormType_Reference || ref->formType == kFormType_Character)) {
				sel.push_back(ref->formID);
			}
		}

		// Localization:
		// TODO: move to separate function
		for (auto &ch : pImpl->chatChannels) {
			if (ch.id == "common") {
				ch.name = L_CHAT;
				ch.nameCaps = L_CHAT_CAPS;
			}
			if (ch.id == "trade") {
				ch.name = L_TRADE;
				ch.nameCaps = L_TRADE_CAPS;
			}
			if (ch.id == "rp") {
				ch.name = L_RP;
				ch.nameCaps = L_ROLEPLAY_CHAT_CAPS;
			}
			if (ch.id == "nonrp") {
				ch.name = L_NONRP;
				ch.nameCaps = L_NONRP_CHAT_CAPS;
			}
		}

		// localization
		if (pImpl->dialogData.is_object()) {
			for (int i = 0; i != pImpl->dialogData["buttons"].size(); ++i) {
				auto &t = pImpl->dialogData["buttons"][i]["text"];
				if (t == "$yes") t = L_YES;
				if (t == "$no") t = L_NO;
			}
		}

		json outMsg, outSelect, outResponse;
		pImpl->gui->Update(sel, pImpl->chatChannels, pImpl->partyData, pImpl->dialogData, outMsg, outSelect, outResponse, pImpl->localPlName);
		if (outResponse.is_null() == false) {
			pImpl->emitTasks.push_back([this, outResponse](INet &net) {
				string btnid = outResponse.get<string>();
				if (btnid != "local_download_ok") {
					net.SendDialogResponse(btnid, pImpl->dialogData);
				}
				if (btnid == "local_download_ok") pImpl->downloadOk = true;
			});
		}
		
		static bool g_pinging = false;
		static string g_pingChId = "";
		static clock_t g_lastPing = 0;

		if (g_pinging && clock() - g_lastPing > 1000) {
			g_lastPing = clock();
			const Movement myMov = Movement_::GetFrom(sd::GetPlayer());
			pImpl->emitTasks.push_back([=](INet &net) {
				int ping = net.GetPing();
				int lag = net.GetLag();
				int outputRate = net.GetOutputPacketRate();
				int numItems = 0;
				for (auto &p : pImpl->entities) {
					if (p.first < 0xff000000 && !dynamic_cast<SmartActor *>(&*p.second)) {
						numItems++;
					}
				}
				string text = "ping=" + std::to_string(ping) + " lag=" + std::to_string(lag) + " items=" + std::to_string(numItems) + " outputRate=" + std::to_string(outputRate);
				this->AddChatMessage(g_pingChId, text, "System", myMov);
			});
		}

		if (outMsg.size()) {

			
			if (outMsg["message"] == "/ping") {
				const string chid = outMsg["channel"].get<string>();
				g_pingChId = chid;
				g_pinging = !g_pinging;
				return;
			}

			pImpl->emitTasks.push_back([=](INet &net) {
				static clock_t lastMsg = 0;
				static std::map<string,int> limitGone;
				//static std::vector<json> messages;

				const string chid = outMsg["channel"].get<string>();
				const bool isTrade = chid == "trade";
				
				const int limit = isTrade ? 1 : 3;
				int ogranMs = 3000;
				if (isTrade) ogranMs = 10000;

				if (clock() - lastMsg > ogranMs) {
					net.SendChatMessage(outMsg.dump());
					lastMsg = clock();
					limitGone[chid] = 0;
					
				}
				else if (limitGone[chid] < limit) {
					net.SendChatMessage(outMsg.dump());
					limitGone[chid]++;
				}
				else {
					if (isTrade) sd::PrintNote(LW_DONT_FLOOD_TRADE);
					else sd::PrintNote(LW_DONT_FLOOD); // TODO: move to Localization.h
				}
			});
		}
		if (outSelect.size()) {
			for (auto &p : pImpl->entities) {
				if (auto ref = p.second->FindForm(); ref) {
					if (ref->formID == outSelect["targetid"]) {
						outSelect["server_targetid"] = p.first;
						outSelect["targetName"] = papyrusObjectReference::GetDisplayName(ref).data;
					}
				}
			}
			outSelect["targetid"] = outSelect["server_targetid"];
			outSelect.erase("server_targetid");

			pImpl->emitTasks.push_back([=](INet &net) {
				net.SendInteractionClick(outSelect.dump());
			});
			pImpl->gui->api_OnInteractionClick(outSelect["targetName"], outSelect["index"]);
		}
	}
}

void World::ReActivateContainer() {
	static auto fsContMenu = new BSFixedString("ContainerMenu");
	bool open = MenuManager::GetSingleton()->IsMenuOpen(fsContMenu);
	if (open != pImpl->cache.contWasOpen) {
		if (!open) {
			assert(pImpl->activatedCont != 0);
			const formid target = pImpl->activatedCont;
			pImpl->activatedCont = 0;

			auto myFormId = pImpl->myIds.second;
			pImpl->emitTasks.push_back([myFormId, target](INet &net) {
				net.Activate(myFormId, target, TESObjectCONT::kTypeID);
			});
		}
		pImpl->cache.contWasOpen = open;
	}
}

void World::UpdateNicknameAndBubble(formid id, IRef *r) {
	// Update nickname and bubble
	auto smartRef = dynamic_cast<SmartActor *>(r);
	if (smartRef == nullptr) return;

	const auto ref = r->FindForm();
	const formid idLocal = ref ? ref->formID : 0;

	auto posSrcFn = [idLocal] {
		try {
			// TODO: is it safe
			NiPoint3 res;
			auto ref = (TESObjectREFR *)LookupFormByID(idLocal);
			if (ref && ref->formType == kFormType_Character) {
				res = ref->pos;
			}
			if (ref) {
				auto pcHead = (*g_thePlayer)->pos;
				pcHead += { 0, 0, 128 };
				auto distToPcHead = (pcHead - res).Length();

				float offsetPercentage = distToPcHead / 512;

				return GetNodeWorldPosition((Actor *)ref, "NPC Head [Head]", false) += { 0, 0, 30.f + (15.f * offsetPercentage)};
			}

		}
		catch (...) {
			assert(0);
		}
		return NiPoint3();
	};

	float distanceCamera = std::numeric_limits<float>::infinity();
	float distance = std::numeric_limits<float>::infinity();
	float h = 0;

	if (ref && ref->formType == kFormType_Character) {
		distance = (sd::GetPosition(ref) - sd::GetPosition(*g_thePlayer)).Length();

		NiPoint3 camPos;
		if (PlayerCamera::GetSingleton()->cameraState && PlayerCamera::GetSingleton()->GetNiCamera()) {
			camPos = PlayerCamera::GetSingleton()->GetNiCamera()->m_worldTransform.pos;
		}
		else {
			assert(0);
			camPos = sd::GetPosition(sd::GetPlayer());
		}

		distanceCamera = (camPos - posSrcFn()).Length();
		int mod = int(10000 / distanceCamera * 1) / 2;
		h = (25 + mod) * 0.9f;
	}
	else {
		auto it = pImpl->nicknames.find(id);
		if (it != pImpl->nicknames.end()) it->second->SetText("");
		it = pImpl->chatBubbleTexts.find(id);
		if (it != pImpl->chatBubbleTexts.end()) it->second->SetText("");
		return;
	}

	if (pImpl->nicknames.count(id)) {
		auto &nameText = pImpl->nicknames.at(id);
		assert(nameText);
		if (nameText) {
			if (ref && ref->formType == kFormType_Character) {
				bool visible = distance <= NICKNAME_DISTANCE_METRES * 70 && sd::HasLOS(sd::GetPlayer(), ref) && !pImpl->nicknameInvisible.count(id);
				auto &e = pImpl->entities.at(id);
				assert(e);
				if (e && e->GetMovement().IsSneaking()) {
					visible = visible && distance <= NICKNAME_DISTANCE_METRES_SNEAKING * 70;
				}

				nameText->SetPreferredTextH((int)h);
				nameText->SetText(visible ? smartRef->GetName() : "");
				nameText->SetPosSource(posSrcFn);
			}
		}
	}

	if (pImpl->chatBubbleTexts.count(id)) {
		auto &bubbleText = pImpl->chatBubbleTexts.at(id);
		assert(bubbleText);
		if (bubbleText) {
			if (ref && ref->formType == kFormType_Character) {
				bubbleText->SetPreferredTextH(h / 2);
				auto newText = (distance <= 512 * 1.5) ? pImpl->chatBubbleSrc[id] : "";
				bubbleText->SetText(newText);
				bubbleText->SetPosSource(posSrcFn);
				bubbleText->SetOffsetY(-20);
			}
		}
	}
}

void World::DeleteExitedPlayers() {
	pImpl->beforeErasePlayers.Update();

	if (clock() - pImpl->lastMovUpdG > 2000) return;

	for (auto &p : pImpl->entities) {
		auto key = p.first;
		if (key < 0xff000000) continue;

		auto ms = clock() - pImpl->lastUpd[key].any();
		bool isActor = !!dynamic_cast<SmartActor *>(&*p.second);
		if (ms < (isActor ? 10'000 : 10'000)) continue;

		bool updateLast200ms = clock() - pImpl->lastMovUpdGFF <= 200;
		if (!updateLast200ms && !isActor) continue;

		pImpl->entities.erase(key);
		pImpl->nicknames.erase(key);
		pImpl->chatBubbleTexts.erase(key);
		if (key == pImpl->myIds.second) pImpl->debugEntities.clear();
		break;
	}
}

void World::UpdateAfkPlayers() {
	for (const auto &pair : pImpl->lastUpd) {
		auto &id = pair.first;
		auto &updMoments = pair.second;
		const float afkTime = (float)updMoments.markAlive - (float)updMoments.movement;

		auto it = pImpl->entities.find(id);
		if (it == pImpl->entities.end()) continue;

		auto ac = (Actor *)it->second->FindForm();
		if (!ac || ac->formType != kFormType_Character) continue;
		
		const bool isAfk = afkTime >= 2000.f && !sd::IsDead(ac);

		if (isAfk) {
			bool inserted = pImpl->afkPlayers.insert(id).second;
			if (inserted) {
				pImpl->afkDispellFn[id] = this->AddChatBubble(id, L_AFK, false);
				sd::EnableAI(ac, false);
			}
		}
		else {
			auto numErased = pImpl->afkPlayers.erase(id);
			if (numErased > 0) {
				pImpl->clearAfk(id, ac);
			}
		}
	}
}

void World::UpdateBlockingPlayers() {
	for (int i = 0; i < 10; ++i) {
		auto pos = sd::GetPlayer()->pos;
		auto actor = sd::FindRandomActor(pos.x, pos.y, pos.z, 1024);
		if (actor && actor->formID >= 0xff000000) {
			if (!!sd::Obscript::IsBlocking(actor)) {
				std::lock_guard<std::recursive_mutex> l(g_aeListMutex);
				g_blockingPlayers.insert(actor);
			}
			else {
				std::lock_guard<std::recursive_mutex> l(g_aeListMutex);
				g_blockingPlayers.erase(actor);
			}
		}
	}
}

void World::GenInvEvents() {
	if (pImpl->pcInventoryControl) {
		pImpl->pcInventoryControl->Update(*g_thePlayer);

		auto ie = pImpl->pcInventoryControl->Next();
		if (ie.IsEmpty() == false) {
			if (!strcmp(ie.type, "drop")) {
				pImpl->factory.wc->Flush(); // force delete local copy of dropped item
				pImpl->lastReq = 0; // force RequestData to faster display dropped item
			}
			pImpl->ies.push_back(ie);
		}
	}
}

void World::RegisterPersistentRefs() {
	auto cell = (*g_thePlayer)->parentCell;
	if (cell == pImpl->lastPcCell) return;
	pImpl->lastPcCell = cell;

	auto refs = sdutil::FindRefs();
	for (auto ref : refs) {
		switch (ref->baseForm->formType) {
		case kFormType_Flora:
		case kFormType_Tree:
		case kFormType_Container:
		case kFormType_Door:
		case kFormType_Activator:
		case kFormType_MovableStatic:
		case kFormType_Ammo:
		case kFormType_Armor:
		case kFormType_Book:
		case kFormType_Potion:
		case kFormType_Ingredient:
		case kFormType_Key:
		case kFormType_Misc:
		case kFormType_SoulGem:
		case kFormType_Weapon:
		case kFormType_Light:
		{
			const auto id = ref->formID;
			if (id && id < 0xff000000) {
				if (pImpl->entities.count(id) == 0) {
					pImpl->entities[id].reset(new PersistentRef(id));
				}
			}
			break;
		}
		default:
			break;
		}
	}
}

void World::UpdateTime() {
	auto dateTime = pImpl->dateTime;
	static auto gameHour = (TESGlobal *)LookupFormByID(ID_TESGlobal::GameHour);
	sd::SetValue(gameHour, dateTime["hour"] 
		+ (dateTime["min"].get<float>() / 60) 
		+ (dateTime["sec"].get<float>() / 60 / 60)
	);
	static auto gameDay = (TESGlobal *)LookupFormByID(ID_TESGlobal::GameDay);
	static auto gameMonth = (TESGlobal *)LookupFormByID(ID_TESGlobal::GameMonth);
	static auto gameYear = (TESGlobal *)LookupFormByID(ID_TESGlobal::GameYear);
	sd::SetValue(gameDay, dateTime.at("day"));
	sd::SetValue(gameMonth, dateTime.at("mon"));
	sd::SetValue(gameYear, dateTime.at("year").get<float>() - 1819.f);
}

void World::UpdateWeather() {
	enum {
		HOURS_TO_CHANGE = 3
	};
	const int wseedTime = int(pImpl->dateTime["hour"].get<double>());
	const int wseedDay = pImpl->dateTime["mon"].get<int>() * 30 + pImpl->dateTime["day"].get<int>();
	const int wseed = (wseedTime + wseedDay * 24) / HOURS_TO_CHANGE;
	const int wtypeid = wseed % 4;
	const auto weather = sd::FindWeather(wtypeid);
	if (weather)
		sd::SetActive(weather, true, true);
	else {
		for (int i = 0; i <= 4; ++i) {
			auto weather = sd::FindWeather(i);
			if (weather) {
				sd::SetActive(weather, true, true);
				break;
			}
		}
	}
}