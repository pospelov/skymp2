#pragma once

class WorldCleaner
{
public:
	WorldCleaner();
	~WorldCleaner();
	void Update();
	void Flush();
	void ModProtection(formid formid, int mod);
	void Clear();

private:

	void DealWithRef(TESObjectREFR *ref);

	struct Impl;
	std::unique_ptr<Impl> pImpl;
};