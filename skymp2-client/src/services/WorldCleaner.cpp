#include "../stdafx.h"
#include "WorldCleaner.h"

#include "../sdutil/FindRefs.h"

struct WorldCleaner::Impl 
{
	std::unordered_map<formid, int> protection;
	void *lastCell = nullptr;
};

WorldCleaner::WorldCleaner() {
	pImpl.reset(new Impl);
}

WorldCleaner::~WorldCleaner() {

}

void WorldCleaner::Update() {
	auto cell = sd::GetParentCell(*g_thePlayer);
	if (cell != pImpl->lastCell) {
		pImpl->lastCell = cell;
		this->Flush();
	}
	for (int i = 0; i != 50; ++i) {
		auto pos = sd::GetPosition(sd::GetPlayer());
		auto ac = sd::FindRandomActor(pos.x, pos.y, pos.z, 4096);
		this->DealWithRef(ac);
	}
}

void WorldCleaner::DealWithRef(TESObjectREFR *ref) {
	if (!ref) return;
	if (ref == sd::GetPlayer()) return;

	if (ref->formID < 0xff000000 && ref->formType == kFormType_Character) {
		auto static fs = new BSFixedString("Dialogue Menu");
		if (MenuManager::GetSingleton()->IsMenuOpen(fs) == false) {
			sd::Delete(ref);
		}
		else {
			// TODO: is it safe to wait here?
			sd::Wait(100);
			UIManager::GetSingleton()->CloseMenu(fs);
			sd::Wait(300);
		}
		return;
	}

	if (ref->formType == kFormType_Water) return;
	if (ref->baseForm->formType == kFormType_Water) return;
	if (ref->baseForm->formID >= 0xFF000000) return; // To prevent water deletion

	auto id = ref->formID;
	if (id == 0x000af671 /* helgen exit blocker */ || pImpl->protection[id] < 0) {
		if (ref->formType == kFormType_Character && ref->formID >= 0xff000000) {
			sd::DisableNoWait(ref, false);
		}
		else sd::Delete(ref);
	}

	switch (ref->baseForm->formType) {
	case kFormType_Door:
	case kFormType_Container:
		sd::Lock(ref, false, false);
		sd::BlockActivation(ref, true);
		break;
	case kFormType_Tree:
	case kFormType_Flora:
	case kFormType_Ammo:
	case kFormType_Armor:
	case kFormType_Book:
	case kFormType_Potion:
	case kFormType_Ingredient:
	case kFormType_Key:
	case kFormType_Misc:
	case kFormType_SoulGem:
	case kFormType_Weapon:
	case kFormType_Light:
	case kFormType_ScrollItem:
		sd::BlockActivation(ref, true);
		enum { Keyframed = 4 };
		sd::SetMotionType(ref, Keyframed, true);
		break;
	case kFormType_MovableStatic:
		if (ref->baseForm->formID == 0x1C0C3) {
			sd::Delete(ref);
		}
		break;
	case kFormType_NPC:
	case kFormType_LeveledCharacter:
		sd::BlockActivation(ref, true);
		break;
	}
}

void WorldCleaner::Flush() {
	auto refs = sdutil::FindRefs();
	for (auto ref : refs) {
		this->DealWithRef(ref);
	}
}

void WorldCleaner::ModProtection(formid formid, int mod) {
	pImpl->protection[formid] += mod;
	this->Flush();
}

void WorldCleaner::Clear() {
	*pImpl = {};
}