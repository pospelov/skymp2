#pragma once
#include "..\gui\ImguiManager.h"
#include "Controls.h"

// Or leave
#define IDX_PARTY_INVITE	0

#define IDX_EXCHANGE		1
#define IDX_WHISPER			2

// All methods must be threadsafe
class Text
{
public:
	enum class Type {
		Default = 0,
		Nickname,
		ChatBubble,
		Empty, // For custom menus
	};

	Text();
	~Text();
	void SetPos(std::pair<float, float> p);
	void SetPos(const NiPoint3 &p);
	void SetPosSource(std::function<NiPoint3()> fn);
	void SetText(const string &str);
	void SetType(Type t);
	void SetPreferredTextH(int size);
	void SetPrePostRender(std::function<void()> preRender, std::function<void()> postRender);
	void SetOffsetY(float offset);
	void AddFlag(const char *flag);
	void RemoveFlag(const char *flag);
	std::pair<float, float> GetPos() const;
	NiPoint3 GetPos3D() const;
	std::string GetText() const;
	Type GetType() const;
	int GetPreferredTextH() const;
	std::pair<std::function<void()>, std::function<void()>> GetPrePostRender() const;
	float GetOffsetY() const;
	bool HasFlag(const char *flag) const;

private:
	struct Impl;
	Impl *pImpl;
};

struct Message
{
	string text;
	uint32_t color = -1;
};

struct ChatChannel
{
	ChatChannel() = default;
	ChatChannel(json j) {
		ChatChannel ch;
		ch.id = j["id"].get<string>();
		ch.name = j["name"].get<string>();
		ch.nameCaps = j["caps"].get<string>();
		ch.noBtnPulse = j["common"].get<int>() != 0;
		ch.readOnly = j["readOnly"].get<int>() != 0;
		ch.focus = j.count("focus") && j["focus"].get<int>() != 0;
		*this = ch;
	}

	string id;
	string name = u8"";
	string nameCaps = u8"";
	vector<Message> messages;
	bool readOnly = 0;
	bool noBtnPulse = 0;
	bool focus = 0;
};

struct AuthData
{
	enum Type : uint8_t {
		Login, 
		Register, 
		Confirm,
		ResetPassword_Email,
		ResetPassword_NewPass,
		COUNT
	};

	string name;
	string password;
	string email;
	string invite;
	string confirmCode;
	string systemError;
	string responseBody;
	int responseStatus = 0;
	bool inviteRequired = false;
	bool rememberMe = false;
	Type type = Type::Login;
	bool finished = false;
	bool forceRetryConfirm = false;
	bool forceResetPassword = false;
};

// All methods must be threadsafe
class GUI : public ImguiManager
{
public:
	GUI(Controls *controls);
	~GUI();

	void SetCursor(bool visible);

	std::shared_ptr<Text> CreateText(const string &text, std::pair<float, float> pos);
	std::shared_ptr<Text> CreateText(const string &text, const NiPoint3 &pos);

	void FocusChatChannel(const string &chId);
	void SetScrollHere(int channelIndex);
	void ShowAuth(const AuthData &authData);
	void HideAuth();
	std::unique_ptr<AuthData> GetAuthResult();

	void Update(const vector<formid> &selectable, 
		const vector<ChatChannel> &channels, 
		json partyData, 
		json dialogData, 
		json &outMsg, 
		json &outSelect, 
		json &outDialogResponse, 
		const string &localPlName);

	void AddTask(std::function<void()> task); // Threadsafe. Will be executed in next Update()

private:
	class Row;
	void Auth(void *, vector<Row> &rows, AuthData &auth, bool *outFinished);

	json UpdateInteractionMenu(const vector<formid> &selectable, json partyData);
	json UpdateChat(const vector<ChatChannel> &channels);
	void UpdateParty(json partyData, string localPlName);
	json UpdateDialog(json dialogData);

	void NewInteractionMenu(formid localId, std::vector<const char *> strings, std::pair<float, float> pos2d = {}, json partyData = nullptr);
	void closeInteractionMenu();

	std::pair<float, float> WorldPointToScreen(const NiPoint3 &pos3d);
	std::pair<float, float> GetCursorPos() const;

	void OnRender(void *fontNick) override;
	bool GetKeyPressed(int vkCode) override;
	void WorldPointToScreen(const NiPoint3 &pos3d, void *imvec2Out);
	void ProcessCtrlCCtrlV();

	struct Impl;
	std::unique_ptr<Impl> pImpl;
};