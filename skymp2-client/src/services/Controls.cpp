#include "../stdafx.h"
#include "Controls.h"

void Controls::Update()
{
	std::lock_guard<std::mutex> l(m);
	if (needApply) {
		needApply = false;
		sd::EnablePlayerControls(state[(UInt32)Control::Movement] >= MIN_ENABLED_STATE,
			state[(UInt32)Control::Fighting] >= MIN_ENABLED_STATE,
			state[(UInt32)Control::CamSwitch] >= MIN_ENABLED_STATE,
			state[(UInt32)Control::Looking] >= MIN_ENABLED_STATE,
			state[(UInt32)Control::Sneaking] >= MIN_ENABLED_STATE,
			state[(UInt32)Control::Menu] >= MIN_ENABLED_STATE,
			state[(UInt32)Control::Activate] >= MIN_ENABLED_STATE,
			true,
			NULL);
		sd::DisablePlayerControls(state[(UInt32)Control::Movement] < MIN_ENABLED_STATE,
			state[(UInt32)Control::Fighting] < MIN_ENABLED_STATE,
			state[(UInt32)Control::CamSwitch] < MIN_ENABLED_STATE,
			state[(UInt32)Control::Looking] < MIN_ENABLED_STATE,
			state[(UInt32)Control::Sneaking] < MIN_ENABLED_STATE,
			state[(UInt32)Control::Menu] < MIN_ENABLED_STATE,
			state[(UInt32)Control::Activate] < MIN_ENABLED_STATE,
			false,
			NULL);
		sd::EnableFastTravel(state[(UInt32)Control::FastTravel] >= MIN_ENABLED_STATE);
		sd::SetInChargen(state[(UInt32)Control::SaveGame] < MIN_ENABLED_STATE,
			state[(UInt32)Control::Wait] < MIN_ENABLED_STATE,
			false);
		sd::SetBeastForm(state[(UInt32)Control::BeastForm] < MIN_ENABLED_STATE);

		if (needPressWasd) {
			needPressWasd = false;
			if (state[(UInt32)Control::Movement] >= MIN_ENABLED_STATE) {
				static const vector<std::pair<BYTE, BYTE>> keys = {
					{'W', 0x11 }, {'A', 0x1E}, {'S', 0x1f}, {'D', 0x20}
				};
				for (auto &pair : keys) {
					keybd_event(pair.first, pair.second, NULL, NULL);
					keybd_event(pair.first, pair.second, KEYEVENTF_KEYUP, NULL);
				}
			}
		}
	}
}

void Controls::SetEnabled(Control control, bool enabled, std::string systemfrom)
{
	std::lock_guard<std::mutex> l(m);
	auto idx = (UInt32)control;
	if (enabled) this->disabled[systemfrom].erase(control);
	else this->disabled[systemfrom].insert(control);
	for (int i = 0; i != std::size(this->state); ++i) {
		this->state[i] = MIN_ENABLED_STATE;
	}
	for (auto pair : this->disabled) {
		for (Control control : pair.second) {
			this->state[(uint32_t)control] = MIN_ENABLED_STATE - 1;
		}
	}
	needApply = true;
	if (control == Control::Movement) needPressWasd = true;
}