#pragma once
#include "../components/SmartRef.h"
#include "../components/Look.h"
#include "../components/Inventory.h"
#include "../components/PersistentRef.h"
#include "GUI.h"

using userid = uint16_t;

class World
{
public:
	
	class INet 
	{
	public:
		virtual void FlushToWorld(World *optionalWorld) = 0;

		virtual void Task(std::function<void()> f) = 0;

		virtual void Handshake(const AuthData &auth, const string &version, bool crashed = false) = 0;
		virtual std::unique_ptr<AuthData> GetHandshakeResult() = 0;

		virtual void Confirm(string name, string confirmCode) = 0;
		virtual int GetConfirmResult() = 0;

		virtual void RetryConfirm(string name) = 0;
		virtual int GetRetryConfirmResult() = 0;

		virtual void StartResetPassword(string email) = 0;
		virtual int GetStartResetPasswordResult() = 0;

		virtual void FinishResetPassword(string code, string password) = 0;
		virtual int GetFinishResetPasswordResult() = 0;

		virtual void AddMarker(formid id) = 0;
		virtual void RequestData(vector<formid> ids, bool hostable) = 0;
		virtual void Activate(formid caster, formid target, int baseType, bool open = true) = 0;
		virtual void UpdateMovement(formid id, const Movement &mov, float uiHealth, float uiMagicka, float uiStamina, formid cellOrWorld, formid cell) = 0;
		virtual void UpdateEquipment(const Equipment &eq) = 0;
		virtual void UpdateLook(const Look &look) = 0;
		virtual void Broadcast(formid ownerId, uint8_t contentType, const string &content, int numDgrams = 1) = 0;
		virtual void InventoryEvent(const InvEvent &ie) = 0;
		virtual void SendDialogResponse(const string &buttonId, const json &dialogData) = 0;
		virtual void SendChatMessage(const string &messageJsonDump) = 0;
		virtual void SendInteractionClick(const string &inteactionClickJsonDump) = 0;
		virtual void LoadMyInventory() = 0;
		virtual void SendScriptEvent(const string &eventName, const string &content, std::function<void(int, string)>) = 0;

		virtual bool IsConnected() const = 0;
		virtual bool IsRequestDataInProgress(bool hostable) const = 0;

		virtual int GetPing() const = 0;
		virtual int GetLag() const = 0;
		virtual int GetOutputPacketRate() const = 0;
	};

	using EmitTask = std::function<void(INet &)>;

	World(WorldCleaner *wc, std::string localPlayerName, GUI *gui);
	~World();

	// ScriptDragon thread

	void Update(bool debugMode, INet &net) {
		Update();
		SetDebugMode(debugMode);
		EmitAll(net);
		//RunBubbleDispellTasks(); // TODO: wtf??!!! This function will never be called! 
	}

	std::pair<userid, formid> GetMyIDs() const;

	// Client methods to be called from the Server
	void UpdateMovement(formid id, Movement movement, formid baseId, formid hostId, uint32_t count);
	void MarkAlive(formid id);
	void UpdateEquipment(formid id, Equipment equipment);
	void UpdateLook(formid id, Look look, std::string name);
	void UpdateInventory(formid id, Inventory inventory);
	void SetMyId(userid id, formid formId);
	void Activate(formid target);
	void AddItem(formid id, uint32_t count);
	void EraseEntity(formid id);
	void BroadcastEraseEntity(formid id);
	void SetOpen(formid target, bool open);
	void ShowRaceMenu();
	void AddChannel(ChatChannel ch, bool allowOverride = true);
	void RemoveChannel(string id);
	void AddChatMessage(string channelId, string message, string author, Movement authorMov);
	void SetPartyData(json data); // pass 'null' to clear party
	void UpdateUIAVs(formid id, vector<float> uiavs);
	void SetDialogData(json data);
	void SetMarkers(vector<formid> markers);
	void BroadcastIn(formid owner, uint8_t contentType, string content);
	void UpdateDateTime(json dateTime);

	void OnActivate(ActiEvent evn);

private:
	void Update();
	void SetDebugMode(bool debug);
	void EmitAll(INet &net);

	// Called from Update()
	void RunBubbleDispellTasks();
	void RemoveWhisperChannelsIfNeed();
	void UpdateGUI();
	void ReActivateTPD();
	void ReActivateContainer();
	void UpdateNicknameAndBubble(formid id, IRef *ref);
	void DeleteExitedPlayers();
	void UpdateAfkPlayers();
	void UpdateBlockingPlayers();
	void GenInvEvents();
	void RegisterPersistentRefs();
	void UpdateTime();
	void UpdateWeather();

	std::function<void()> AddChatBubble(formid id, string msg, bool autoDispell = true);

	void EmitMovement(INet &net, Movement mov, formid id); // id 0 means my id
	void EmitLook(INet &net);
	void EmitEquipment(INet &net);

	bool PCEquipmentUpdated() const;
	formid LookupEntityByName(string name) const;
	formid CastToServerFormat(formid localId) const;

	// Should be called on chests too 
	// (even if you don't need it's InvEvents)
	InvEvent NextEvent();

	struct Impl;
	std::unique_ptr<Impl> pImpl;
};