#pragma once

#define _SCL_SECURE_NO_WARNINGS  

#include "..\scriptdragon\skyscript.h"
#include "..\scriptdragon\obscript.h"
#include "..\scriptdragon\types.h"
#include "..\scriptdragon\enums.h"
#include "..\scriptdragon\plugin.h"

#include "../common/IPrefix.h"
#include "../skse/PapyrusActor.h"
#include "../skse/PapyrusObjectReference.h"
#include "../skse/PapyrusForm.h"
#include "../skse/PapyrusGame.h"
#include "../skse/GameForms.h"
#include "../skse/GameReferences.h"
#include "../skse/GameObjects.h"
#include "../skse/GameInput.h"
#include "../skse/GameData.h"
#include "../skse/GameMenus.h"
#include "../skse/GameExtraData.h"
#include "../skse/GameCamera.h"
#include "../skse/SafeWrite.h"
#include "../skse/Utilities.h"

#include "..\..\json\single_include\nlohmann\json.hpp"
using json = nlohmann::json;

namespace sd
{
	inline NiPoint3 GetPosition(TESObjectREFR *ref) {
		return {
			GetPositionX(ref), GetPositionY(ref), GetPositionZ(ref)
		};
	}

	inline bool SetPosition(TESObjectREFR *ref, const NiPoint3 &pos) {
		return SetPosition(ref, pos.x, pos.y, pos.z);
	}
}

#include <vector>
#include <array>
#include <set>
#include <stack>
#include <queue>
#include <list>
#include <memory>
#include <string>
#include <algorithm>
#include <functional>
#include <ctime>
#include <typeinfo>
#include <sstream>
#include <mutex>
#include <thread>
#include <optional>
#include <fstream>
#include <boost/stacktrace.hpp>
#include <boost/exception/all.hpp>
#include <boost/signals2.hpp>
#include <boost/lockfree/queue.hpp>

#include <d3d9.h>
#include <process.h>

typedef boost::error_info<struct tag_stacktrace, boost::stacktrace::stacktrace> traced;

template <class E>
auto with_trace(const E& e) {
	return boost::enable_error_info(e)
		<< traced(boost::stacktrace::stacktrace());
}

inline std::string get_trace(std::exception &e) {
	const boost::stacktrace::stacktrace *st = boost::get_error_info<traced>(e);

	std::stringstream ss;
	if (st) {
		ss << "\n" << *st;
	}
	return ss.str();
}

static_assert(CLOCKS_PER_SEC == 1000, "");

using namespace std::string_literals;

using formid = uint32_t;

#define SKYMP2_HOOKS_DLL "SKYMP2.hooks"
#define SKYMP2_FILES_DIR "SKYMP2"
#define SKYMP2_CONFIG    "SKYMP2.json"

template <class T>
void CheckForm(T *form) {
	if (!form) throw with_trace(std::logic_error("nullptr form"));
	if (form->formType != T::kTypeID) throw with_trace(std::logic_error("wrong form type"));
}

#pragma comment(lib, "Ws2_32.lib")

#define skymp2_assert(x) assert(x) // TODO: implement own assert

using string = std::string;
template<class T> using vector = std::vector<T>;

#define SAFE_PARSE(x) [&] { try { return json::parse(x); } catch(std::exception &e) { throw std::logic_error("SAFE_PARSE error line " + std::to_string(__LINE__) + e.what()); } }()