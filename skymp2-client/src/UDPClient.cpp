#include <string>
#include <list>
#include <boost/asio/ip/udp.hpp>
#include <boost/array.hpp>
using string = std::string;
using boost::asio::ip::udp;

#include "UDPClient.h"

inline std::pair<string, string> ParseHostAndPort(const string &hostAndPort) {
	string host;
	string port;
	for (size_t i = 0; i != hostAndPort.size(); ++i) {
		if (hostAndPort[i] == ':') {
			port = "read me pls";
			break;
		}
		host.push_back(hostAndPort[i]);
	}
	if (port == "read me pls") {
		port.clear();
		for (INT i = hostAndPort.size() - 1; i >= 0; --i) {
			if (hostAndPort[i] == ':') break;
			port = hostAndPort[i] + port;
		}
	}
	return { host, port };
}

class UDPNet
{
public:
	UDPNet() : io_service(new boost::asio::io_context), socket(*io_service) {
		socket.open(udp::v4());
	}

	void send(const string &data, const string &hostAndPort) {
		auto p = ParseHostAndPort(hostAndPort);
		this->send(data, p.first.data(), atoi(p.second.data()));
	}

	void send(const string &data, const char *destIp, uint16_t destPort) {
		///destIp = "127.0.0.1";
		auto receiver_endpoint = udp::endpoint(boost::asio::ip::address::from_string(destIp), destPort);
		try {
			socket.send_to(boost::asio::buffer(data, data.size() + 1), receiver_endpoint);
		}
		catch (std::exception &) {
			// TODO: Handle net errors
		}
	}

	std::pair<const char *, udp::endpoint> receive_from() {
		try {
			if (!socket.available()) return { "",{} };
			udp::endpoint sender_endpoint;
			size_t len = socket.receive_from(
				boost::asio::buffer(recv_buf), sender_endpoint);
			recv_buf.elems[len] = 0;
			return { recv_buf.elems, sender_endpoint };
		}
		catch (...) {
			return { "",{} };
		}
	}

private:
	boost::asio::io_context *const io_service;
	udp::socket socket;
	boost::array<char, 2048> recv_buf = { 0 };
};

struct UDPClient::Impl {
	std::unique_ptr<UDPNet> udpNet{ new UDPNet };
	std::list<NetEvent> events;
	string ipAddress_;
	unsigned short port_ = 0;

	void tick() {
		while (1) {
			const auto pair = udpNet->receive_from();
			const string res = pair.first;
			if (res.empty()) break;
			NetEvent ne;
			ne.type = NetEvent::MESSAGE;
			ne.msg = { res.begin(), res.end() };
			events.push_back(ne);
		}
	}

	bool receive(NetEvent &event) {
		tick();
		if (events.empty()) return false;
		auto m = events.front();
		events.pop_front();
		event = m;
		return true;
	}

	void connect(const char *ipAddress, unsigned short port) {
		if (port_) {
			throw std::runtime_error("already connected");
		}
		ipAddress_ = ipAddress;
		port_ = port;
		NetEvent ne;
		ne.type = NetEvent::CONNECT;
		events.push_back(ne);
	}

	void send(const RawMessage &message, Reliability reliability) {
		const string addr = ipAddress_ + ':' + std::to_string(port_);
		const string s = { message.begin(), message.end() };
		udpNet->send(s, addr);
	}
};

UDPClient::UDPClient() : pImpl(new Impl) {

}

UDPClient::~UDPClient() {
	delete pImpl;
}

void UDPClient::connect(const char *ipAddress, unsigned short port) {
	pImpl->connect(ipAddress, port);
}

bool UDPClient::receive(NetEvent &event) noexcept {
	return pImpl->receive(event);
}

void UDPClient::send(const RawMessage &message, Reliability reliability) noexcept {
	return pImpl->send(message, reliability);
}