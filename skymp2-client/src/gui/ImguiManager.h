#pragma once

class ImguiManager
{
public:
	// Depends on g_pIDirect3D9, g_pIDirect3DDevice9 and TheIInputHook
	// TODO: ����������� - ���������� �� ����������� �� ����������� ���������
	ImguiManager();
	virtual ~ImguiManager();

	void EmitAll(void *inet); // World::INet *
	void AddNetTask(std::function<void(void *)> netTask);

	// List of variables:
	// "interactionMenuBlock" (clock_t)
	// "escapeMenuBlock" (clock_t)
	// Both functions are threadsafe
	void SetVar(const char *name, float value);
	float GetVar(const char *name) const;

	bool IsBlocked(const char *varName) const {
		return this->GetVar(varName) >= (float)clock();
	}

	void api_OnInteractionClick(string targetName, int index) {
		this->TriggerEvent({ "interactionClick", targetName, std::to_string(index) });
	}

	void api_OnBroadcastIn(formid ownerId, uint8_t contentType, const string &content) {
		this->TriggerEvent({ "broadcastIn", std::to_string(int(contentType)), content, std::to_string(ownerId) });
	}

protected:
	void SetCursorVisible(bool v);
	float GetWindowWidth() const;
	float GetWindowHeight() const;
	virtual void OnRender(void *fontNickname) = 0;
	virtual bool GetKeyPressed(int vkCode) = 0;

private:
	void TriggerEvent(vector<string> arguments);
	vector<string> NextEvent(); // returns empty vector on fail

	void Render(std::function<void()> luBaOnRender);

	ImguiManager(const ImguiManager &) = delete;
	void operator=(const ImguiManager &) = delete;

	struct Impl;
	Impl *pImpl;
};