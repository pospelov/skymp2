#pragma once
#include "../libs/imgui.h"

namespace ImguiUtils
{
	void BeginInvisible(const char *id, ImVec2 pos, bool focus, bool supersize = false);
	void EndInvisible();
	void DrawImageNoWindow(LPDIRECT3DDEVICE9, const char *path, float optionalWidth = 0.f, float optionalHeight = 0.f);
	void DrawImage(LPDIRECT3DDEVICE9, const char *path, ImVec2 pos, float widthMult = 1.0f, float heightMult = 1.0f, bool focus = false, const char *id = 0);
	void DrawText_(string text, ImVec2 pos, bool withColors, string uniqueId);
}