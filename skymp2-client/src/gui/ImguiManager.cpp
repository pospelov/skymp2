#include "../stdafx.h"
#include "ImguiManager.h"

#include "../directx/DirectXHook.h"
#include "../dinput/DInput.hpp"
#include "../libs/InputConverter.h"
#include "../libs/imgui.h"
#include "../libs/imgui/imgui_impl_dx9.h"
#include "ImguiUtils.h"
#include "Localization.h"
#include "../TaskQueue.h"
#include "../sdutil/PlayWav.h"
#include "../sdutil/FormInfo.h"
#include "../components/IILocalPlayer.h"
#include <filesystem>

#include "../services/GUI.h" // For SetCursor in api
#include "../services/World.h" // For World::INet

extern "C"
{
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}
#include <LuaBridge/LuaBridge.h>

// https://ru.stackoverflow.com/questions/783946/%D0%9A%D0%BE%D0%BD%D0%B2%D0%B5%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D1%82%D1%8C-%D0%B2-%D0%BA%D0%BE%D0%B4%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D1%83-utf8
string cp1251_to_utf8(const char *str) {
	string res;
	int result_u, result_c;
	result_u = MultiByteToWideChar(1251, 0, str, -1, 0, 0);
	if (!result_u) { return ""; } // ����� �����, ��������� ����� ������� ���������
	wchar_t *ures = new wchar_t[result_u];
	if (!MultiByteToWideChar(1251, 0, str, -1, ures, result_u)) {
		delete[] ures;
		return "";
	}
	result_c = WideCharToMultiByte(65001, 0, ures, -1, 0, 0, 0, 0);
	if (!result_c) {
		delete[] ures;
		return "";
	}
	char *cres = new char[result_c];
	if (!WideCharToMultiByte(65001, 0, ures, -1, cres, result_c, 0, 0)) {
		delete[] cres;
		return "";
	}
	delete[] ures;
	res.append(cres);
	delete[] cres;
	return res;
}

// imgui_lua_bindings.cpp
extern lua_State* lState;
void LoadImguiBindings();

string g_scriptsDir;

#define TAKE_SCREENSHOTS FALSE

#define FONT_LOAD_SIZE 20
//#define PIXELS(x) (x/FONT_LOAD_SIZE) // use with SetWindowFontScale

class FunctionalInputListener : public InputListener
{
public:

	virtual void OnPress(uint8_t code) { if (onPress) onPress(code); }
	virtual void OnRelease(uint8_t code) { if (onRelease) onRelease(code); }
	virtual void OnMousePress(uint8_t code) { if (onMousePress) onMousePress(code); }
	virtual void OnMouseRelease(uint8_t code) { if (onMouseRelease) onMouseRelease(code); }
	virtual void OnMouseMove(unsigned int x, unsigned int y, unsigned int z) { if (onMouseMove) onMouseMove(x, y, z); }

	std::function<void(uint8_t)> onPress, onRelease, onMousePress, onMouseRelease;
	std::function<void(float, float, float)> onMouseMove;
};

struct ImguiManager::Impl
{
	class Data
	{
	public:
		Data()
		{
			mouseX = 0;
			mouseY = 0;
		}

		std::atomic<int> mouseX, mouseY;
		boost::lockfree::queue<uint8_t, boost::lockfree::capacity<65534>> input;
	};

	Data data;
	std::atomic<bool> cursorVisible;

	std::recursive_mutex varsMutex;
	std::map<string, float> vars;

	std::recursive_mutex eventsMutex;
	std::queue<vector<string>> events;

	std::recursive_mutex netTasksMutex;
	using NetTask = std::function<void(World::INet &)>;
	std::queue<NetTask> netTasks;

	void AddNetTask(NetTask task) {
		std::lock_guard<std::recursive_mutex> l(netTasksMutex);
		netTasks.push(task);
	}
};

std::unordered_map<int, ImFont*> font2;

// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/d3dxsavesurfacetofile
#include <D3dx9tex.h> // D3DXSaveSurfaceToFile

// https://stackoverflow.com/questions/1962142/take-screenshot-of-directx-full-screen-application
__forceinline void TakeScreenshot() {

	static TaskQueue g_screenQ;

	bool threadStarted = false;
	if (!threadStarted) {
		threadStarted = true;
		std::thread([] {
			while (1) {
				Sleep(1000);
				g_screenQ.RunTopAndClear();
			}
		}).detach();
	}

	auto pDev = g_pIDirect3DDevice9;
	auto hWnd = FindWindow("Skyrim", "Skyrim");

	static char *szFilename = nullptr;
	if (!szFilename) {
		szFilename = new char[MAX_PATH];
		GetTempPathA(MAX_PATH, szFilename);
		sprintf(szFilename, "%s/%s", szFilename, "zzzrep_a.bin");
	}

	IDirect3DSurface9* surface;
	D3DDISPLAYMODE mode;
	pDev->GetDisplayMode(0, &mode); // pDev is my *IDirect3DDevice
	// we can capture only the entire screen,
	// so width and height must match current display mode
	pDev->CreateOffscreenPlainSurface(mode.Width, mode.Height, D3DFMT_A8R8G8B8, D3DPOOL_SCRATCH, &surface, NULL);
	std::shared_ptr<IDirect3DSurface9> pSurface(surface, [](IDirect3DSurface9 *s) {
		s->Release();
	});
	if (pDev->GetFrontBufferData(0, surface) == D3D_OK)
	{
		g_screenQ.Add([pSurface] {
			D3DXSaveSurfaceToFile(szFilename, D3DXIFF_JPG, &*pSurface, NULL, NULL);
		});
	}
}

void api_DrawList_AddConvexPolyFilled(luabridge::LuaRef points, uint32_t col) {
	vector<ImVec2> ps;
	for (int i = 1; i <= points.length(); ++i) {
		ps.push_back({ points[i][1], points[i][2] });
	}
	ImGui::GetOverlayDrawList()->AddConvexPolyFilled(ps.data(), ps.size(), col);
}

void api_SetMousePos(float x, float y) {
}

float api_GetMouseX() {
	return ImGui::GetIO().MousePos.x;
}

float api_GetMouseY() {
	return ImGui::GetIO().MousePos.y;
}

bool api_IsItemHovered() {
	return ImGui::IsItemHovered();
}

void api_PushSkympFont(int i) {
	try {
		ImGui::PushFont(font2.at(i));
	}
	catch (...) {
		ImGui::PushFont(font2.begin()->second);
	}
}

void api_Begin2(string id, int flags) {
	ImGui::Begin(id.data(), nullptr, flags);
}

float api_GetDisplayWidth() {
	return ImGui::GetIO().DisplaySize.x;
}

float api_GetDisplayHeight() {
	return ImGui::GetIO().DisplaySize.y;
}

volatile bool g_isCursorVisible = false;

bool api_IsCursorVisible() {
	return g_isCursorVisible;
}

volatile bool g_setCursorVisible = false;
volatile bool g_setCursorInvisible = false;
void api_SetCursorVisible(bool visible) {
	if (visible) g_setCursorVisible = true;
	else g_setCursorInvisible = true;
}

void api_SetWindowPos(float x, float y) {
	ImGui::SetWindowPos({ x, y });
}

void api_SetWindowSize(float x, float y) {
	ImGui::SetWindowSize({ x, y });
}

ImguiManager *g_imguiManager = nullptr;

void api_DisableMenu(const string &menuName, float ms) {
	assert(g_imguiManager);
	if (!g_imguiManager) return;

	if (menuName == "interactionMenu" || menuName == "escapeMenu") {
		string var = menuName + "Block";
		if (std::isfinite(ms)) g_imguiManager->SetVar(var.data(), clock() + int(ms));
	}
}

void api_Image(string path, float width, float height) {
	if (isfinite(width) == false) width = 0;
	if (isfinite(height) == false) height = 0;
	path = g_scriptsDir + "assets/" + path;
	if (std::filesystem::exists(path)) {
		ImguiUtils::DrawImageNoWindow(g_pIDirect3DDevice9, path.data(), width, height);
	}
	else {
		ImGui::Text("?");
	}
}

void api_PlaySound(formid soundId) {
	assert(g_imguiManager);
	if (!g_imguiManager) return;
	dynamic_cast<GUI *>(g_imguiManager)->AddTask([=] {
		auto sound = (BGSSoundDescriptorForm *)LookupFormByID(soundId);
		if (sound && sound->formType == kFormType_SoundDescriptor) {
			auto soundM = (TESSound *)LookupFormByID(0x14004);
			soundM->descriptor = sound;
			sd::Play(soundM, *g_thePlayer);
		}
	});
}

luabridge::LuaRef api_WorldPointToScreenPoint(float x, float y, float z) {
	ImVec2 p;
	float screenZ;
	auto pcam = PlayerCamera::GetSingleton();
	pcam->WorldPtToScreenPt3({ x, y, z }, p.x, p.y, screenZ);
	if (screenZ < 0) p = ImVec2{ -10000000.f,-10000000.f };
	else {
		p.x *= api_GetDisplayWidth();
		p.y = (1 - p.y) * api_GetDisplayHeight();
	}
	auto t = luabridge::newTable(lState);
	t.append(p.x);
	t.append(p.y);
	return t;
}

void api_Broadcast(uint32_t contentType, const string &content, luabridge::LuaRef ownerId) {
	assert(g_imguiManager);
	if (!g_imguiManager) return;

	formid ownerIdInt = 0;
	if (ownerId.isNumber()) {
		auto asFloat = ownerId.cast<float>();
		if (std::isfinite(asFloat)) ownerIdInt = asFloat;
	}
	g_imguiManager->AddNetTask([contentType, content, ownerIdInt](void *net) {
		assert(net);
		((World::INet *)net)->Broadcast(ownerIdInt, (uint8_t)contentType, content, 10);
	});
}

extern string g_name;
// ���� ����� �� ������, ����� race condition)))0
string api_GetMyName() {
	return g_name;
}

void api_LoadMyInventory() {
	assert(g_imguiManager);
	if (!g_imguiManager) return;
	g_imguiManager->AddNetTask([](void *net) {
		assert(net);
		((World::INet *)net)->LoadMyInventory();
	});
}

TaskQueue g_sendScriptEventQ;
uint64_t g_freeCallbackId = 0;
std::unordered_map<uint64_t, std::function<void(int, string)>> g_callbacks;

void api_SendScriptEvent(string eventName, string content, luabridge::LuaRef callback) {
	assert(g_imguiManager);
	if (!g_imguiManager) return;

	auto id = g_freeCallbackId++;
	g_callbacks[id] = [callback](int code, string body) {
		callback(code, body);
	};

	// callback ������ ����������, ��� ��� ��� ����������� ����������� ������� �� lua_state
	// ��������� �������� � ����������������
	g_imguiManager->AddNetTask([eventName, content, id](void *net) {
		assert(net);
		((World::INet *)net)->SendScriptEvent(eventName, content, [id](int code, string body) {
			g_sendScriptEventQ.Add([=] {
				auto callback = g_callbacks[id];
				g_callbacks.erase(id);
				callback(code, body);
			});
		});
	});
}

void api_PrintNote(string str) {
	assert(g_imguiManager);
	if (!g_imguiManager) return;
	dynamic_cast<GUI *>(g_imguiManager)->AddTask([=] {
		sd::PrintNote("%s", str.data());
	});
}

void api_Print(const string &str) {
	Console_Print("%s", str.data());
}

luabridge::LuaRef api_Ls() {
	auto res = luabridge::newTable(lState);
	for (auto &p : std::filesystem::recursive_directory_iterator(g_scriptsDir)) {
		if (!p.is_directory()) {
			res.append(p.path().string());
		}
	}
	return res;
}

// skymp

bool g_invInfoTasked = false;
std::recursive_mutex g_invM;
struct {
	uint32_t numSlots = 0;
	vector<uint32_t> ids, counts;
	vector<string> names;
} g_inv;

uint32_t api_GetNumSlots() {
	std::lock_guard<std::recursive_mutex> l(g_invM);
	return g_inv.numSlots;
}

formid api_GetNthSlotId(uint32_t n) {
	--n;
	std::lock_guard<std::recursive_mutex> l(g_invM);
	if (n >= g_inv.ids.size()) return 0;
	return g_inv.ids[n];
}

uint32_t api_GetNthSlotCount(uint32_t n) {
	--n;
	std::lock_guard<std::recursive_mutex> l(g_invM);
	if (n >= g_inv.counts.size()) return 0;
	return g_inv.counts[n];
}

string api_GetNthSlotName(uint32_t n) {
	--n;
	std::lock_guard<std::recursive_mutex> l(g_invM);
	if (n >= g_inv.names.size()) return 0;
	return g_inv.names[n].data();
}

int api_GetFormType(formid id) {
	if (id >= 0xff000000) return -1;
	auto form = LookupFormByID(id);
	return form ? form->formType : -1;
}

string api_GetFormName(formid id) {
	auto form = LookupFormByID(id);
	if (!form || id >= 0xff000000) return "";
	return cp1251_to_utf8(papyrusForm::GetName(form).data);
}

string api_GetFormInfo(formid id) {
	auto info = FormInfo::Resolve(id);
	return info.espm + ':' + info.idToHex();
}

void api_UpdateInventoryInfo() {
	assert(g_imguiManager);
	if (!g_imguiManager) return;

	static volatile bool g_updating = false;
	if (g_updating) return;
	g_updating = true;

	dynamic_cast<GUI *>(g_imguiManager)->AddTask([] {
		auto inv = IILocalPlayer::GetFrom(*g_thePlayer);
		std::lock_guard<std::recursive_mutex> l(g_invM);
		g_inv = {};
		for (auto &pair : inv.simple) {
			++g_inv.numSlots;
			g_inv.ids.push_back(pair.first);
			g_inv.counts.push_back(pair.second);

			auto form = LookupFormByID(pair.first);
			if (form) {
				g_inv.names.push_back(
					cp1251_to_utf8(papyrusForm::GetName(form).data)
				);
				continue;
			}
			g_inv.names.push_back("Unknown");
		}
		g_updating = false;
	});
}

ImguiManager::ImguiManager() {
	g_imguiManager = this;
	pImpl = new Impl;

	g_scriptsDir = SKYMP2_FILES_DIR"\\scripts\\";
	if (std::filesystem::exists("scripts_path.txt")) {
		std::ifstream t("scripts_path.txt");
		g_scriptsDir = string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
	}
	if (!g_scriptsDir.empty() && g_scriptsDir.back() != '\\') {
		if (g_scriptsDir.back() == '/') g_scriptsDir.pop_back();
		g_scriptsDir += '\\';
	}
	Console_Print("Reading scripts from: %s", g_scriptsDir.data());
	auto value = (g_scriptsDir + "gamemodes/?.lua" + ";"
		+ g_scriptsDir + "gamemodes/?.t" + ";"
		+ g_scriptsDir + "?.lua" + ";"
		+ g_scriptsDir + "?.t");
	_putenv_s("LUA_PATH", value.data());


	auto hwnd = FindWindow("Skyrim", "Skyrim");

	// Setup ImGui binding
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
	ImGui_ImplDX9_Init(hwnd, g_pIDirect3DDevice9);

	// Setup style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsLight();

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them. 
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple. 
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Read 'misc/fonts/README.txt' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesCyrillic());

	ImFont* font = io.Fonts->AddFontFromFileTTF(SKYMP2_FILES_DIR "/FuturaMediumCTT1.ttf", FONT_LOAD_SIZE * 1, NULL, io.Fonts->GetGlyphRangesCyrillic());

	font2[-1] = io.Fonts->AddFontFromFileTTF(SKYMP2_FILES_DIR "/FuturaMediumCTT1.ttf", 22, NULL, io.Fonts->GetGlyphRangesCyrillic());
	font2[-2] = io.Fonts->AddFontFromFileTTF(SKYMP2_FILES_DIR "/FuturaMediumCTT1.ttf", 12, NULL, io.Fonts->GetGlyphRangesCyrillic());

	for (int i = 20; i != 55; i++) {
		if (i % 2 == 1) {
			assert(i - 1 >= 20);
			font2[i] = font2[i - 1];
		}
		else
			font2[i] = io.Fonts->AddFontFromFileTTF(SKYMP2_FILES_DIR "/futuralightc.otf", i, NULL, io.Fonts->GetGlyphRangesCyrillic());
	}

	auto MUST_BE_LAST_LOADED_FONT_EVER = io.Fonts->AddFontFromFileTTF(SKYMP2_FILES_DIR "/futuralightc.otf", 24, NULL, io.Fonts->GetGlyphRangesCyrillic());

	// Initialize Lua:
	auto L = luaL_newstate();
	lState = L;
	luaL_openlibs(lState);

	auto initScript = [L] {
		auto p = (g_scriptsDir + "main.lua");
		if (std::filesystem::exists(p)) {
			if (luaL_dofile(L, p.data())) {
				string error = lua_tostring(L, -1);
				throw std::logic_error(error);
			}
		}
		else {
			throw std::logic_error("Lua: " + p + " not found");
		}
	};
	auto patchImguiBindings = [L] {
		luabridge::getGlobalNamespace(L)
			.beginNamespace("imgui")
			.addFunction("DrawList_AddConvexPolyFilled", api_DrawList_AddConvexPolyFilled)
			.addFunction("SetMousePos", api_SetMousePos)
			.addFunction("GetMouseX", api_GetMouseX)
			.addFunction("GetMouseY", api_GetMouseY)
			.addFunction("IsItemHovered", api_IsItemHovered)
			.addFunction("PushSkympFont", api_PushSkympFont)
			.addFunction("IsCursorVisible", api_IsCursorVisible)
			.addFunction("SetCursorVisible", api_SetCursorVisible)
			.addFunction("SetWindowSize", api_SetWindowSize)
			.addFunction("SetWindowPos", api_SetWindowPos)
			.addFunction("Begin2", api_Begin2)
			.addFunction("GetDisplayWidth", api_GetDisplayWidth)
			.addFunction("GetDisplayHeight", api_GetDisplayHeight)
			.addFunction("DisableMenu", api_DisableMenu)
			.addFunction("Image", api_Image)
			.addFunction("PlaySound", api_PlaySound)
			.addFunction("WorldPointToScreenPoint", api_WorldPointToScreenPoint)
			.endNamespace();
		luabridge::getGlobalNamespace(L)
			.beginNamespace("skymp")
			.addFunction("GetNumSlots", api_GetNumSlots)
			.addFunction("GetNthSlotId", api_GetNthSlotId)
			.addFunction("GetNthSlotCount", api_GetNthSlotCount)
			.addFunction("GetNthSlotName", api_GetNthSlotName)
			.addFunction("GetFormType", api_GetFormType)
			.addFunction("GetFormName", api_GetFormName)
			.addFunction("GetFormInfo", api_GetFormInfo)
			.addFunction("UpdateInventoryInfo", api_UpdateInventoryInfo)
			.addFunction("Broadcast", api_Broadcast)
			.addFunction("GetMyName", api_GetMyName)
			.addFunction("LoadMyInventory", api_LoadMyInventory)
			.addFunction("SendScriptEvent", api_SendScriptEvent)
			.addFunction("PrintNote", api_PrintNote)
			.endNamespace();
		luabridge::getGlobalNamespace(L)
			.addFunction("print", api_Print)
			.addFunction("ls", api_Ls);
	};
	LoadImguiBindings();
	patchImguiBindings();
	initScript();

	static std::atomic<bool> g_changed = false;
	std::thread([] {
		while (1) {
			HANDLE hDir;
			hDir = FindFirstChangeNotification(g_scriptsDir.data(),
				TRUE, FILE_NOTIFY_CHANGE_LAST_WRITE);
			if (hDir == INVALID_HANDLE_VALUE) {
				Sleep(1000);
				continue;
			}
			WaitForSingleObject(hDir, INFINITE);
			g_changed = true;
			FindCloseChangeNotification(hDir);
		}
	}).detach();

	g_pIDirect3DDevice9->OnPresent.connect([this, L, initScript, patchImguiBindings](void *) {
		ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowRounding, 0);
		try {
			if (g_changed) {
				g_changed = false;
				LoadImguiBindings();
				patchImguiBindings();
				initScript();
			}
			auto luaOnRender = luabridge::getGlobal(L, "onRender");
			auto luaOnEvent = luabridge::getGlobal(L, "onEvent");

			g_isCursorVisible = this->pImpl->cursorVisible; // g_isCursorVisible is used in Lua api

			// Set cursor visible or invisible if tasked from Lua
			if (g_setCursorVisible) {
				dynamic_cast<GUI *>(this)->SetCursor(true);
				g_setCursorVisible = false;
			}
			if (g_setCursorInvisible) {
				dynamic_cast<GUI *>(this)->SetCursor(false);
				g_setCursorInvisible = false;
			}

			this->Render([=] {
				g_sendScriptEventQ.Update();
				while (1) {
					auto evn = this->NextEvent();
					if (evn.empty()) break;
					auto arguments = luabridge::newTable(L);
					for (size_t i = 1; i < evn.size(); ++i) {
						arguments.append(evn[i]);
					}
					luaOnEvent(evn[0], arguments);
				}
				if (luaOnRender.isFunction()) {
					luaOnRender();
				}
			});
		}
		catch (std::exception &e) {
			Console_Print(e.what());
		}
		ImGui::PopStyleVar();
	});
	if (TAKE_SCREENSHOTS) {
		g_pIDirect3DDevice9->OnPresent.connect([](void *) {
			static clock_t g_lastScreenshot = 0;
			if (clock() - g_lastScreenshot > 1000) {
				if (GetForegroundWindow() == FindWindow("Skyrim", "Skyrim")) {
					TakeScreenshot();
				}
				g_lastScreenshot = clock();
			}
		});
	}

	auto listener = new FunctionalInputListener;
	listener->onMouseMove = [this](float newX, float newY, float newZ) {
		pImpl->data.mouseX = (int)newX;
		pImpl->data.mouseY = (int)newY;
	};
	listener->onPress = [this](uint8_t code) {
		pImpl->data.input.push(code);
	};

	TheIInputHook->AddListener(listener);
}

ImguiManager::~ImguiManager() {
	delete pImpl;
	pImpl = nullptr;
}

void ImguiManager::EmitAll(void *inet) {
	assert(inet);
	auto &net = *(World::INet *)inet;
	std::lock_guard<std::recursive_mutex> l(pImpl->netTasksMutex);
	while (pImpl->netTasks.empty() == false) {
		auto task = pImpl->netTasks.front();
		pImpl->netTasks.pop();
		task(net);
	}
}

void ImguiManager::AddNetTask(std::function<void(void *)> task) {
	pImpl->AddNetTask([=](World::INet &net) { task(&net); });
}

void ImguiManager::SetVar(const char *name, float value) {
	std::lock_guard<std::recursive_mutex> l(pImpl->varsMutex);
	pImpl->vars[name] = value;
}

float ImguiManager::GetVar(const char *name) const {
	try {
		std::lock_guard<std::recursive_mutex> l(pImpl->varsMutex);
		return pImpl->vars.at(name);
	}
	catch (...) {
		return 0.f;
	}
}

inline ImVec2 GetSkyrimWindowSize() {
	auto hwnd = FindWindow("Skyrim", "Skyrim");
	static RECT *rect = nullptr;
	if (!rect) {
		rect = new RECT;
		GetWindowRect(hwnd, rect);
	}
	LONG width = rect->right - rect->left;
	LONG height = rect->bottom - rect->top;
	return ImVec2(width, height);
}

void ImguiManager::SetCursorVisible(bool v) {
	pImpl->cursorVisible = v;
}

float ImguiManager::GetWindowWidth() const {
	return GetSkyrimWindowSize().x;
}

float ImguiManager::GetWindowHeight() const {
	return GetSkyrimWindowSize().y;
}

void ImguiManager::TriggerEvent(vector<string> arguments) {
	std::lock_guard<std::recursive_mutex> l(pImpl->eventsMutex);
	pImpl->events.push(arguments);
}

vector<string> ImguiManager::NextEvent() {
	std::lock_guard<std::recursive_mutex> l(pImpl->eventsMutex);
	if (pImpl->events.empty()) return {};
	auto res = pImpl->events.front();
	pImpl->events.pop();
	return res;
}

void ImguiManager::Render(std::function<void()> luaOnRender) {
	// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
	// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
	// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
	// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.turn;

	ImGuiIO& io = ImGui::GetIO();
	io.MouseDrawCursor = false;

	{
		io.MouseDown[0] = this->GetKeyPressed(VK_LBUTTON);
		io.MouseDown[1] = this->GetKeyPressed(VK_RBUTTON);
		io.MouseDown[2] = this->GetKeyPressed(VK_MBUTTON);
		io.KeyCtrl = this->GetKeyPressed(VK_CONTROL);
		io.KeyAlt = this->GetKeyPressed(VK_MENU);
		io.KeyShift = this->GetKeyPressed(VK_SHIFT);
		io.KeySuper = this->GetKeyPressed(VK_LWIN) || sd::GetKeyPressed(VK_RWIN);


		for (int32_t i = 0; i != 256; ++i)
		{
			io.KeysDown[i] = this->GetKeyPressed(i);
		}

		io.MousePos = ImVec2(pImpl->data.mouseX, pImpl->data.mouseY);

		static bool capspwas = false;
		bool capsp = this->GetKeyPressed(VK_CAPITAL);
		static bool caps = false;
		if (capsp != capspwas) {
			capspwas = capsp;
			if (capsp) caps = !caps;
		}

		while (1) {
			static std::unordered_map<wchar_t, wchar_t> ruByEn, enByRu;
			static bool filled = false;
			if (!filled) {
				filled = 1;
				const static std::wstring ru = L_ABC_RU;
				const static std::wstring en = L_ABC_EN;
				for (int i = 0; i != ru.size(); ++i) {
					ruByEn[en[i]] = ru[i];
					enByRu[ru[i]] = en[i];
				}
			}

			static bool rus = false;
			bool pressedComb = this->GetKeyPressed(VK_LSHIFT) && this->GetKeyPressed(VK_MENU);
			static bool pressedWas = false;
			if (pressedComb != pressedWas) {
				pressedWas = pressedComb;
				if (pressedComb) {
					rus = !rus;
				}
			}

			uint8_t scan;
			if (!pImpl->data.input.pop(scan)) break;
			wchar_t wstr[] = { (wchar_t)input::ScanCodeToText(scan), 0 };

			if (wstr[0] != L'.') {
				if (rus && ruByEn.count(wstr[0])) {
					wstr[0] = ruByEn[wstr[0]];
				}
				if (!rus && enByRu.count(wstr[0])) {
					wstr[0] = enByRu[wstr[0]];
				}
			}


			std::wstring abc = L"�������������������������������� qwertyuiopasdfghjklzxcvbnm �`1234567890";
			std::wstring ABC = L"�������������������������������� QWERTYUIOPASDFGHJKLZXCVBNM �~!\"�;%:?*()";
			assert(abc.size() == ABC.size());
			int k = -1;
			for (int i = 0; i != abc.size(); ++i) {
				if (wstr[0] == abc[i] || wstr[0] == ABC[i]) {
					k = i;
				}
			}
			bool shift = (this->GetKeyPressed(VK_LSHIFT) || this->GetKeyPressed(VK_RSHIFT));
			if (k != -1) {
				if (caps) std::swap(ABC, abc);
				wstr[0] = shift ? ABC[k] : abc[k];
			}
			if (rus && wstr[0] == L"/"[0]) wstr[0] = L'.';
			else if (rus && wstr[0] == L"."[0]) wstr[0] = L',';
			if (rus && wstr[0] == L',') wstr[0] = (caps || shift) ? L'�' : L'�'; // YU : yu
			if (rus && wstr[0] == L'.' && shift) wstr[0] = L',';

			bool discard = this->GetKeyPressed(VK_CONTROL);
			if (!discard) io.AddInputCharacter(wstr[0]);
		}
	}

	ImGui_ImplDX9_NewFrame();

	{
		auto &style = ImGui::GetStyle();
		style.Colors[ImGuiCol_::ImGuiCol_ResizeGrip] = ImVec4(0, 0, 0, 1);
		style.Colors[ImGuiCol_::ImGuiCol_ResizeGripActive] = ImVec4(10 / 256.0, 10 / 256.0, 10 / 256.0, 1);
		style.Colors[ImGuiCol_::ImGuiCol_ResizeGripHovered] = ImVec4(12 / 256.0, 12 / 256.0, 12 / 256.0, 1);

		style.Colors[ImGuiCol_::ImGuiCol_Border] = ImVec4(0, 0, 0, 0);

		style.Colors[ImGuiCol_::ImGuiCol_Button] = ImVec4(5 / 256.0, 5 / 256.0, 5 / 256.0, 1);
		style.Colors[ImGuiCol_::ImGuiCol_ButtonActive] = ImVec4(10 / 256.0, 10 / 256.0, 10 / 256.0, 1);
		style.Colors[ImGuiCol_::ImGuiCol_ButtonHovered] = ImVec4(12 / 256.0, 12 / 256.0, 12 / 256.0, 1);

		style.Colors[ImGuiCol_::ImGuiCol_TitleBg] = ImVec4(6 / 256.0, 6 / 256.0, 6 / 256.0, 1);
		style.Colors[ImGuiCol_::ImGuiCol_TitleBgActive] = ImVec4(10 / 256.0, 10 / 256.0, 10 / 256.0, 1);
		style.Colors[ImGuiCol_::ImGuiCol_TitleBgCollapsed] = ImVec4(6 / 256.0, 6 / 256.0, 6 / 256.0, 1);

		style.Colors[ImGuiCol_::ImGuiCol_FrameBg] = ImVec4(3 / 256.0, 3 / 256.0, 3 / 256.0, 0.75);
		style.Colors[ImGuiCol_::ImGuiCol_FrameBgHovered] = ImVec4(3 / 256.0, 3 / 256.0, 3 / 256.0, 0.75);
		style.Colors[ImGuiCol_::ImGuiCol_FrameBgActive] = ImVec4(3 / 256.0, 3 / 256.0, 3 / 256.0, 0.75);

		luaOnRender();

		style.ScrollbarRounding = 15;


		const float DEV_WIDTH = 1920.0f;
		const float DEV_HEIGHT = 1080.0f;

		auto width = GetSkyrimWindowSize().x;
		auto height = GetSkyrimWindowSize().y;
		const float widthMult = width / DEV_WIDTH,
			heightMult = height / DEV_HEIGHT;

		static auto fsMainMenu = new BSFixedString("Main Menu");
		const bool logoVisible = MenuManager::GetSingleton()->IsMenuOpen(fsMainMenu);
		if (logoVisible) {

			const ImVec2 versionPos(width * 0.9, height * 0.9);
			static string version;
			static string build;
			if (version.empty()) std::ifstream(SKYMP2_FILES_DIR "/version.txt") >> version;
			if (build.empty()) std::ifstream(SKYMP2_FILES_DIR "/build-id.txt") >> build;
			string versionFormatted = version + " {BEBEBE}(build " + build + ")";
			ImguiUtils::DrawText_(versionFormatted.data(), versionPos, true, "versionTextUniqueId");

			const ImVec2 siriusLogoPos(width * 0.15, height * 0.840);
			ImguiUtils::DrawImage(g_pIDirect3DDevice9, SKYMP2_FILES_DIR "/sirius.png", siriusLogoPos, widthMult * 0.5, heightMult * 0.5);

			const ImVec2 mpLogoPos(width * 0.2185, 0);
			ImguiUtils::DrawImage(g_pIDirect3DDevice9, SKYMP2_FILES_DIR "/skymp.png", mpLogoPos, widthMult * 0.50, heightMult * 0.50);

			bool isLoading = getenv("SKYMP2_LOADING") && getenv("SKYMP2_LOADING") == string("1");

			static bool wasLoadSession = false;

			static float percent = 0;
			static bool finished = false;
			static ImVec4 col = { 1,1,1,0.5 };

			bool renderLine = false;
			if (isLoading) {
				wasLoadSession = true;
				renderLine = true;
			}
			if (!isLoading && wasLoadSession) {
				renderLine = true;
				percent = 100;

				// On Progress Bar Finish
				if (!finished) {
					finished = true;
				}
			}

			if (renderLine) {
				ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowMinSize, { 0,0 });
				ImGui::PushStyleVar(ImGuiStyleVar_::ImGuiStyleVar_WindowRounding, 0);
				ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, col);
				ImGui::Begin(" ###progressBar", NULL, ImGuiWindowFlags_::ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_::ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_::ImGuiWindowFlags_NoInputs);
				const ImVec2 pos(width * 0, height * 0.833);
				ImGui::SetWindowPos(pos);
				auto percentRaw = getenv("LOADING_PERCENTAGE");
				if (!percentRaw) percentRaw = "0";

				int inPercent = atoi(percentRaw);
				if (percent < inPercent) percent += abs(percent - inPercent) * 0.1f;

				if (percent >= 130) {
					col.w -= 0.1f;
					col.w = std::max(col.w, 0.f);
				}
				else if (percent >= 95) {
					static bool plus = 1;
					if (plus) {
						col.w += 0.01;
						if (col.w >= 1) plus = 0;
					}
					else {
						col.w -= 0.01;
						if (col.w <= 0.5) plus = 1;
					}
				}
				if (percent <= 0 || percent >= 140) col.w = 0;

				ImGui::SetWindowSize({ width * percent / 100.f, 2 });
				ImGui::End();
				ImGui::PopStyleVar(2);
				ImGui::PopStyleColor();
			}
		}

		this->OnRender(&font2);

		TheIInputHook->SetInputEnabled(!pImpl->cursorVisible.load());
		if (pImpl->cursorVisible.load())
			ImguiUtils::DrawImage(g_pIDirect3DDevice9, SKYMP2_FILES_DIR "/cursor.png", { io.MousePos.x - 8.f, io.MousePos.y - 12.f }, widthMult * 1, heightMult * 1, true);
	}

	g_pIDirect3DDevice9->SetRenderState(D3DRS_ZENABLE, false);
	g_pIDirect3DDevice9->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	g_pIDirect3DDevice9->SetRenderState(D3DRS_SCISSORTESTENABLE, false);

	if (g_pIDirect3DDevice9->BeginScene() >= 0)
	{
		ImGui::Render();
		ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
		g_pIDirect3DDevice9->EndScene();
	}
	//HRESULT result = g_pIDirect3DDevice9->Present(NULL, NULL, NULL, NULL);

	// Handle loss of D3D9 device
	/*if (result == D3DERR_DEVICELOST && g_pIDirect3DDevice9->TestCooperativeLevel() == D3DERR_DEVICENOTRESET)
	{
		ImGui_ImplDX9_InvalidateDeviceObjects();
		g_pIDirect3DDevice9->Reset(&pImpl->g_d3dpp);
		ImGui_ImplDX9_CreateDeviceObjects();
	}*/
}