#include "../stdafx.h"
#include "ImguiUtils.h"
#include "../gui/imgui_multicolor.h"

// D3DXCreateTextureFromFileA
// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/d3dxcreatetexturefromfile
// https://stackoverflow.com/questions/17015088/missing-files-directx-sdk-d3dx9-lib-d3dx9-h
#include <D3dx9tex.h>
#pragma comment( lib, "D3dx9.lib" )

void ImguiUtils::BeginInvisible(const char *id, ImVec2 pos, bool focus, bool superSize)
{
	// Invisible window  
	// https://github.com/ocornut/imgui/issues/530
	ImGui::PushStyleColor(ImGuiCol_::ImGuiCol_WindowBg, ImVec4(0, 0, 0, 0));
	ImGuiWindowFlags flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoInputs | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing;
	//if (focus) flags |= ImGuiWindowFlags_Tooltip | ImGuiWindowFlags_AlwaysUseWindowPadding;

	if (focus) {
		ImGui::SetNextWindowFocus();
	}

	ImGui::Begin(id, nullptr, flags);
	ImGui::SetWindowPos(pos);
	if (superSize) ImGui::SetWindowSize(ImVec2(1000, 1000));
}

void ImguiUtils::EndInvisible()
{
	ImGui::End();
	ImGui::PopStyleColor();
}

void ImguiUtils::DrawImageNoWindow(LPDIRECT3DDEVICE9 device, const char *path, float optionalW, float optionalH) {
	// Load a texture
	static std::map<std::string, LPDIRECT3DTEXTURE9> my_texture;
	if (!my_texture[path]) {
		HRESULT res = D3DXCreateTextureFromFileA(device, path, &my_texture[path]);
		IM_ASSERT(res == 0);
		IM_ASSERT(my_texture[path] != NULL);
	}
	// Retrieve texture info
	D3DSURFACE_DESC my_texture_desc;
	HRESULT res = my_texture[path]->GetLevelDesc(0, &my_texture_desc);
	IM_ASSERT(res == 0);

	ImVec2 uv_min(0.0f, 0.0f);
	ImVec2 uv_max(1.0f, 1.0f);

	auto size = ImVec2((float)my_texture_desc.Width, (float)my_texture_desc.Height);
	if (abs(optionalW) > 0.01) size.x = optionalW;
	if (abs(optionalH) > 0.01) size.y = optionalH;
	ImGui::Image(my_texture[path], size, uv_min, uv_max);
}

void ImguiUtils::DrawImage(LPDIRECT3DDEVICE9 device, const char *path, ImVec2 pos, float widthMult, float heightMult, bool focus, const char *id) {
	// Load a texture
	static std::map<std::string, LPDIRECT3DTEXTURE9> my_texture;
	if (!my_texture[path]) {
		HRESULT res = D3DXCreateTextureFromFileA(device, path, &my_texture[path]);
		IM_ASSERT(res == 0);
		IM_ASSERT(my_texture[path] != NULL);
	}
	// Retrieve texture info
	D3DSURFACE_DESC my_texture_desc;
	HRESULT res = my_texture[path]->GetLevelDesc(0, &my_texture_desc);
	IM_ASSERT(res == 0);

	ImVec2 uv_min(0.0f, 0.0f);
	ImVec2 uv_max(1.0f, 1.0f);

	BeginInvisible(id ? id : path, pos, focus);
	ImGui::Image(my_texture[path], ImVec2((float)my_texture_desc.Width * widthMult, (float)my_texture_desc.Height * heightMult), uv_min, uv_max);
	EndInvisible();
}

void ImguiUtils::DrawText_(std::string text, ImVec2 pos, bool withColors, string uniqueId) {

	const string wid = "###" + uniqueId + "___" + text;
	BeginInvisible(wid.data(), pos, false, 1);

	if (!withColors) ImGui::Text("%s", text.data());
	else TextWithColors("%s", text.data());

	EndInvisible();
}