#pragma once
#include "services/World.h"
#include <skymp2-cellhost/src/Cellhost.h>

struct AuthData;

class Net : public World::INet
{
public:
	Net(const string &serverAddress);
	~Net();

	void FlushToWorld(World *optionalWorld) override;

	void Task(std::function<void()> f) override;

	void Handshake(const AuthData &auth, const string &version, bool crashed = false) override;
	std::unique_ptr<AuthData> GetHandshakeResult() override;

	void Confirm(string name, string confirmCode) override;
	int GetConfirmResult() override; // -1 (no result), 0 (system_error) or HTTP state code

	void RetryConfirm(string name) override;
	int GetRetryConfirmResult() override;

	void StartResetPassword(string email) override;
	int GetStartResetPasswordResult() override;

	void FinishResetPassword(string code, string password) override;
	int GetFinishResetPasswordResult() override;

	void AddMarker(formid id) override;
	void RequestData(vector<formid> ids, bool hostable) override;
	void Activate(formid caster, formid target, int baseType, bool open) override;
	void UpdateMovement(formid id, const Movement &mov, float uiHealth, float uiMagicka, float uiStamina, formid cellOrWorld, formid cell) override;
	void UpdateEquipment(const Equipment &eq) override;
	void UpdateLook(const Look &look) override;
	void Broadcast(formid ownerId, uint8_t contentType, const string &content, int numDgrams = 1) override;
	void InventoryEvent(const InvEvent &ie) override;
	void SendDialogResponse(const string &buttonId, const json &dialogData) override;
	void SendChatMessage(const string &messageJsonDump) override;
	void SendInteractionClick(const string &inteactionClickJsonDump) override;
	void LoadMyInventory() override;
	void SendScriptEvent(const string &eventName, const string &content, std::function<void(int, string)> callback) override;

	bool IsConnected() const override;
	bool IsRequestDataInProgress(bool hostable) const override;

	int GetPing() const override;
	int GetLag() const override;
	int GetOutputPacketRate() const override;

private:
	void Spawn(json mpch);
	void RunCallbacks();
	bool RunUDP();
	void RunParty();
	void RunImmediateCallbacks();

	struct Impl;
	std::unique_ptr<Impl> pImpl;
};