#pragma once
#include "SmartRef.h"
#include "Look.h"

class SmartActor :
	public SmartRef,
	public IListener<Look>
{
public:
	SmartActor(NiPoint3 spawnPoint, float drawDistance, WorldCleaner &wc, std::shared_ptr<IMovementInterpolator> movementInterpolator);
	~SmartActor();

	void Push(const Look &look) override;
	void PushName(const std::string &name);
	string GetName() const;

private:
	void OnUpdate() override;
	void OnDespawn() override;

	void WriteName(TESForm *outForm) const;

	struct Impl;
	std::unique_ptr<Impl> pImpl;
};