#include "../stdafx.h"
#include "MIPlayer.h"

#include "SmartRef.h"

#include <unordered_set>

std::recursive_mutex g_drawnM;
std::unordered_set<formid> g_drawn;

extern "C" {
	__declspec(dllexport) const char *SKYMP2_SetWeaponDrawn(const char *jDump) {
		std::lock_guard<std::recursive_mutex> l(g_drawnM); // also helps to sync access to stringHolder

		static string resultDraw = json{ { "draw", "0x1" } }.dump();
		static string resultSheathe = json{ { "draw", "0x0" } }.dump();

		json j = json::parse(jDump);

		string ecxS = j["ecx"];
		uint32_t ecxU;
		std::stringstream ss;
		ss << std::hex << ecxS;
		ss >> ecxU;
		auto actor = reinterpret_cast<Actor *>(ecxU);

		if (g_drawn.count(actor->formID))
			return resultDraw.data();
		else
			return resultSheathe.data();
	}
}

inline bool IsImmovable(TESObjectREFR *ref) {
	return ref->baseForm->formType == kFormType_Door 
		|| ref->baseForm->formType == kFormType_Container 
		|| ref->baseForm->formType == kFormType_Flora 
		|| ref->baseForm->formType == kFormType_Tree;
}

inline void HarvestOrOpen(TESObjectREFR *ref, bool open) {

	switch (ref->baseForm->formType) {
	case kFormType_Ammo:
	case kFormType_Armor:
	case kFormType_Book:
	case kFormType_Potion:
	case kFormType_Ingredient:
	case kFormType_Key:
	case kFormType_Misc:
	case kFormType_SoulGem:
	case kFormType_Weapon:
	case kFormType_Light:
		if (open) {
			sd::DisableNoWait(ref, false);
		}
		break;
	default:
		break;
	}
	if (ref->baseForm->formType == kFormType_Container && sd::Is3DLoaded(ref) && sd::HasLOS(*g_thePlayer, ref)) {
		sd::ExecuteConsoleCommand(open ? "aps AAAMPChest Open" : "aps AAAMPChest Close", ref);
		/*
		Scriptname AAAMPChest extends ObjectReference

		function Open()
		If PlayGamebryoAnimation("Open", abStartOver = True)
		Utility.Wait(0.5)
		EndIf
		endFunction

		function Close()
		If PlayGamebryoAnimation("Close", abStartOver = True)
		Utility.Wait(0.5)
		EndIf
		endFunction
		*/
	}
	if (open) {
		if (ref->baseForm->formType == kFormType_Tree || ref->baseForm->formType == kFormType_Flora) {
			auto chicken = sd::PlaceAtMe(ref, LookupFormByID(ID_TESNPC::EncChicken), 1, false, false);
			if (chicken) {
				sd::Activate(ref, chicken, 1);
				sd::Delete(chicken);
			}
		}
	}
}

auto degreeMin = [](TESForm *form)->float {
	const auto t = form->formType;
	// For base forms only
	assert(t != kFormType_Reference);
	assert(t != kFormType_Character);
	switch (t) {
	case kFormType_Flora:
	case kFormType_Tree:
		return 5.f;
	default:
		return 1.f;
	}
};

enum Motion {
	Motion_Dynamic = 1,//: The object will be simulated by havok(fall, etc...).It appears that the engine may automatically set this object to the SphereInertia or BoxInertia motion type.
	Motion_SphereInertia = 2,// : "Softened" movement simulation performed using a sphere inertia tensor.
	Motion_BoxInertia = 3,// : "Softened" movement simulation performed using a box inertia tensor.
	Motion_Keyframed = 4,// : The object will NOT be simulated by Havok(will only move if forced to, through SetPosition() or TranslateTo())
	Motion_Fixed = 5,// : This motion type is used primarily for the static elements of a game scene, e.g.the landscape.
	Motion_ThinBoxInertia = 6,// : A box inertia motion optimized for thin boxes to have less stability problems.
	Motion_Character = 7,// : A specialized motion used for character controllers.
};

struct MIPlayer::State
{
	clock_t lastSneakRepair = 0;
	clock_t lastBlockRepair = 0;
	clock_t lastDrawnRepair = 0;
};

struct MIPlayer::Impl
{
	std::optional<Movement> movement;

	std::vector<std::function<void(TESObjectREFR *)>> tasks;

	clock_t lastPush = 0;
	clock_t setMotionTypeMoment = 0;
	bool lastStopLooking = false;

	State state; // To modify inside tasks only
};

MIPlayer::MIPlayer() {
	pImpl.reset(new Impl);
}

MIPlayer::~MIPlayer() {
}

void MIPlayer::Update(TESObjectREFR *ref) {

	if (pImpl->movement.has_value() == false) throw with_trace(std::logic_error("you have to push movement before update"));

	for (auto task : pImpl->tasks) task(ref);
	pImpl->tasks = {};
}

void MIPlayer::PushMovement(const Movement &mov) {
	if (pImpl->movement.has_value()) {
		this->OnMovementChange(pImpl->state, *pImpl->movement, mov);
	}
	else {
		pImpl->tasks.push_back([=] (TESObjectREFR *ref){
			if(!IsImmovable(ref)) sd::SetPosition(ref, mov.Pos());
			if (ref->formType == kFormType_Character) {
			}
			else {
				pImpl->setMotionTypeMoment = clock() + 500;
				if (mov.IsOpen()) {
					sd::SetOpen(ref, 1);
					HarvestOrOpen(ref, mov.IsOpen());
				}
			}
		});
	}
	pImpl->movement = mov;
	pImpl->lastPush = clock();
}

void MIPlayer::OnMovementChange(State &state, const Movement &was, const Movement &now) {
	assert(pImpl->lastPush != 0);

	auto ms = clock() - pImpl->lastPush;
	auto seconds = ms / 1000.f;

	auto speed = max((was.Pos() - now.Pos()).Length() / seconds, 75.0) * 0.975;
	if (now.IsSneaking() && now.GetRunMode() == Movement::RunMode::Walking)
		speed = 50;

	pImpl->tasks.push_back([=, &state](TESObjectREFR *ref) {
		if (was == now && ref->formType == kFormType_Reference) return;

		auto ac = ref->formType == kFormType_Character ? (Actor *)ref : nullptr;
		if (ac) {
			sd::SetHeadTracking(ac, false);
			sd::ClearLookAt(ac);
			sd::StopCombat(ac);

			// Dead
			if (now.IsDead()) {
				if (sd::IsDead(ac) == false) {
					sd::EndDeferredKill(ac);
					sd::Kill(ac, nullptr);
				}
				return;
			}
			else {
				// Health
				constexpr float hpFull = 1'000'000.f;
				constexpr float epsilon = 0.02f;
				sd::SetActorValue(ac, "Health", hpFull);
				const auto currentPercentage = sd::GetActorValuePercentage(ac, "Health");
				const auto destPercentage = max(now._hpPercent * 0.01f, 0.02f);
				const auto diffPercentage = currentPercentage - destPercentage;
				if (diffPercentage > epsilon) {
					sd::DamageActorValue(ac, "Health", diffPercentage * hpFull);
				}
				else if (diffPercentage < -epsilon) {
					sd::RestoreActorValue(ac, "Health", diffPercentage * hpFull);
				}
			}
			// To respawn dead actors
			if (!now.IsDead() && was.IsDead()) {
				sd::Disable(ac, false);
			}
			sd::StartDeferredKill(ac);
		}

		if (!ac) {
			if (pImpl->setMotionTypeMoment && clock() > pImpl->setMotionTypeMoment) {
				sd::SetMotionType(ref, Motion_Keyframed, true);
				pImpl->setMotionTypeMoment = 0;
			}

			// Open
			bool ignore = ref->baseForm->formType == kFormType_Container && papyrusGame::GetCurrentCrosshairRef(0) == ref;
			if (!ignore) {
				if (now.IsOpen() != was.IsOpen()) {
					sd::SetOpen(ref, now.IsOpen());
					HarvestOrOpen(ref, now.IsOpen());
				}
			}
		}

		// ���, ������, ���� ��������
		const bool stay = (sd::GetPosition(ref) - now._pos).Length() <= 1.0
			&& abs(sd::GetAngleX(ref) - now.Rotation().x) <= degreeMin(ref->baseForm)
			&& abs(sd::GetAngleY(ref) - now.Rotation().y) <= degreeMin(ref->baseForm)
			&& abs(sd::GetAngleZ(ref) - now.Rotation().z) <= degreeMin(ref->baseForm);
		if(!stay)
		{
			static std::map<Movement::RunMode, float> unitsOffset = {
				{ Movement::RunMode::Standing, 0.f },
				{ Movement::RunMode::Walking, 3.f },
				{ Movement::RunMode::Running, 3.f },
				{ Movement::RunMode::Sprinting, 3.f } // ��� ������ ��������, ��� ������� �� ����� ��������, ��� ��� 3 ����� ������
			};
			static std::map<Movement::RunMode, float> offsetZ = {
				{ Movement::RunMode::Standing, 0.f }, // ����� �� ���� ��������� �� ����, �� �� ����� ������ �� �����...
				{ Movement::RunMode::Walking, -512.f }, // �� ����� �������, ��� ��� ��������, �� ����� ����� �� Z ���������� NPC ��������� �������� ��� �����
				{ Movement::RunMode::Running, -1024.f },
				{ Movement::RunMode::Sprinting, 0.f } // �������� 0 ���������� ��� ���������� �������� ������� � ������� � �����
			};
			const NiPoint3 offset{
				unitsOffset[now.GetRunMode()] * std::sin(now.Direction() / 180.f * std::acosf(-1)),
				unitsOffset[now.GetRunMode()] * std::cos(now.Direction() / 180.f * std::acosf(-1)),
				offsetZ[now.GetRunMode()]
			};


			float offsetAngleZ = 0;
			if (now.GetRunMode() == Movement::RunMode::Standing) {
				offsetAngleZ = now.Rotation().z - sd::GetAngleZ(ref);
				if (abs(offsetAngleZ) < 15) offsetAngleZ = 0;
			}
			
			if (ac) sd::KeepOffsetFromActor(ac, ac, offset.x, offset.y, offset.z, 0, 0, offsetAngleZ, now.GetRunMode() == Movement::RunMode::Walking ? 2048 : 1, 1);
			
			const float angleAbs = abs(now.Rotation().z - was.Rotation().z);
			const bool isLooking = angleAbs >= 5.0f;
			const bool stopLooking = ac && !isLooking && now.GetRunMode() == Movement::RunMode::Standing;
			const bool forceStopLooking = stopLooking && !pImpl->lastStopLooking;
			pImpl->lastStopLooking = stopLooking;
			if (ac && forceStopLooking) {
				sd::ClearKeepOffsetFromActor(ac);
				sd::KeepOffsetFromActor(ac, ac, 0, 0, 0, 0, 0, 0, 1, 1);
			}

			auto p = now.Pos();
			auto realPos = sd::GetPosition(ref);
			auto distance = (realPos - p).Length();
			auto distanceNoZ = (NiPoint3(realPos.x, realPos.y, 0) -= {p.x, p.y, 0}).Length();
			auto rotSpeed = abs((was.Rotation() - now.Rotation()).Length() / seconds);
			if (ac && rotSpeed < 10) rotSpeed = 0;
			if (forceStopLooking) rotSpeed = 100;

			const bool shouldTranslateActor = forceStopLooking || now.GetRunMode() != Movement::RunMode::Standing || distanceNoZ > 8.0 || distance > 64.0 || now.IsInJumpState();
			const bool shouldTranslateRef = true;
			if (!IsImmovable(ref)) {
				if ((shouldTranslateActor && ac) || (shouldTranslateRef && !ac)) {
					auto r = now.Rotation();
					sd::TranslateTo(ref, p.x, p.y, p.z,
						r.x, r.y, r.z, (distance <= max(ms * 2, 128)) ? speed : 200'000, rotSpeed);
				}
			}
		}


		if (ac) {
			// ������
			if (now.GetRunMode() == Movement::RunMode::Sprinting) { // ������ ���������, ����� �� ����������
				static auto fsSprintStart = new BSFixedString("SprintStart");
				ref->animGraphHolder.SendAnimationEvent(fsSprintStart);
			}
			if (now.GetRunMode() == Movement::RunMode::Standing || (was.GetRunMode() == Movement::RunMode::Sprinting && now.GetRunMode() != Movement::RunMode::Sprinting)) {
				static auto fsSprintStop = new BSFixedString("SprintStop");
				ref->animGraphHolder.SendAnimationEvent(fsSprintStop);
			}

			// Sneaking
			static auto fsSneakStart = new BSFixedString("SneakStart");
			static auto fsSneakStop = new BSFixedString("SneakStop");
			if (clock() - pImpl->state.lastSneakRepair > 2000) {
				pImpl->state.lastSneakRepair = clock();
				ref->animGraphHolder.SendAnimationEvent(now.IsSneaking() ? fsSneakStart : fsSneakStop);
			}

			// Blocking
			static auto fsBlockStart = new BSFixedString("BlockStart");
			static auto fsBlockStop = new BSFixedString("BlockStop");
			if (clock() - pImpl->state.lastBlockRepair > 1000) {
				pImpl->state.lastBlockRepair = clock();
				ref->animGraphHolder.SendAnimationEvent(now.IsBlocking() ? fsBlockStart : fsBlockStop);
			}

			// Combat
			if (clock() - pImpl->state.lastDrawnRepair > 100) {
				pImpl->state.lastDrawnRepair = clock();
				if (!now.IsInJumpState()) {
					const bool isDrawnReal = ac && sd::IsWeaponDrawn(ac);
					if (now.IsWeapDrawn() && !isDrawnReal) {
						{
							std::lock_guard<std::recursive_mutex> l(g_drawnM);
							g_drawn.insert(ac->formID);
						}
						ac->DrawSheatheWeapon(1);
					}
					if (!now.IsWeapDrawn() && isDrawnReal) {
						{
							std::lock_guard<std::recursive_mutex> l(g_drawnM);
							g_drawn.erase(ac->formID);
						}
						ac->DrawSheatheWeapon(0);
					}
				}
			}
		}
	});
}