#include "../stdafx.h"
#include "Movement.h"

Movement Movement_::GetFrom(TESObjectREFR *ref) {

	if (ref->formType != kFormType_Character) {
		auto mov = Movement();
		mov._pos = ref->pos;
		mov.SetRot({ sd::GetAngleX(ref), sd::GetAngleY(ref), sd::GetAngleZ(ref) });
		if(ref == sd::GetPlayerGrabbedRef() || papyrusGame::GetCurrentCrosshairRef(0) == ref) mov._flags |= Movement::Grabbed;
		int openState = sd::GetOpenState(ref);
		if (openState == 1 || openState == 2) mov._flags |= Movement::Open;
		if (papyrusObjectReference::IsHarvested(ref))mov._flags |= Movement::Open;
		return mov;
	}

	Actor *actor = (Actor *)ref;

	static auto fsDirection = (new BSFixedString("Direction"));
	static auto fsSpeedSampled = (new BSFixedString("SpeedSampled"));
	static auto fsBInJumpState = (new BSFixedString("bInJumpState"));

	auto mov = Movement();
	mov._pos = sd::GetPosition(actor);
	mov._angleZ = uint8_t(sd::GetAngleZ(actor) / 1.4);

	float animDirection = 0.0;
	actor->animGraphHolder.GetVariableFloat(fsDirection, &animDirection);
	mov._direction = uint16_t(animDirection * 360 / 0.00549324788);

	float speedSampled = 0;
	actor->animGraphHolder.GetVariableFloat(fsSpeedSampled, &speedSampled);

	if (sd::IsSneaking(actor)) mov._flags |= Movement::Sneaking;
	if (sd::IsWeaponDrawn(actor)) mov._flags |= Movement::WeapDrawn;
	if (sd::Obscript::IsBlocking(actor) > 0.0) mov._flags |= Movement::Blocking;
	if (sd::IsDead(actor) || sd::GetActorValuePercentage(actor, "Health") <= 0.0f) mov._flags |= Movement::Dead;


	if (speedSampled != 0) {
		if (sd::IsSprinting(actor))
			mov._flags |= (Movement::Walking | Movement::Running);
		else if (!mov.IsBlocking() && (actor == *g_thePlayer ? !!PlayerControls::GetSingleton()->runMode : sd::IsRunning(actor)))
			mov._flags |= Movement::Running;
		else
			mov._flags |= Movement::Walking;

		if (mov.GetRunMode() == Movement::RunMode::Walking
			&& mov.IsBlocking() && mov.IsSneaking()) {
			mov._flags &= ~Movement::Walking; // ������� Walking
			mov._flags |= Movement::Running; // ������ Running 
		}
	}

	uint8_t isInJumpState;
	actor->animGraphHolder.GetVariableBool(fsBInJumpState, &isInJumpState);
	if (isInJumpState) {
		// see comments in header
		mov._flags &= ~(Movement::WeapDrawn);
		mov._flags |= Movement::Blocking;
	}

	mov._hpPercent = uint8_t(sd::GetActorValuePercentage(actor, "Health") * 100);

	return mov;
}