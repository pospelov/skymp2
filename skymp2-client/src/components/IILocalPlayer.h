#pragma once
#include "Inventory.h"

class IILocalPlayer : public IInventoryInterpolator
{
public:
	enum {
		CleanFirst = 1
	};

	IILocalPlayer(uint32_t flags = 0);
	~IILocalPlayer() override;
	void Update(TESObjectREFR *ref) override;
	void PushInventory(const Inventory &inv) override;

	static Inventory GetFrom(TESObjectREFR *refr);

	InvEvent Next();

private:
	struct Impl;
	std::unique_ptr<int> _; // prevent copy
	std::shared_ptr<Impl> pImpl;
};