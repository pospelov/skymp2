#include "../stdafx.h"
#include "Look.h"

#include "../sdutil/TintUtil.h"
#include "../sdutil/NewFormID.h"
#include "../sdutil/Esp.h"

std::vector<Look::Tint> g_lastTints;
volatile bool g_badTintsInPC = false; // TODO: atomic<bool>

Look Look_::GetFromPlayer() {
	auto ac = *g_thePlayer;
	auto npc = (TESNPC *)ac->baseForm;
	Look res;
	res.isFemale = sd::GetSex(npc) == 1;
	res.raceID = ac->race->formID;
	res.weight = UInt8(npc->weight) / 10;
	memcpy(res.skinRGB.data(), &npc->color, 3);
	if (npc->headData && npc->headData->hairColor)
	{
		auto hairColor = npc->headData->hairColor->color;
		memcpy(res.hairRGB.data(), &hairColor, 3);
	}
	else {
		std::array<uint8_t, 3> defaultCol = { 57, 55, 40 };
		memcpy(res.hairRGB.data(), defaultCol.data(), 3);
	}
	if (npc->headData && npc->headData->headTexture)
		res.headTextureSet = npc->headData->headTexture->formID;
	if (npc->headparts)
	{
		for (auto it = npc->headparts; it != npc->headparts + npc->numHeadParts; ++it)
			res.headparts.push_back((*it)->formID);
	}
	if (npc->faceMorph)
	{
		std::copy(npc->faceMorph->option, npc->faceMorph->option + npc->faceMorph->kNumOptions,
			res.options.begin());
		std::copy(npc->faceMorph->presets, npc->faceMorph->presets + npc->faceMorph->kNumPresets,
			res.presets.begin());
	}

	// Tints:
	if (g_badTintsInPC)
		res.tints = g_lastTints;
	else {
		for (size_t i = 0; i < (*g_thePlayer)->tintMasks.count; ++i)
		{
			auto &it = (*g_thePlayer)->tintMasks[i];
			Look::Tint m;
			std::string texture_path = (it)->texture->str.data;
			std::transform(texture_path.begin(), texture_path.end(), texture_path.begin(), ::tolower);
			m.alpha = (it)->alpha;
			memcpy(&m.rgba, &(it)->color, 4);
			m.texture = sdutil::GetTintMaskTextureID(texture_path.data());
			m.type = (it)->tintType;
			res.tints.push_back(m);
		}
		g_lastTints = res.tints;
	}

	static bool helperThreadStarted = false;
	if (!helperThreadStarted) {
		helperThreadStarted = true;
		std::thread([] {
			while (1) {
				Sleep(1);
				static auto fsRaceSexMenu = new BSFixedString("RaceSex Menu");
				static auto fsConsole = new BSFixedString("Console");
				if (MenuManager::GetSingleton()->IsMenuOpen(fsRaceSexMenu)) {
					g_badTintsInPC = false;

					const auto allow = InputManager::GetSingleton()->allowTextInput;
					if (allow != 0) {
						if (MenuManager::GetSingleton()->IsMenuOpen(fsConsole) == false) {
							keybd_event(VK_LSHIFT, 0x11, NULL, NULL);
							keybd_event(VK_LSHIFT, 0x11, KEYEVENTF_KEYUP, NULL);
							keybd_event(VK_LSHIFT, 0x1c, NULL, NULL);
							keybd_event(VK_LSHIFT, 0x1c, KEYEVENTF_KEYUP, NULL);
						}
					}
				}
			}
		}).detach();
	}

	return res;
}

TESNPC *Look_::Apply(const Look &lo, TESNPC *_npc) {
	// TODO: anticheat

	auto npc = _npc;
	if (!npc) {
		npc = (TESNPC *)malloc(sizeof TESNPC);
		if (!npc) return _npc; // if malloc fails

		static const formid initid = 0x00000D62 + sdutil::GetEspOffset();
		static formid id = initid;
		auto skympEspNpc = LookupFormByID(id);
		++id;
		if (id == initid + 99) id = initid;
		assert(skympEspNpc);
		memcpy(npc, skympEspNpc, sizeof TESNPC);
		// TODO: �����������, ����� �� ������������� 
		//npc->formID = 0;
		//npc->SetFormID(sdutil::NewFormID(), 1);
	}
	assert(npc);
	npc->actorData.flags_.female = !!lo.isFemale;
	memcpy(&npc->color, lo.skinRGB.data(), 3);

	/* Anticheat */
	{
		if (!LookupFormByID(lo.raceID)) return npc;
	}

	// Race
	auto race = LookupFormByID(lo.raceID);
	//if (!race) throw with_trace(std::logic_error("race == nullptr"));
	assert(race);
	assert(race->formType == kFormType_Race);
	npc->race.race = (TESRace *)race;

	// Weight
	npc->weight = lo.weight * 10.f;

	// Headparts
	npc->numHeadParts = (uint8_t)lo.headparts.size();
	if (lo.headparts.size() > 0) {
		npc->headparts = new BGSHeadPart*[lo.headparts.size()];
	}
	else {
		npc->headparts = nullptr;
	}
	for (size_t i = 0; i != lo.headparts.size(); i++) {
		npc->headparts[i] = (BGSHeadPart *)LookupFormByID(lo.headparts[i]);
		assert(npc->headparts[i]);
		assert(npc->headparts[i]->formType == kFormType_HeadPart);
	}

	// Head data
	if (!npc->headData) npc->headData = (TESNPC::HeadData *)FormHeap_Allocate(sizeof TESNPC::HeadData);
	npc->headData->hairColor = nullptr;
	npc->headData->headTexture = nullptr;
	auto resultTextureSet = LookupFormByID(lo.headTextureSet);
	if (resultTextureSet) {
		assert(resultTextureSet->formType == kFormType_TextureSet);
		npc->headData->headTexture = (BGSTextureSet *)resultTextureSet;
	}
	auto factory = ((IFormFactory **)0x012E57B0)[BGSColorForm::kTypeID];
	auto hair_color = (BGSColorForm *)factory->Create();
	memcpy(&hair_color->color, lo.hairRGB.data(), 3);
	hair_color->color.alpha = 255;
	npc->headData->headTexture = nullptr;
	npc->headData->hairColor = hair_color;

	// Face morph
	npc->faceMorph = new TESNPC::FaceMorphs;
	std::copy(lo.presets.begin(), lo.presets.end(), npc->faceMorph->presets);
	std::copy(lo.options.begin(), lo.options.end(), npc->faceMorph->option);

	if (npc != sd::GetPlayer()->baseForm) {
		npc->actorData.voiceType = (BGSVoiceType *)LookupFormByID(0x0002F7C3);
	}

	// Clear AI Packages to prevent idle animations with Furniture
	auto doNothing = (TESPackage *)LookupFormByID(ID_TESPackage::DoNothing);
	auto flagsSource = (TESPackage *)LookupFormByID(ID_TESPackage::DefaultMoveToCustom02IgnoreCombat); // ignore combat && no combat alert
	doNothing->packageFlags = flagsSource->packageFlags;
	npc->aiForm.unk10.unk0 = (uint32_t)doNothing;
	npc->aiForm.unk10.next = nullptr;

	return npc;
}

void Look_::ApplyTints(const Look &lo, Actor *ac) {
	ac->baseForm->formID = (*g_thePlayer)->baseForm->formID;

	(*g_thePlayer)->tintMasks.Allocate(lo.tints.size());

	for (size_t i = 0; i < lo.tints.size(); ++i) {
		auto _new = (TintMask *)FormHeap_Allocate(sizeof TintMask);
		auto &tint = lo.tints[i];
		memcpy(&_new->color, &tint.rgba, 4);
		_new->color.alpha = 0;
		_new->alpha = tint.alpha;
		_new->tintType = tint.type;

		auto texture = (TESTexture *)FormHeap_Allocate(sizeof TESTexture);
		texture->str = sdutil::GetTintMaskTexturePath(tint.texture);
		_new->texture = texture;
		(*g_thePlayer)->tintMasks[i] = _new;
	}
	g_badTintsInPC = true;

	bool updateWeight = ac == *g_thePlayer;
	CALL_MEMBER_FN(ac, QueueNiNodeUpdate)(updateWeight);
}

void Look_::ApplyToPlayer(const Look &look) { // TODO: fix crash after death 

	auto npc = (TESNPC *)(*g_thePlayer)->baseForm;
	sd::Wait(500);
	Look_::Apply(look, npc);
	Look_::ApplyTints(look, (*g_thePlayer));
	sd::Wait(500);
}