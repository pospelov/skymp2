#include "../stdafx.h"
#include "Equipment.h"

enum {
	TORCH = 0x1d4ec
};

inline bool TorchEquipped(Actor *ac) {
	return (sd::GetEquippedItemType(ac, 0) == 11);// left is 0 in this function
}

volatile bool g_pcUpdateEquipment = true;
std::unique_ptr<Equipment> g_pcCacheEquipment = nullptr;

extern "C" {
	__declspec(dllexport) void SKYMP2_OnEquipItem(Actor *actor, TESForm *item, bool equip) {
		formid id = actor ? actor->formID : 0;
		if (id == 0x14) {
			g_pcUpdateEquipment = true;
		}
	}
}

Equipment Equipment_::GetFrom(Actor *ac) {

	Equipment eq;
	if (ac->formID != 0x14 || g_pcUpdateEquipment || !g_pcCacheEquipment) {
		for (int i = 0; i < papyrusObjectReference::GetNumItems(ac); ++i) {
			auto form = papyrusObjectReference::GetNthForm(ac, i);
			if (form && sd::IsEquipped(ac, form)) {
				switch (form->formType) {
				case kFormType_Armor:
				case kFormType_Ammo:
				case kFormType_Light:
					eq.armor.push_back(form->formID);
					break;
				case kFormType_Weapon: // TODO: both hands
					eq.hands[0] = form->formID;
					break;
				}
			}
		}
		if (ac->formID == 0x14) {
			g_pcUpdateEquipment = false;
			g_pcCacheEquipment.reset(new Equipment(eq));
		}
	}
	else
		eq = *g_pcCacheEquipment;


	if (TorchEquipped(ac)) {
		eq.hands[1] = TORCH;
	}
	for (int i = 0; i != 2; i++) {
		auto spell = sd::GetEquippedSpell(ac, (int)!i);
		if (spell) eq.hands[i] = spell->formID;
	}
	return eq;
}

void Equipment_::ApplyTo(const Equipment &eq, Actor *ac) {
	auto eqWas = Equipment_::GetFrom(ac);

	// Hands
	for (int i = 0; i < 1; ++i) { // TODO: both hands
		auto form = LookupFormByID(eq.hands[i]);
		auto formWas = (TESForm *)sd::GetEquippedWeapon(ac, (bool)i);
		if (!formWas) formWas = sd::GetEquippedSpell(ac, !i);
		if (formWas != form) {
			/*if (formWas) {
			sd::UnequipItem(ac, formWas, false, true);
			sd::RemoveItem(ac, formWas, -1, true, nullptr);
			sd::PrintNote("unequip %p %p", formWas, form);
			}*/
			if (!form) form = LookupFormByID(0x1f4); // unarmed
			if (form) {
				if (form->formType != FormType::kFormType_Spell) {
					sd::EquipItem(ac, form, true, true);
				}
				else {
					sd::EquipSpell(ac, (SpellItem *)form, !i);
				}
			}
		}
	}

	// Torch
	if (eq.hands[1] == TORCH) {
		sd::EquipItem(ac, LookupFormByID(TORCH), 1, 1);
	}
	else if (TorchEquipped(ac)) {
		sd::UnequipItem(ac, LookupFormByID(TORCH), false, true);
	}

	// Armor
	std::set<formid> toAdd, toRemove;

	for (auto element : eq.armor) {
		if (std::find(eqWas.armor.begin(), eqWas.armor.end(), element) == eqWas.armor.end()) toAdd.insert(element);
	}
	for (auto element : eqWas.armor) {
		if (std::find(eq.armor.begin(), eq.armor.end(), element) == eq.armor.end()) toRemove.insert(element);
	}

	for (auto formid : toRemove) {
		auto form = LookupFormByID(formid);
		if (form && (form->formType == kFormType_Armor || form->formType == kFormType_Ammo || form->formType == kFormType_Light)) {
			sd::UnequipItem(ac, form, false, true);
			sd::RemoveItem(ac, form, -1, true, nullptr);
		}
	}
	for (auto formid : toAdd) {
		auto form = LookupFormByID(formid);
		if (form && (form->formType == kFormType_Armor || form->formType == kFormType_Ammo || form->formType == kFormType_Light)) {
			if (form->formType == kFormType_Ammo) sd::AddItem(ac, form, 1000, true);
			sd::EquipItem(ac, form, true, true);
		}
	}
}

bool Equipment_::ApplyToPlayer(const Equipment &eq) {
	std::set<TESForm *> toEq;
	for (auto fid : eq.armor) toEq.insert(LookupFormByID(fid));
	for (int i = 0; i != 2; ++i)toEq.insert(LookupFormByID(eq.hands[i]));
	toEq.erase(nullptr);
	for (auto form : toEq) {
		sd::EquipItem(sd::GetPlayer(), form, false, true);
	}
	return true;
}