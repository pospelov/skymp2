#include "../stdafx.h"
#include "PersistentRef.h"

#include "../sdutil/HimikaActivateEvent.h"
#include "../TaskQueue.h"

#include "MIPlayer.h"
#include "IILocalPlayer.h"

using ActiEvents = std::list<ActiEvent>;

struct PersistentRef::Impl
{
	std::unique_ptr<MIPlayer> mi;
	std::unique_ptr<IILocalPlayer> ii;
	Movement mov;
};

ActiEvent::Callback g_onActi = nullptr;
TaskQueue g_eventTaskQueue;

PersistentRef::PersistentRef(formid id_) : id(id_) {
	pImpl.reset(new Impl);
	this->SetMode(EntityMode::In);
	static bool sinkAttached = false;
	if (!sinkAttached) {
		actiEventSource->AddEventSink(new ActivateEventSink([](TESActivateEvent &evn) {
			const auto sae = ActiEvent{ evn.caster ? evn.caster->formID : 0, evn.target ? evn.target->formID : 0 };
			g_eventTaskQueue.Add([sae] {
				if (g_onActi) g_onActi(sae);

				const std::set<uint32_t> g_itemTypes = {
					kFormType_Ammo,
					kFormType_Armor,
					kFormType_Book,
					kFormType_Ingredient,
					kFormType_Key,
					kFormType_Misc,
					kFormType_Potion,
					kFormType_SoulGem,
					kFormType_Weapon,
				};
				auto ref = (TESObjectREFR *)LookupFormByID(sae.target);
				if (ref->formType != kFormType_Reference) ref = nullptr;
				if (ref && g_itemTypes.count(ref->baseForm->formType)) {
					// Predict item  pick up
					//sd::Disable(ref, false);
				}
				if (ref && (ref->baseForm->formType == kFormType_Flora || ref->baseForm->formType == kFormType_Tree)) {
					// TODO: Predict flora picked up?
				}
			});
		}));
		sinkAttached = true;
	}
}

void PersistentRef::Update() {
	g_eventTaskQueue.Update();

	auto refr = (TESObjectREFR *)LookupFormByID(this->id);

	if (pImpl->ii) {
		pImpl->ii->Update(refr);
		if (sd::HasLOS(sd::GetPlayer(), refr)) pImpl->ii->Next(); // see description. TODO: Ensure it works and HasLOS is safe
	}

	if (pImpl->mov._pos.Length() > 0) {
		auto refr = (TESObjectREFR *)LookupFormByID(this->id);
		if (refr) {
			assert(refr->formType == kFormType_Reference || refr->formType == kFormType_Character);
			if (pImpl->mi) {
				pImpl->mi->Update(refr);
			}
		}
	}
}

void PersistentRef::SetOnActivate(ActiEvent::Callback cb) {
	g_onActi = cb;
}

void PersistentRef::Push(const Movement &mov) {
	if(pImpl->mi) pImpl->mi->PushMovement(mov);
	pImpl->mov = mov;
}

void PersistentRef::Push(const Inventory &inv) {
	if (!pImpl->ii) pImpl->ii.reset(new IILocalPlayer(IILocalPlayer::CleanFirst));
	assert(pImpl->ii);
	pImpl->ii->PushInventory(inv);
}

formid PersistentRef::GetId() const {
	return this->id;
}

Movement PersistentRef::GetMovement() const {
	auto refr = (TESObjectREFR *)LookupFormByID(this->id);
	if (refr) {
		if (refr->formType == kFormType_Reference || refr->formType == kFormType_Character) {
			return Movement_::GetFrom(refr);
		}
	}
	throw std::logic_error("PersistentRef::GetMovement() unable to find REFR");
}

void PersistentRef::SetMode(EntityMode m) {
	switch (m) {
	case EntityMode::In:
		if(pImpl->mi == nullptr) pImpl->mi.reset(new MIPlayer);
		break;
	case EntityMode::Out:
		pImpl->mi.reset();
		break;
	default:
		assert(0);
		break;
	}
}

PersistentRef::~PersistentRef() {
}

