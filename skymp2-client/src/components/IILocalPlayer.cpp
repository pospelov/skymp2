#include "../stdafx.h"
#include "IILocalPlayer.h"
#include "Equipment.h"
#include "../dinput/DInput.hpp"

#include "../TaskQueue.h"


// Helpers to work with events
auto &g_disp = g_containerChangedEventDispatcher;
class ContainerChangedEventSink : public BSTEventSink<TESContainerChangedEvent>
{
public:
	using Callback = std::function<void(TESContainerChangedEvent &)>;

	ContainerChangedEventSink(Callback callback) : cb(callback) {}
	virtual ~ContainerChangedEventSink() override {}

	virtual	EventResult ReceiveEvent(TESContainerChangedEvent * evn, EventDispatcher<TESContainerChangedEvent> * dispatcher) {
		cb(*evn);
		return EventResult::kEvent_Continue;
	}

private:
	const Callback cb;
};

struct IILocalPlayer::Impl
{
	Inventory lastReadInv;

	std::vector<std::function<void(TESObjectREFR *)>> tasks;
	std::function<void(TESObjectREFR *)> task;
	volatile bool destroyed = false; // TODO: atomic
	bool sinkCreated = false;

	TaskQueue q;
	std::list<InvEvent> ies;
	int popedEvents = 0;
	bool nearWas = false;
};

Inventory GetFrom(TESObjectREFR *refr) {
	assert(refr);
	assert(refr->formType == kFormType_Character || refr->formType == kFormType_Reference);
	Inventory res;
	for (size_t i = 0; true; ++i) {
		auto form = papyrusObjectReference::GetNthForm(refr, i);
		if (!form) break;
		Inventory::Entry e;
		e.id = form->formID;
		e.count = sd::GetItemCount(refr, form);
		res.AddItem(e);
	}
	return res;
}

enum class Action { Add, Remove };

inline void ModInv(TESObjectREFR *refr, const Inventory &inv, Action action) {
	assert(refr);
	assert(refr->formType == kFormType_Character || refr->formType == kFormType_Reference);
	for (auto p : inv.simple) {
		auto item = LookupFormByID(p.first);
		if (!item) {
			Console_Print("Item not found %x", p.first);
			continue; // TODO: 

			std::stringstream ss;
			ss << "ILocalPlayer.cpp ModInv: form with id not found " << std::hex << p.first;
			throw std::logic_error(ss.str());
		}

		if (action == Action::Add) sd::AddItem(refr, item, p.second, true);
		else if (action == Action::Remove) {
			if (refr == sd::GetPlayer() && sd::IsEquipped(sd::GetPlayer(), item)) return;
			sd::RemoveItem(refr, item, p.second, true, nullptr);
		}
		else assert(false);
	}
}

IILocalPlayer::IILocalPlayer(uint32_t flags) {
	pImpl.reset(new Impl);
	pImpl->tasks.push_back([=](TESObjectREFR *refr) {
		if (flags & CleanFirst) {
			sd::RemoveAllItems(refr, nullptr, 0, 1);
		}
	});
}

IILocalPlayer::~IILocalPlayer() {
	pImpl->destroyed = true;
}

TaskQueue g_removeAllItems;
bool g_openWas = false;

void IILocalPlayer::Update(TESObjectREFR *refr) {
	// Global tasks
	g_removeAllItems.Update();

	if (!refr) return; // TODO: wtf?
	assert(refr);
	assert(refr->formType == kFormType_Character || refr->formType == kFormType_Reference);

	switch (refr->baseForm->formType) {
	case kFormType_NPC:
	case kFormType_Container:
		if (sd::GetPlayer() == refr || sd::HasLOS(sd::GetPlayer(), refr))
		{ // Is it ok to use HasLOS?
			if (pImpl->tasks.size()) {
				for (auto t : pImpl->tasks) t(refr);
				pImpl->tasks.clear();
			}
			if (pImpl->task) {
				pImpl->task(refr);
				pImpl->task = nullptr;
			}
		}
		break;
	default:
		break;
	}

	pImpl->q.Update();

	const auto formId = refr->formID;
	auto pImpl_ = pImpl;

	// ...

	static std::map<formid, ContainerChangedEventSink *> sink;
	if (pImpl->sinkCreated == false) {
		pImpl->sinkCreated = true;
		sink[formId] = new ContainerChangedEventSink([formId, pImpl_](TESContainerChangedEvent &evn) {
			if (pImpl_->destroyed) return;
			if (evn.fromFormId == formId) {
				if (evn.toFormId != 0) {
					InvEvent ie;
					ie.type = "put";
					ie.entry.count = evn.count;
					ie.entry.id = evn.itemFormId;
					ie.ref = evn.toFormId;
					pImpl_->q.Add([pImpl_, ie] { pImpl_->ies.push_back(ie); });
					g_removeAllItems.Add([ie] {
						auto refr = (TESObjectREFR *)LookupFormByID(ie.ref);
						if (refr) {
							if (refr->formType == kFormType_Reference) sd::RemoveAllItems(refr, nullptr, false, true);
						}
					});
				}
				else if (evn.toReference != 0) {
					InvEvent ie;
					ie.type = "drop";
					ie.entry.count = evn.count;
					ie.entry.id = evn.itemFormId;
					ie.ref = evn.toReference;
					pImpl_->q.Add([pImpl_, ie] { pImpl_->ies.push_back(ie); });
				}
				else {
					InvEvent ie;
					ie.type = "use";
					ie.entry.count = evn.count;
					assert(ie.entry.count);
					ie.entry.id = evn.itemFormId;
					ie.ref = 0;
					//pImpl_->q.Add([pImpl_, ie] { pImpl_->ies.push_back(ie); }); // TODO: check it's remove by player (eat) not by script
				}
			}
			else if (evn.toFormId == formId) {
				if (evn.fromFormId != 0) {
					auto refr = (TESObjectREFR *)LookupFormByID(evn.fromFormId);
					assert(refr);
					assert(refr->formType == kFormType_Reference || refr->formType == kFormType_Character);
					if (refr->baseForm->formType == kFormType_Container) {
						InvEvent ie;
						ie.type = "take";
						ie.entry.count = evn.count;
						ie.entry.id = evn.itemFormId;
						ie.ref = evn.fromFormId;
						pImpl_->q.Add([pImpl_, ie] { pImpl_->ies.push_back(ie); });
					}
				}
			}
		});
		g_disp->AddEventSink(sink[formId]);
	}
}

void IILocalPlayer::PushInventory(const Inventory &inv) {
	pImpl->task = ([=](TESObjectREFR *refr) {

		auto lastInv = GetFrom(refr);

		auto toAdd = Inventory(inv) - lastInv;
		auto toRemove = lastInv - inv;

		ModInv(refr, toRemove, Action::Remove);

		if (refr->baseForm->formType == kFormType_Container) {
			auto cont = (TESObjectCONT *)refr->baseForm;
			for (int i = 0; i < cont->container.numEntries; ++i) {
				auto e = cont->container.entries[i];
				sd::RemoveItem(refr, e->form, -1, 1, nullptr);
			}
		}
		ModInv(refr, toAdd, Action::Add);

	});
}

Inventory IILocalPlayer::GetFrom(TESObjectREFR *refr) {
	return ::GetFrom(refr);
}

InvEvent IILocalPlayer::Next() {
	InvEvent res;
	auto &ies = pImpl->ies;
	if (ies.size() > 0) {
		res = ies.front();
		ies.pop_front();
		pImpl->popedEvents++;
	}
	return res;
}