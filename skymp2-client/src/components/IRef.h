#pragma once
#include "Movement.h"
#include "Equipment.h"
#include "Look.h"
#include "Inventory.h"

class IUpdatable
{
public:
	virtual void Update() = 0;
};

template <class T>
class IListener
{
public:
	virtual void Push(const T &data) = 0;
};

enum class EntityMode {
	In, Out
};

class IRef : 
	public IUpdatable,
	public IListener<Movement>,
	public IListener<Inventory>
{
public:
	virtual void SetMode(EntityMode entityMode) = 0;

	virtual formid GetId() const = 0;
	virtual Movement GetMovement() const = 0;

	TESObjectREFR *FindForm() const {
		auto res = (TESObjectREFR *)LookupFormByID(GetId());
		if (res) assert(res->formType == kFormType_Reference || res->formType == kFormType_Character);
		return res;
	}

	TESForm *FindBaseForm() const {
		auto f = this->FindForm();
		return f ? f->baseForm : nullptr;
	}

	bool IsSpawned() const {
		return this->GetId() != 0;
	}
};