#pragma once
#include "../stdafx.h"
#include "Movement.h"

class SmartRef;

class MIPlayer : public IMovementInterpolator
{
public:
	MIPlayer();
	~MIPlayer() override;

	void Update(TESObjectREFR *ref) override;
	void PushMovement(const Movement &mov) override;

private:
	struct State;

	void OnMovementChange(State &state, const Movement &was, const Movement &now);

	struct Impl;
	std::shared_ptr<Impl> pImpl;
};