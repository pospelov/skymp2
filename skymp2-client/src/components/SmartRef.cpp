#include "../stdafx.h"
#include "SmartRef.h"
#include "../sdutil/FindRefs.h"
#include "MIPlayer.h"

enum {
	WAIT_AFTER_DESPAWN = 1000
};

struct SmartRef::Impl
{
	// My Data (Owning)
	formid id = 0;
	TESForm *base = nullptr;
	NiPoint3 pos;
	float drawDistance = 0;
	std::shared_ptr<IMovementInterpolator> mi;
	clock_t spawnMoment = 0;
	std::vector<Task> tasks, tasksEverySpawn;
	Equipment eq;
	Movement pushedMov;
	uint32_t count = 0;
	clock_t despawnMoment = 0;

	struct SpawnedData
	{
		clock_t lastEqUpd = 0;
	};
	std::unique_ptr<SpawnedData> spawnedData; // Must be reset on every spawn

	// Dependencies (Not Owning)
	WorldCleaner *wc = nullptr;
};

SmartRef::SmartRef(TESForm *base, NiPoint3 pos, float drawDistance, WorldCleaner &wc, std::shared_ptr<IMovementInterpolator> movementInterpolator) {
	pImpl.reset(new Impl);
	pImpl->wc = &wc;

	this->SetBaseForm(base);
	this->SetPos(pos);
	this->SetDrawDistance(drawDistance);
	this->SetMovementInterpolator(movementInterpolator);
}

SmartRef::~SmartRef() {
	if (this->IsSpawned()) this->Despawn();
}

void SmartRef::SetBaseForm(TESForm *base) {
	if (!base) throw with_trace(std::logic_error("base can't be nullptr"));
	pImpl->base = base;
}

void SmartRef::SetCount(uint32_t count) {
	pImpl->count = count;
}

void SmartRef::SetRef(formid id) {
	if(this->IsSpawned()) throw with_trace(std::logic_error("must not be spawned"));
	pImpl->id = id;
	pImpl->spawnedData.reset(new Impl::SpawnedData);
}

void SmartRef::SetDrawDistance(float d) {
	if (d <= 0) throw with_trace(std::logic_error("drawDistance must be greater than 0"));
	pImpl->drawDistance = d;
}

void SmartRef::SetMovementInterpolator(std::shared_ptr<IMovementInterpolator> mi) {
	if (!mi) throw with_trace(std::logic_error("movementInterpolator can't be nullptr"));
	pImpl->mi = mi;
}

void SmartRef::Push(const Movement &mov) {
	if(pImpl->mi) pImpl->mi->PushMovement(mov);
	this->SetPos(mov.Pos());
	pImpl->pushedMov = mov;
}

void SmartRef::Push(const Inventory &inv) {
	// TODO
}

void SmartRef::Push(const Equipment &eq) {
	if (eq != pImpl->eq) {
		pImpl->eq = eq;
		if (pImpl->spawnedData) pImpl->spawnedData->lastEqUpd = 0;
	}
}

void SmartRef::SetMode(EntityMode mode) {
	switch (mode) {
	case EntityMode::In:
		if (pImpl->mi == nullptr) pImpl->mi.reset(new MIPlayer);
		break;
	case EntityMode::Out:
		pImpl->mi.reset();
		break;
	default:
		assert(0);
		break;
	}
}

formid SmartRef::GetId() const {
	return pImpl->id;
}

Movement SmartRef::GetMovement() const {
	return pImpl->pushedMov;
}

void SmartRef::AddTask(Task task, uint32_t flags) {
	pImpl->tasks.push_back(task);
	if (flags & EverySpawn)
		pImpl->tasksEverySpawn.push_back(task);
}

void SmartRef::ForceDespawn() {
	try {
		this->Despawn();
	}
	catch (...) {

	}
}

formid SmartRef::SwapRef(formid formID) {
	formid res = 0;
	if (this->IsSpawned()) {
		res = pImpl->id;
		pImpl->id = 0;
	}
	this->Spawn(formID);
	return res;
}

void SmartRef::Update() {
	auto plPos = sd::GetPosition(sd::GetPlayer());
	auto distance = (plPos - pImpl->pos).Length();
	if (this->IsSpawned()) {
		auto ref = (TESObjectREFR *)LookupFormByID(pImpl->id);
		if (ref) distance = min(distance, (plPos - sd::GetPosition(ref)).Length());

		if (!ref) {
			this->Despawn("nullptr ref");
		}
		else if (ref->baseForm != pImpl->base) {
			this->Despawn("baseForm changed");
		}
		else if (distance >= pImpl->drawDistance) {
			this->Despawn("too much distance");
		}
		else if (sd::GetWorldSpace(ref) != sd::GetWorldSpace(*g_thePlayer)) {
			this->Despawn("different worlds");
		}
		else if (auto refCell = sd::GetParentCell(ref), pcCell = sd::GetParentCell(*g_thePlayer); refCell != pcCell && (pcCell && pcCell->IsInterior())) {
			this->Despawn("different interior cell");
		}
		else if ((clock() - pImpl->spawnMoment > 5000 && !sd::Is3DLoaded(ref))) {
			this->Despawn("3D isn't loaded"); // OK
		}
		else {
			if (pImpl->mi) pImpl->mi->Update(ref);

			assert(pImpl->spawnedData != nullptr);
			if (clock() - pImpl->spawnMoment > 333 
				&& clock() - pImpl->spawnedData->lastEqUpd > 500) { // as ApplyTo notes says, we should flood
				pImpl->spawnedData->lastEqUpd = clock();
				if (ref->formType == kFormType_Character) {
					Equipment_::ApplyTo(pImpl->eq, (Actor *)ref);
				}
			}
			
			for (auto task : pImpl->tasks) task(ref);
			pImpl->tasks.clear();

			this->OnUpdate();
		}
	}
	else {
		bool isHairlessCorpse = pImpl->base && pImpl->base->formID == 0x0010D13E;
		if (distance < pImpl->drawDistance && clock() - pImpl->despawnMoment > WAIT_AFTER_DESPAWN
			&& !isHairlessCorpse) {
			this->Spawn();
			auto ref = (TESObjectREFR *)LookupFormByID(pImpl->id);
			assert(ref);
			assert(ref->formType == kFormType_Character || ref->formType == kFormType_Reference);
			for (auto task : pImpl->tasksEverySpawn) task(ref);
		}
	}
}

void SmartRef::Spawn(formid refFromSetRef) {
	if (this->IsSpawned()) throw with_trace(std::logic_error("already spawned"));
	
	TESObjectREFR *refToPlaceAt = sd::GetPlayer();

	const bool isActor = pImpl->base->formType == kFormType_NPC;
	const auto pcCell = sd::GetParentCell(sd::GetPlayer());
	const bool pcInInterior = pcCell && pcCell->IsInterior();

	float mDistance = 1'000.f;
	auto refs = sdutil::FindRefs((isActor && pcInInterior) ? pcCell : nullptr);
	for (auto ref : refs) {
		float distance = (sd::GetPosition(ref) - (pImpl->pos)).Length();
		if (distance < mDistance) {
			mDistance = distance;
			refToPlaceAt = ref;
		}
	}
	if (sd::GetPlayer() == refToPlaceAt) {
		Console_Print("refToPlaceAt was g_thePlayer");
	}

	if (isActor && pImpl->eq.armor.size()) {
		auto npc = (TESNPC *)pImpl->base;
		auto baseOutfit = (BGSOutfit *)LookupFormByID(ID_BGSOutfit::ArmorBladesOutfitNoHelmet); // TODO: choose another base outfit?
		auto outfit = (BGSOutfit *)FormHeap_Allocate(sizeof BGSOutfit);
		if (outfit) {
			memcpy(outfit, baseOutfit, sizeof BGSOutfit);

			auto &array = outfit->armorOrLeveledItemArray;

			vector<TESForm *> forms;
			for (formid id : pImpl->eq.armor) {
				auto f = LookupFormByID(id);
				if (f && f->formType == kFormType_Armor) {
					forms.push_back(f);
				}
			}
			if (forms.size()) {
				array.Allocate(forms.size());
				for (size_t i = 0; i < forms.size(); ++i) array[i] = forms[i];

				npc->defaultOutfit = outfit;
				npc->sleepOutfit = outfit;
			}
		}
	}

	auto ref = refFromSetRef ? (TESObjectREFR *)LookupFormByID(refFromSetRef) : sd::PlaceAtMe(refToPlaceAt, pImpl->base, 1, true, false);
	if (!ref) throw with_trace(std::logic_error("unable to place"));

	assert(pImpl->wc);
	if(pImpl->wc) pImpl->wc->Flush();

	if (pImpl->count > 1) {
		auto name = papyrusObjectReference::GetDisplayName(ref);
		string *newName = new string(string(name.data) + " (" + std::to_string(pImpl->count) + ')');
		auto fs = new BSFixedString(newName->data());
		papyrusObjectReference::SetDisplayName(ref, *fs, true);
	}

	pImpl->spawnedData.reset(new Impl::SpawnedData);
	pImpl->id = ref->formID;
	pImpl->spawnMoment = clock();
}

void SmartRef::Despawn(const char *dbgReason) {
	if (!this->IsSpawned()) throw with_trace(std::logic_error("already despawned"));

	pImpl->wc->ModProtection(pImpl->id, -1);
	pImpl->id = 0;
	pImpl->despawnMoment = clock();
	this->OnDespawn();
	Console_Print("%s", dbgReason);
}

void SmartRef::SetPos(const NiPoint3 &pos) {
	pImpl->pos = pos;
}