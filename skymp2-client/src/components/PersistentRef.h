#pragma once
#include "IRef.h"

struct ActiEvent
{
	using Callback = std::function<void(ActiEvent)>;

	formid caster = 0;
	formid target = 0;


	bool IsEmpty() const noexcept {
		return !caster && !target;
	}
};

class PersistentRef : public IRef
{
public:
	void Update() override;

	void Push(const Movement &mov) override;
	void Push(const Inventory &inv) override;
	void SetMode(EntityMode mode) override;
	formid GetId() const override;
	Movement GetMovement() const override;

	explicit PersistentRef(formid id);
	~PersistentRef();

	static void SetOnActivate(ActiEvent::Callback callback);

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl;
	const formid id;
};