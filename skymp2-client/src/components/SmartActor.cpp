#include "../stdafx.h"
#include "SmartActor.h"

clock_t g_lastTintsApply = 0;

enum {
	GLOBAL_DELAY_MS = 250,
	SPAWN_DELAY_MS = 400
};

struct SmartActor::Impl
{
	std::optional<Look> look;
	bool tintsApplied = true;
	std::string name = " ";
	TESNPC *lookApplyRes = nullptr;
	clock_t spawnMoment = 0;
	bool los = false;
};

SmartActor::SmartActor(NiPoint3 spawnPoint, float drawDistance, WorldCleaner &wc, std::shared_ptr<IMovementInterpolator> movementInterpolator) 
	: SmartRef(LookupFormByID(0x0010D13E), spawnPoint, drawDistance, wc, movementInterpolator) {
	pImpl.reset(new Impl);
}

SmartActor::~SmartActor() {
}

void SmartActor::Push(const Look &look) {
	if (pImpl->look != look) {
		pImpl->tintsApplied = false;

		pImpl->look = look;
		pImpl->lookApplyRes = Look_::Apply(look);
		this->SetBaseForm(pImpl->lookApplyRes);
		this->WriteName(pImpl->lookApplyRes);
	}
}

void SmartActor::PushName(const std::string &name) {
	pImpl->name = name;
	this->WriteName(pImpl->lookApplyRes);
}

string SmartActor::GetName() const {
	return pImpl->name;
}

void SmartActor::OnUpdate() {
	if (pImpl->look.has_value() == false) return;

	auto ref = this->FindForm();
	assert(ref);
	assert(ref->formType == kFormType_Character);

	if (!ref) return;

	if (!pImpl->tintsApplied && clock() - g_lastTintsApply > GLOBAL_DELAY_MS && clock() - pImpl->spawnMoment > SPAWN_DELAY_MS) {
		g_lastTintsApply = clock();
		pImpl->tintsApplied = true;
		Look_::ApplyTints(*pImpl->look, (Actor *)ref);
	}
	sd::AllowPCDialogue((Actor *)ref, false); // TODO: no flood, api

	bool los = sd::HasLOS(sd::GetPlayer(), ref);
	if (los != pImpl->los) {
		pImpl->los = los;
		pImpl->tintsApplied = false;
	}
}

void SmartActor::OnDespawn() {
	// Is it OnSpawn???
	pImpl->spawnMoment = clock();
	pImpl->tintsApplied = false;
}

void SmartActor::WriteName(TESForm *out) const {
	if (!out) return;
	auto &name = pImpl->name;
	auto buf = new char[name.size() + 1];
	memcpy(buf, name.data(), name.size() + 1);
	papyrusForm::SetName(out, BSFixedString(buf));
}