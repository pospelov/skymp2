#pragma once
#pragma once

enum {
	RightHand = 0,
	LeftHand = 1
};

struct Equipment
{
	std::vector<formid> armor;
	formid hands[2];

	using Tuple = std::tuple<std::vector<formid>, formid, formid>;

	Equipment() = default;
	Equipment(Tuple t) {
		armor = std::get<0>(t);
		hands[0] = std::get<1>(t);
		hands[1] = std::get<2>(t);
	}

	Tuple toTuple() const {
		return std::make_tuple(armor, hands[0], hands[1]);
	}
};

using EqTuple = Equipment::Tuple;

#ifdef NLOHMANN_JSON_HPP
inline void to_json(json &j, Equipment &p) {
	auto res = json::object();
	res["rightHand"] = p.hands[RightHand];
	res["leftHand"] = p.hands[LeftHand];
	res["armor"] = json::array();
	for (size_t i = 0; i != p.armor.size(); ++i) {
		res["armor"].push_back(p.armor[i]);
	}
	std::swap(j, res);
}

inline void from_json(const json &j_, Equipment &p) {
	auto j = j_;
	p.hands[RightHand] = j["rightHand"];
	p.hands[LeftHand] = j["leftHand"];
	for (size_t i = 0; i != j["armor"].size(); ++i) {
		p.armor.push_back(j["armor"][i]);
	}
}
#endif

inline bool operator==(const Equipment &lhs, const Equipment &rhs) {
	return lhs.armor == rhs.armor && lhs.hands[0] == rhs.hands[0] && lhs.hands[1] == rhs.hands[1];
}

inline bool operator!=(const Equipment &lhs, const Equipment &rhs) {
	return !(rhs == lhs);
}

namespace Equipment_
{
	Equipment GetFrom(Actor *ac);

	// note: you should flood this function with 500ms interval
	void ApplyTo(const Equipment &eq, Actor *ac);

	// returns: false when player doesn't have such items
	bool ApplyToPlayer(const Equipment &eq);
}