#pragma once
#pragma once

#include <tuple>

struct Look
{

	uint8_t isFemale = 0;
	formid raceID = 0;
	uint8_t weight = 0;
	std::array<uint8_t, 3> skinRGB = { 0,0,0 };
	std::array<uint8_t, 3> hairRGB = { 0,0,0 };
	std::vector<formid> headparts;
	formid headTextureSet = 0;

	enum {
		NumOptions = 19,
		NumPresets = 4
	};

	std::array<float, NumOptions> options;
	std::array<uint32_t, NumPresets> presets;

	struct Tint
	{
		uint8_t texture = 0;
		uint32_t rgba = 0;
		float alpha = 0;
		uint32_t type = 0;

		using Tuple = std::tuple<uint8_t, uint32_t, float, uint32_t>;

		Tuple ToTuple() const {
			Tuple t;
			std::get<0>(t) = texture; std::get<1>(t) = rgba; std::get<2>(t) = alpha; std::get<3>(t) = type;
			return t;
		}

		Tint() = default;

		explicit Tint(uint8_t tex, uint32_t rgba_, float a, uint32_t t) :
			texture(tex), rgba(rgba_), alpha(a), type(t)
		{
		}

		explicit Tint(const Tuple &t) {
			texture = std::get<0>(t); rgba = std::get<1>(t); alpha = std::get<2>(t); type = std::get<3>(t);
		}
	};
	std::vector<Tint> tints;

	using Tuple = std::tuple<
		uint8_t, // isFemale 0
		formid, // raceId 1
		uint8_t, // weight 2
		std::array<uint8_t, 3>, // skinRGB 3
		std::array<uint8_t, 3>, // hairRGB 4
		std::vector<formid>, // headparts 5
		formid, // headTextureSet 6
		std::vector<Tint::Tuple>, // tints 7
		std::array<float, NumOptions>, // options 8
		std::array<uint32_t, NumPresets> // presets 9
	>;

	Tuple ToTuple() const {
		Tuple t;
		std::get<0>(t) = isFemale;
		std::get<1>(t) = raceID;
		std::get<2>(t) = weight;
		std::get<3>(t) = skinRGB;
		std::get<4>(t) = hairRGB;
		std::get<5>(t) = headparts;
		std::get<6>(t) = headTextureSet;

		std::vector<Tint::Tuple> tintTuples;
		tintTuples.reserve(tints.size());
		for (auto &tint : tints) tintTuples.push_back(tint.ToTuple());
		std::get<7>(t) = tintTuples;

		std::get<8>(t) = options;
		std::get<9>(t) = presets;

		return t;
	}

	Look() = default;

	Look(const Tuple &t) {
		isFemale = std::get<0>(t);
		raceID = std::get<1>(t);
		weight = std::get<2>(t);
		skinRGB = std::get<3>(t);
		hairRGB = std::get<4>(t);
		headparts = std::get<5>(t);
		headTextureSet = std::get<6>(t);

		for (auto &tinttuple : std::get<7>(t)) {
			tints.push_back(Tint(tinttuple));
		}

		options = std::get<8>(t);
		presets = std::get<9>(t);
	}
};

#ifdef NLOHMANN_JSON_HPP
inline void to_json(json &j, Look::Tint &p) {
	auto res = json::array();
	res.push_back(p.rgba);
	res.push_back(p.alpha);
	res.push_back(p.texture);
	res.push_back(p.type);
	std::swap(j, res);
}

inline void from_json(const json &j_, Look::Tint &p) {
	auto j = j_;
	p.rgba = j[0];
	p.alpha = j[1];
	p.texture = j[2];
	p.type = j[3];
}

inline void to_json(json &j, Look &p) {
	auto res = json::object();
	res["isFemale"] = p.isFemale;
	res["raceID"] = p.raceID;
	res["weight"] = p.weight;
	res["skinRGB"] = p.skinRGB;
	res["hairRGB"] = p.hairRGB;

	res["headparts"] = json::array();
	for (auto v : p.headparts) res["headparts"].push_back(v);

	res["options"] = json::array();
	for (auto v : p.options) res["options"].push_back(v);

	res["presets"] = json::array();
	for (auto v : p.presets) res["presets"].push_back(v);

	res["tints"] = json::array();
	for (auto &v : p.tints) {
		json jTints;
		to_json(jTints, v);
		res["tints"].push_back(jTints);
	}

	res["headTextureSet"] = p.headTextureSet;

	std::swap(j, res);
}

inline void from_json(const json &j_, Look &p) {
	auto j = j_;
	try {
		p.isFemale = j["isFemale"];
	}
	catch (...) {
		p.isFemale = false;
	}
	try {
		p.raceID = j["raceID"];
	}
	catch (...) {
		p.raceID = 0x00013746;
	}
	try {
		p.weight = j["weight"];
	}
	catch (...) {
		p.weight = 0;
	}
	try {
		p.skinRGB = j["skinRGB"];
	}
	catch (...) {
		p.skinRGB = { 0,0,0 };
	}
	try {
		p.hairRGB = j["hairRGB"];
	}
	catch (...) {
		p.hairRGB = { 0,0,0 };
	}
	try {
		p.headTextureSet = j["headTextureSet"];
	}
	catch (...) {
		p.headTextureSet = 0;
	}

	try {
		for (auto &v : j["headparts"]) p.headparts.push_back(v);
	}
	catch (...) {
		p.headparts.clear();
	}

	size_t i = 0;
	try {
		for (auto &v : j["options"]) {
			p.options[i++] = v;
			if (i == p.options.size()) break;
		}
	}
	catch (...) {
		for (auto &opt : p.options) opt = 0;
	}

	i = 0;
	try {
		for (auto &v : j["presets"]) {
			p.presets[i++] = v;
			if (i == p.presets.size()) break;
		}
	}
	catch (...) {
		for (auto &preset : p.presets) preset = 0;
	}

	try {
		for (auto &v : j["tints"]) {
			Look::Tint tint;
			from_json(v, tint);
			p.tints.push_back(tint);
		}
	}
	catch (...) {
		p.tints.clear();
	}
}
#endif

inline bool operator==(const Look::Tint &rhs, const Look::Tint &lhs) {
	return rhs.texture == lhs.texture && rhs.rgba == lhs.rgba && rhs.type == lhs.type && rhs.alpha == lhs.alpha;
}

inline bool operator!=(const Look::Tint &rhs, const Look::Tint &lhs) {
	return !(rhs == lhs);
}

inline bool operator==(const Look &rhs, const Look &lhs) {
	return rhs.isFemale == lhs.isFemale
		&& rhs.raceID == lhs.raceID
		&& rhs.weight == lhs.weight
		&& rhs.skinRGB == lhs.skinRGB
		&& rhs.hairRGB == lhs.hairRGB
		&& rhs.headparts == lhs.headparts
		&& rhs.headTextureSet == lhs.headTextureSet
		&& rhs.options == lhs.options
		&& rhs.presets == lhs.presets
		&& rhs.tints == lhs.tints;
}

inline bool operator!=(const Look &rhs, const Look &lhs) {
	return !(rhs == lhs);
}

namespace Look_ 
{
	Look GetFromPlayer();
	TESNPC *Apply(const Look &look, TESNPC *npc = nullptr);
	void ApplyTints(const Look &look, Actor *ac);

	void ApplyToPlayer(const Look &look);
}