#pragma once

#pragma once
#include <map>

struct Inventory
{
	std::map<formid, uint32_t> simple;

	struct Entry { formid id; uint32_t count; };

	void AddItem(Entry e) {
		if (e.count) simple[e.id] += e.count;
	}

	void RemoveItem(formid id, uint32_t c) {
		uint32_t count = 0;
		try {
			count = simple.at(id);
		}
		catch (...) {
			throw std::logic_error("no such item");
		}

		if (count < c) throw std::logic_error("no such item");
		if (count == c) {
			simple.erase(id);
		}
		else if (count > c) {
			simple[id] -= c;
		}
	}

	Inventory operator-(const Inventory &toRemove) const {
		auto res = *this;
		for (auto &p : toRemove.simple) {
			try {
				for (size_t i = 0; i != p.second; ++i) res.RemoveItem(p.first, 1);
			}
			catch (...) {

			}
		}
		return res;
	}
};

inline bool operator==(Inventory i1, Inventory i2) {
	return i1.simple == i2.simple;
}

// TODO: error handling

inline void to_json(json &j, Inventory::Entry &p) {
	auto res = json::array();
	res.push_back(p.id);
	res.push_back(p.count);
	std::swap(j, res);
}

inline void from_json(const json &j_, Inventory::Entry &p) {
	auto j = j_;
	assert(j.is_array());
	p.id = j[0];
	p.count = j[1];
}

inline void to_json(json &j, Inventory &p) {
	auto res = json::array();
	for (auto p : p.simple) {
		json el = json::object();
		el["id"] = p.first;
		el["count"] = p.second;
		res.push_back(el);
	}
	std::swap(j, res);
}

inline void from_json(const json &j_, Inventory &p) {
	auto j = j_;
	assert(j.is_array());
	for (size_t i = 0; i < j.size(); ++i) {
		auto el = j[i];
		Inventory::Entry e;
		e.id = el["id"];
		e.count = el["count"];
		p.AddItem(e);
	}
}

struct InvEvent
{
	Inventory::Entry entry = { 0,0 };
	formid ref = 0;
	const char *type = "";

	bool IsEmpty() const { return !type[0]; }
};

class IInventoryInterpolator
{
public:
	virtual void Update(TESObjectREFR *ref) = 0;
	virtual void PushInventory(const Inventory &inv) = 0;
	virtual ~IInventoryInterpolator() = default;
};