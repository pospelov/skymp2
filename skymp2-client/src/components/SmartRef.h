#pragma once
#include "IRef.h"

#include "../services/WorldCleaner.h"

class SmartRef : 
	public IRef,
	public IListener<Equipment>
{
public:
	void Update();

	void Push(const Movement &movement) override;
	void Push(const Inventory &inventory) override;
	void Push(const Equipment &equipment) override;
	void SetMode(EntityMode entityMode) override;
	formid GetId() const override;
	Movement GetMovement() const override;

	SmartRef(TESForm *baseForm, NiPoint3 spawnPoint, float drawDistance, WorldCleaner &wc, std::shared_ptr<IMovementInterpolator> movementInterpolator);
	virtual ~SmartRef();

	void SetBaseForm(TESForm *baseForm);
	void SetCount(uint32_t count);
	void SetRef(formid id); // be carefull

	// Do NOT use in production code
	void ForceDespawn();
	formid SwapRef(formid refId);

private:
	void Spawn(formid refFromSetRef = 0);
	void Despawn(const char *debugReason = "not specified");
	void SetPos(const NiPoint3 &pos); // Doesn't actually move the reference
	void SetMovementInterpolator(std::shared_ptr<IMovementInterpolator> movementInterpolator);
	void SetDrawDistance(float drawDistance);

	struct Impl;
	std::shared_ptr<Impl> pImpl;

protected:
	virtual void OnUpdate() {}
	virtual void OnDespawn() {}

	enum TaskFlags {
		EverySpawn = 1
	};

	using Task = std::function<void(TESObjectREFR *)>;
	void AddTask(Task fn, uint32_t flags = 0);
};