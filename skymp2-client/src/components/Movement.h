#pragma once
#include <tuple>

struct Movement
{
public:
	enum class RunMode
	{
		Standing = 0,
		Walking = 1,
		Running = 2,
		Sprinting = 3
	};

	NiPoint3 Rotation() const {
		return { float(_direction * 0.00549324788), float(_angleY * 0.00549324788), _angleZ * 1.4f };
	}
	RunMode GetRunMode() const {
		if (_flags & Walking) {
			return (_flags & Running) ? RunMode::Sprinting : RunMode::Walking;
		}
		return (_flags & Running) ? RunMode::Running : RunMode::Standing;
	}

	NiPoint3 Pos() const { return _pos; }
	float Direction() const { return float(_direction * 0.00549324788); }
	bool IsSneaking() const { return _flags & Sneaking; }
	bool IsWeapDrawn() const { return _flags & WeapDrawn; }
	bool IsInJumpState() const { return (_flags & Blocking) && !(_flags & WeapDrawn); }
	bool IsBlocking() const { return (_flags & Blocking) && (_flags & WeapDrawn); }
	bool IsDead() const { return (_flags & Dead); }
	bool IsGrabbed() const { return (_flags & Grabbed); }
	bool IsOpen() const { return (_flags & Open); }

	void SetPos(const NiPoint3 &newPos) { _pos = newPos; }
	void SetRotRadians(const NiPoint3 &rotRad) {
		NiPoint3 degrees;
		degrees.x = rotRad.x * (180.0 / 3.141592653589793238463);
		degrees.y = rotRad.y * (180.0 / 3.141592653589793238463);
		degrees.z = rotRad.z * (180.0 / 3.141592653589793238463);
		SetRot(degrees);
	}
	void SetRot(const NiPoint3 &rotDegrees) {
		_direction = uint16_t(rotDegrees.x / 0.00549324788);
		_angleY = uint16_t(rotDegrees.y / 0.00549324788);
		_angleZ = uint8_t(rotDegrees.z / 1.4);
	}

	Movement() = default;

	NiPoint3 _pos;
	uint16_t _angleY = 0; // for bow sync
	uint8_t _angleZ = 0; // angleZ / 1.4
	uint16_t _direction = 0; // / 0.00549324788
	uint8_t _flags = 0;
	string _lastAe;
	uint8_t _hpPercent = 0;

	enum {
		Walking = 1 << 0,
		Running = 1 << 1,
		// Standing is when !Walking && !Running
		// Sprinting is whan Walking && Running

		WeapDrawn = 1 << 2,
		Blocking = 1 << 3,
		// Blocking without WeapDrawn is Jumping!

		Sneaking = 1 << 4,
		Dead = 1 << 5,

		Grabbed = 1 << 6,
		Open = 1 << 7 // Harvested (flora and items)
	};
};

#ifdef NLOHMANN_JSON_HPP

#ifndef NIPOINT_CONVERTIONS
inline void to_json(nlohmann::json &j, NiPoint3 &p) {
	auto res = nlohmann::json::array();
	res.push_back(p.x);
	res.push_back(p.y);
	res.push_back(p.z);
	std::swap(j, res);
}

inline void from_json(const nlohmann::json &j_, NiPoint3 &p) {
	auto j = j_;
	try {
		p.x = j.at(0).get<float>();
	}
	catch (...) {
		p.x = 0.f;
	}
	try {
		p.y = j.at(1).get<float>();
	}
	catch (...) {
		p.y = 0.f;
	}
	try {
		p.z = j.at(2).get<float>();
	}
	catch (...) {
		p.z = 0.f;
	}
}
#endif

inline void to_json(json &j, Movement &p) {
	auto res = json::object();
	res["pos"] = p._pos;
	res["rot"] = p.Rotation();
	res["flags"] = p._flags;
	//res["lastAe"] = p._lastAe;
	res["hpPercent"] = p._hpPercent;
	std::swap(j, res);
}

inline void from_json(const json &j_, Movement &p) {
	auto j = j_;
	p.SetRot(j["rot"]);
	p._pos = j["pos"];
	p._hpPercent = j["hpPercent"];
	try {
		p._flags = j["flags"];
	}
	catch (...) {
		p._flags = 0;
	}
	try {
		p._lastAe = j["lastAe"];
	}
	catch (...) {
		p._lastAe = "";
	}
}
#endif

inline bool operator==(const Movement &lhs, const Movement &rhs) {
	json j1, j2;
	to_json(j1, Movement(lhs));
	to_json(j2, Movement(rhs));
	return j1.dump() == j2.dump();
}

inline bool operator !=(const Movement &lhs, const Movement &rhs) {
	return !(lhs == rhs);
}

class IMovementInterpolator
{
public:
	virtual void Update(TESObjectREFR *ref) = 0;
	virtual void PushMovement(const Movement &mov) = 0;
	virtual ~IMovementInterpolator() {};
};

namespace Movement_
{
	Movement GetFrom(TESObjectREFR *actor);
}