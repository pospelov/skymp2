#include "../stdafx.h"
#include "FindRefs.h"

vector<TESObjectREFR *> sdutil::FindRefs(TESObjectCELL *targetCell) {
	vector<TESObjectREFR *> res;
	res.reserve(512);
	auto currentCell = sd::GetParentCell(*g_thePlayer);
	if (currentCell) {
		const formid formID = currentCell->formID;
		enum { CELL_SEARCH_DEEPTH = 40 };
		for (formid tmpFormID = (formID > CELL_SEARCH_DEEPTH) ? formID - CELL_SEARCH_DEEPTH : 0; tmpFormID != formID + CELL_SEARCH_DEEPTH; ++tmpFormID)
		{
			auto tmpCell = (TESObjectCELL *)LookupFormByID(tmpFormID);
			if (tmpCell && tmpCell->formType == kFormType_Cell)
			{
				if (!targetCell || tmpCell == targetCell) {
					for (size_t i = 0; i != tmpCell->objectList.count; ++i) {
						auto refr = tmpCell->objectList[i];
						if (refr && !(refr->flags & TESForm::kFlagUnk_0x800)) // not disabled
							res.push_back(refr);
					}
				}
			}
		}
	}
	return res;
}