#pragma once

namespace sdutil {
	// https://stackoverflow.com/questions/2302841/win32-playsound-how-to-control-the-volume

	using namespace std;

	inline void ReadWavFileIntoMemory(string fname, BYTE** pb, DWORD *fsize) {
		ifstream f(fname, ios::binary);

		f.seekg(0, ios::end);
		int lim = f.tellg();
		*fsize = lim;

		*pb = new BYTE[lim];
		f.seekg(0, ios::beg);

		f.read((char *)*pb, lim);

		f.close();
	}

	// TODO: fix memory leak
	inline void PlayWav(const char *filename) {
		DWORD dwFileSize;
		BYTE* pFileBytes;

		auto prefix = SKYMP2_FILES_DIR "\\sound-fx-ui\\";
		string path;
		(path += prefix) += filename;
		ReadWavFileIntoMemory(path.data(), &pFileBytes, &dwFileSize);

		BYTE* pDataOffset = (pFileBytes + 40);

		float fVolume = 0.42;

		__int16 * p = (__int16 *)(pDataOffset + 8);
		for (int i = 80 / sizeof(*p); i < dwFileSize / sizeof(*p); i++) {
			p[i] = (float)p[i] * fVolume;
		}
		PlaySound((LPCSTR)pFileBytes, NULL, SND_MEMORY | SND_ASYNC);
	}
}