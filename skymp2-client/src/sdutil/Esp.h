#pragma once

namespace sdutil
{
	bool Requires(const char* modName);
	formid GetEspOffset();
}