#include "../stdafx.h"
#include "Esp.h"
#include <ShlObj.h>

bool sdutil::Requires(const char* modName) {
	bool result = false;

	char buf[MAX_PATH];
	SHGetFolderPath(nullptr, CSIDL_LOCAL_APPDATA, nullptr, SHGFP_TYPE_CURRENT, buf);
	strcat_s(buf, "\\Skyrim\\plugins.txt");

	std::ifstream in(buf);
	if (in.is_open()) {
		while (!in.eof()) {
			in.getline(buf, MAX_PATH);

			if (buf[0] == '#')
				continue;

			if (_stricmp(buf, modName) == 0) {
				result = true;
				break;
			}
		}

		in.close();
	}

	return result;
}

formid sdutil::GetEspOffset() {
	formid res = 0x00000000;
	formid AAASkympIsHere = 0x1328;
	while (1) {
		auto global = (TESGlobal *)LookupFormByID(res + AAASkympIsHere);
		if (global && global->formType == kFormType_Global && abs(reinterpret_cast<float &>(global->unk20) - 108.0) <= 0.5) {
			return res;
		}
		res += 0x01000000;
		if (res > 0xFE000000) throw std::logic_error("esp not found");
	}
}