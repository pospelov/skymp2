#include "../stdafx.h"
#include "Spawn.h"
#include "NewFormID.h"

// Note: Returned vector may contain duplicates
// Here is legacy code (April 2017 or earlier) with at least one mistake
// PVS-Studio message: V530 The return value of function 'unique' is required to be utilized. Spawn.cpp 39

// UPD: Patched in 2.1.11 to support DLC cells and worlds

inline std::vector<formid> GetLoadedRefs(formid worldSpaceID) {
	std::vector<formid> result;

	{

		const auto dh = DataHandler::GetSingleton();

		static std::map<TESWorldSpace *, std::vector<formid>> data;

		static auto fsMainMenu = new BSFixedString("Main Menu");
		if (MenuManager::GetSingleton()->IsMenuOpen(fsMainMenu))
			data.clear();

		if (data.empty()) {
			for (size_t i = 0; i < dh->worldSpaces.count; ++i) {
				auto ws = dh->worldSpaces[i];
				if (ws && ws->formType == kFormType_WorldSpace)
					data.insert({ ws,{} });

			}
			for (formid id = 0; id <= 0x03000000; ++id) {
				auto ref = (TESObjectREFR *)LookupFormByID(id);
				if (ref && ref->formType == kFormType_Reference) {
					void *refWp = ref->GetWorldspace();
					for (auto it = data.begin(); it != data.end(); ++it) {
						if (refWp != it->first) continue;
						int formType = ref->baseForm ? ref->baseForm->formType : -1;
						//if (ref->baseForm->formID < 0xff000000) continue;
						if (formType != kFormType_Static) continue;
						if (ref->baseForm && ref->baseForm->formID == ID_TESObjectSTAT::MapMarker) continue;
						if (ref->formType == kFormType_Water || ref->baseForm->formType == kFormType_Water) continue;
						if (ref->baseForm->formID >= 0xFF000000) continue; // To prevent water deletion
						if (ref->formID % 0x01000000 == 0x16fe2) continue; // Ship in Raven Rock

						it->second.push_back(ref->formID);
						///std::unique(it->second.begin(), it->second.end());
					}
				}
			}
		}

		result = data[(TESWorldSpace *)LookupFormByID(worldSpaceID)];
	}

	if (result.empty()) {
		std::stringstream ss;
		ss << "no loaded refs for WorldSpace " << std::hex << worldSpaceID;
		throw with_trace(std::logic_error(ss.str()));
	}

	return result;
}

inline void MoveTo(uint32_t markerRefID) {
	// TODO: ostringstream => sprintf_s
	std::ostringstream os;
	os << "player.moveto " << std::hex << markerRefID;
	auto str = os.str();
	sd::ExecuteConsoleCommand((char *)str.data(), nullptr);
}

inline void EnsureMainMenuOpen() {
	static auto fsMainMenu = new BSFixedString("Main Menu");
	if (!MenuManager::GetSingleton()->IsMenuOpen(fsMainMenu)) throw with_trace(std::logic_error("main menu must be open"));
}

inline void SpawnInInterior(TESObjectCELL *cell, NiPoint3 pos, float angleZ) {
	EnsureMainMenuOpen();
	CheckForm(cell);
	if (!cell->IsInterior()) throw with_trace(std::logic_error("interior expected"));


	auto marker = (TESObjectREFR *)LookupFormByID(0x000C44AD);
	if (!marker) throw with_trace(std::logic_error("marker not found"));

	marker->parentCell = cell;
	marker->pos = pos;
	marker->rot = { 0,0,angleZ };
	MoveTo(marker->formID);

	// TODO: ����� �� ������� �������� ������??
}

inline void SpawnInExterior(TESWorldSpace *world, NiPoint3 posLocalPl, float angleZ) {
	EnsureMainMenuOpen();
	CheckForm(world);

	const bool isInteriors = false; // TODO
	if (isInteriors) throw with_trace(std::logic_error("exterior worldspace expected"));

	auto markerIDs = GetLoadedRefs(world->formID);

	formid nearestMarker = markerIDs.front();
	auto nearestMarkerRef = (TESObjectREFR *)LookupFormByID(nearestMarker);
	CheckForm(nearestMarkerRef);

	for (auto marker : markerIDs) {

		auto markerRef = (TESObjectREFR *)LookupFormByID(marker);
		CheckForm(markerRef);

		const auto posNearest = sd::GetPosition(nearestMarkerRef),
			posI = sd::GetPosition(markerRef);
		if ((posNearest - posLocalPl).Length() > (posI - posLocalPl).Length())
			nearestMarker = marker;
	}
	nearestMarkerRef->pos = posLocalPl;
	nearestMarkerRef->rot = { 0,0,angleZ };
	MoveTo(nearestMarkerRef->formID);

	// TODO: ����� �� ������� �������� ������??
}

void sdutil::Spawn(formid cellOrWp, NiPoint3 pos, float angleZ) {
	auto form = LookupFormByID(cellOrWp);
	if (form == nullptr) {
		std::stringstream ss;
		ss << "sdutil::Spawn nullptr form " << std::hex << cellOrWp;
		throw with_trace(std::logic_error(ss.str()));
	}

	switch (form->formType) {
	case kFormType_Cell:
		SpawnInInterior((TESObjectCELL *)form, pos, angleZ);
		break;
	case kFormType_WorldSpace:
		SpawnInExterior((TESWorldSpace *)form, pos, angleZ);
		break;
	default:
		throw with_trace(std::logic_error("sdutil::Spawn cell or worldspace expected"));
	}
}
