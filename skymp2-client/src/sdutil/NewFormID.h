#pragma once

namespace sdutil
{
	inline formid NewFormID()
	{
		static std::mutex mutex;
		std::lock_guard<std::mutex> l(mutex);
		static formid id = ~0;
		do
		{
			id--;
		} while (LookupFormByID(id));
		return id;
	}
}