#pragma once
template <class EventT>
class BSTEventSource1
{

public:
	typedef BSTEventSink<EventT> SinkT;

	void AddEventSink(SinkT * eventSink) { AddEventSink_Internal(eventSink); }
	void RemoveEventSink(SinkT * eventSink) { RemoveEventSink_Internal(eventSink); }
	void SendEvent(EventT * evn) { SendEvent_Internal(evn); }

	void operator()(EventT * evn) { SendEvent_Internal(evn); }

protected:
	HIMIKA_DEFINE_MEMBER_FN(ctor, BSTEventSource1*, 0x0073E790);
	HIMIKA_DEFINE_MEMBER_FN(dtor, BSTEventSource1*, 0x00695990);
	HIMIKA_DEFINE_MEMBER_FN(AddEventSink_Internal, void, 0x006E3E30, SinkT * eventSink);
	HIMIKA_DEFINE_MEMBER_FN(RemoveEventSink_Internal, void, 0x008CE0C0, SinkT * eventSink);
	HIMIKA_DEFINE_MEMBER_FN(SendEvent_Internal, void, 0x006EBC10, EventT * evn);

	// members
	/*BSSpinLock			lock;				// 000
	tArray<SinkT *>	eventSinks;			// 008
	tArray<SinkT *>	addBuffer;			// 014 - schedule for add
	tArray<SinkT *>	removeBuffer;		// 020 - schedule for remove
	bool				stateFlag;			// 02C - some internal state changed while sending*/
};

struct TESActivateEvent
{
	TESObjectREFR	* target;		// 00 - TESObjectREFRPtr
	TESObjectREFR	* caster;		// 04 - TESObjectREFRPtr
};

uint8_t *holder = (uint8_t *)0x012E4C30;
auto actiEventSource = reinterpret_cast<BSTEventSource1<TESActivateEvent> *>(holder + 0x030);

template <>
class BSTEventSink <TESActivateEvent>
{
public:
	virtual ~BSTEventSink() {}	// todo?
	virtual	EventResult ReceiveEvent(TESActivateEvent * evn, EventDispatcher<TESActivateEvent> * dispatcher) = 0;
};

class ActivateEventSink : public BSTEventSink<TESActivateEvent>
{
public:
	using Callback = std::function<void(TESActivateEvent &)>;

	ActivateEventSink(Callback callback) : cb(callback) {}
	//virtual ~ActivateEventSink() override {}

	virtual	EventResult ReceiveEvent(TESActivateEvent * evn, EventDispatcher<TESActivateEvent> * dispatcher) {
		cb(*evn);
		return EventResult::kEvent_Continue;
	}

private:
	const Callback cb;
};