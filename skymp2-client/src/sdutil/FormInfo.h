#pragma once
#include "skse/GameData.h"

class FormInfo
{
public:
	formid id;
	string espm;

	static FormInfo Resolve(formid localId) {
		if (localId >= 0xff000000) return { localId - 0xff000000, "" };
		if (localId < 0x01000000) return { localId, "Skyrim.esm" };
		const auto dataHandler = DataHandler::GetSingleton();
		const uint8_t modIndex = localId / 0x01000000;
		const auto modCount = dataHandler->modList.loadedModCount;
		if (modCount <= modIndex) {
			std::stringstream ss;
			ss << std::hex << "FormInfo::Resolve " << localId << " failed, modCount is " << std::dec << modCount;
			throw std::logic_error(ss.str());
		}
		auto modInfo = dataHandler->modList.loadedMods[modIndex];
		if (!modInfo) throw std::logic_error("nullptr modInfo");
		return { localId % 0x01000000, modInfo->name };
	}

	formid toLocalId() const {
		if (espm.empty()) {
			assert(id < 0x01000000);
			return id + 0xff000000;
		}
		if (espm == "Skyrim.esm") {
			return id;
		}
		const auto dataHandler = DataHandler::GetSingleton();
		const auto modCount = (uint8_t)dataHandler->modList.loadedModCount;
		for (uint8_t i = 0; i < modCount; ++i) {
			auto modInfo = dataHandler->modList.loadedMods[i];
			if (modInfo->name == espm) {
				return 0x01000000 * i + id;
			}
		}
		throw std::logic_error("plugin with name " + espm + " not found");
	}

	string idToHex() const noexcept {
		std::stringstream ss;
		ss << std::hex << id;
		return ss.str();
	}

	json toJson() const {
		return {
			{"espm", espm}, {"id", id}
		};
	}

	static FormInfo fromJson(json j) {
		FormInfo res;
		res.espm = j.at("espm");
		res.id = j.at("id");
		return res;
	}
};