#pragma once

namespace sdutil
{
	vector<TESObjectREFR *> FindRefs(TESObjectCELL *cell = nullptr);
}