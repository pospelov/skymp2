/*
 * To build, set up your Release configuration like this:
 *
 * [Runtime Library]
 * Multi-threaded (/MT)
 *
 * Visit www.frida.re to learn more about Frida.
 */

#pragma comment(lib, "frida-gumjs.lib")

#include "frida-gumjs.h"
#include <windows.h>

int weapDrawn = 0;

static void on_message (GumScript * script, const gchar * message, GBytes * data, gpointer user_data);

void Hook()
{
  GumScriptBackend * backend;
  GCancellable * cancellable = NULL;
  GError * error = NULL;
  GumScript * script;
  GMainContext * context;
  
  gum_init_embedded ();

  backend = gum_script_backend_obtain_duk ();

  char src[8192];
  auto f = fopen("SKYMP2/hooks.js", "r");
  int c = 0;
  int i = 1;
  src[0] = fgetc(f);
  do {
	  c = fgetc(f);
	  if (c != EOF) {
		  src[i] = c;
		  ++i;
	  }
  } while (c != EOF);
  fclose(f);
  src[i] = 0;


  script = gum_script_backend_create_sync (backend, "example",
	  src,
      cancellable, &error);
  g_assert (error == NULL);

  gum_script_set_message_handler (script, on_message, NULL, NULL);

  gum_script_load_sync (script, cancellable);

  //MessageBeep (MB_ICONINFORMATION);
  Sleep (1);

  context = g_main_context_get_thread_default ();
  while (g_main_context_pending (context) || 1)
    g_main_context_iteration (context, TRUE);

  gum_script_unload_sync (script, cancellable);

  g_object_unref (script);

  gum_deinit_embedded ();
}

static void
on_message (GumScript * script,
            const gchar * message,
            GBytes * data,
            gpointer user_data)
{
  JsonParser * parser;
  JsonObject * root;
  const gchar * type;

  parser = json_parser_new ();
  json_parser_load_from_data (parser, message, -1, NULL);
  root = json_node_get_object (json_parser_get_root (parser));

  type = json_object_get_string_member (root, "type");
  if (strcmp (type, "log") == 0)
  {
    const gchar * log_message;

    log_message = json_object_get_string_member (root, "payload");
    //g_print ("%s\n", log_message);
	OutputDebugStringA(log_message);
  }
  else
  {
    //g_print ("on_message: %s\n", message);
	  OutputDebugStringA(message);
  }

  g_object_unref (parser);
}

extern "C" {
	__declspec(dllexport) int SetUpHooks() {
		Hook();
		return 0;
	}
}