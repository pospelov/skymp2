cmake_minimum_required(VERSION 3.12)
project(skymp2)

# http://cmake.3232098.n2.nabble.com/Default-visual-studio-filters-td6056770.html
MACRO (cmp_IDE_SOURCE_PROPERTIES SOURCE_PATH HEADERS SOURCES)
    STRING(REPLACE "/" "\\\\" source_group_path ${SOURCE_PATH}  )
    source_group(${source_group_path} FILES ${HEADERS} ${SOURCES})
ENDMACRO (cmp_IDE_SOURCE_PROPERTIES NAME HEADERS SOURCES INSTALL_FILES)

# https://stackoverflow.com/questions/24173330/cmake-is-not-able-to-find-boost-libraries
if(MSVC)
  SET (BOOST_ROOT "C:\\projects\\vcpkg\\installed\\x64-windows")
  SET (BOOST_INCLUDEDIR "C:\\projects\\vcpkg\\installed\\x64-windows\\include")
  SET (BOOST_LIBRARYDIR "C:\\projects\\vcpkg\\installed\\x64-windows\\lib")
  SET (BOOST_MIN_VERSION "1.55.0")
  set (Boost_NO_BOOST_CMAKE ON)
endif()
if(UNIX)
  SET (BOOST_ROOT "/vcpkg/installed/x64-linux")
  SET (BOOST_INCLUDEDIR "/vcpkg/installed/x64-linux/include")
  SET (BOOST_LIBRARYDIR "/vcpkg/installed/x64-linux/lib")
  SET (BOOST_MIN_VERSION "1.55.0")
  set (Boost_NO_BOOST_CMAKE ON)
endif()
FIND_PACKAGE(Boost ${BOOST_MIN_VERSION} REQUIRED filesystem iostreams) #lockfree, algorithm, lexical-cast, math, crc, signals2, stacktrace, asio, beast are headeronly
if (NOT Boost_FOUND)
  message(FATAL_ERROR "Fatal error: Boost (version >= 1.55) required.")
else()
  message(STATUS "Setting up BOOST")
  message(STATUS " Includes - ${Boost_INCLUDE_DIRS}")
  message(STATUS " Library  - ${Boost_LIBRARY_DIRS}")
  include_directories(${Boost_INCLUDE_DIRS})
  link_directories(${Boost_LIBRARY_DIRS})
endif (NOT Boost_FOUND)

if(MSVC)
  add_definitions(-D_CRT_SECURE_NO_WARNINGS)
  add_definitions(-D_SILENCE_ALL_CXX17_DEPRECATION_WARNINGS)
endif()

if(MSVC)
  # common
  file(GLOB COMMON_SRC
    "skymp2-client/common/*.h"
    "skymp2-client/common/*.cpp"
  )
  add_library(common ${COMMON_SRC})
  set_target_properties(common PROPERTIES COMPILE_FLAGS /FI"IPrefix.h")
  target_include_directories(common PUBLIC "skymp2-client")

  # SKSE
  file(GLOB SKSE_SRC
    "skymp2-client/skse/*.h"
    "skymp2-client/skse/*.cpp"
  )
  add_library(SKSE ${SKSE_SRC})
  set_target_properties(SKSE PROPERTIES COMPILE_FLAGS /FI"../common/IPrefix.h")
  target_include_directories(SKSE PUBLIC "skymp2-client")
  target_compile_definitions(SKSE PUBLIC RUNTIME=1 RUNTIME_VERSION=0x09200000)

  # ScriptDragon
  file(GLOB SCRIPTDRAGON_SRC
    "skymp2-client/scriptdragon/*.h"
    "skymp2-client/scriptdragon/*.cpp"
  )
  add_library(ScriptDragon ${SCRIPTDRAGON_SRC})

  # frida-gumjs
  file(GLOB FRIDA_GUMJS_SRC
    "skymp2-client/frida-gumjs/*.h"
    "skymp2-client/frida-gumjs/*.cpp"
  )
  link_directories("skymp2-client/frida-gumjs")
  add_library(frida-gumjs SHARED ${FRIDA_GUMJS_SRC})
  set_target_properties(frida-gumjs PROPERTIES
    OUTPUT_NAME "SKYMP2"
    SUFFIX ".hooks"
  )

  # lua
  file(GLOB LUA_SRC
    "lua/*.c",
    "lua/*.h"
  )
  get_filename_component(full_path ${CMAKE_CURRENT_SOURCE_DIR}/lua/lbitlib.c ABSOLUTE)
  list(REMOVE_ITEM LUA_SRC "${full_path}")
  get_filename_component(full_path ${CMAKE_CURRENT_SOURCE_DIR}/lua/lua.c ABSOLUTE)
  list(REMOVE_ITEM LUA_SRC "${full_path}")
  add_library(lua STATIC ${LUA_SRC})

  # imgui_lua_bindings
  file(GLOB IMGUI_LUA_BINDINGS_SRC
    "imgui_lua_bindings/imgui_lua_bindings.cpp"
  )
  add_library(imgui_lua_bindings STATIC ${IMGUI_LUA_BINDINGS_SRC})
  target_include_directories(imgui_lua_bindings PUBLIC
    "lua"
    "skymp2-client/src/libs" # for imgui.h
  )

  # skymp2-client
  file(GLOB SKYMP2_CLIENT_SRC
    "skymp2-client/src/*.cpp"
    "skymp2-client/src/*.h"
    "skymp2-client/src/components/*.cpp"
    "skymp2-client/src/components/*.h"
    "skymp2-client/src/dinput/*.cpp"
    "skymp2-client/src/dinput/*.hpp"
    "skymp2-client/src/directx/*.cpp"
    "skymp2-client/src/directx/*.h"
    "skymp2-client/src/gui/*.cpp"
    "skymp2-client/src/gui/*.h"
    "skymp2-client/src/sdutil/*.cpp"
    "skymp2-client/src/sdutil/*.h"
    "skymp2-client/src/services/*.cpp"
    "skymp2-client/src/services/*.h"
    "skymp2-client/src/libs/*.cpp"
    "skymp2-client/src/libs/*.h"
    "skymp2-client/src/libs/disasm/*.c"
    "skymp2-client/src/libs/disasm/*.h"
    "skymp2-client/src/libs/imgui/*.cpp"
    "skymp2-client/src/libs/imgui/*.h"
  )
  find_library(DX9_LIB d3dx9 "$ENV{DXSDK_DIR}/Lib/x86/" NO_DEFAULT_PATH) # https://gist.github.com/billyquith/3a509315722fc822c8e904bc903364b8
  add_library(skymp2-client SHARED ${SKYMP2_CLIENT_SRC})
  target_link_libraries(skymp2-client 
    ${DX9_LIB} 
	common 
	SKSE 
	ScriptDragon 
	lua 
	imgui_lua_bindings)
  set_target_properties(skymp2-client PROPERTIES
    CXX_STANDARD 17
    CXX_EXTENSIONS OFF
    OUTPUT_NAME "SKYMP2"
    SUFFIX ".asi"
  )
  target_include_directories(skymp2-client PUBLIC "LuaBridge/Source" "lua" "$ENV{DXSDK_DIR}/Include" "" "..")
  add_subdirectory("../skymp2-cellhost" build)
  target_link_libraries(skymp2-client skymp2-cellhost)

  # check version of modules
  set(REQUIRED "0.0.2")
  if(NOT ("$ENV{SKYMP2_CELLHOST_VERSION}" STREQUAL "${REQUIRED}"))
   message( FATAL_ERROR "\n\n\nWrong skymp2-cellhost version " "$ENV{SKYMP2_CELLHOST_VERSION}" "\n" "${REQUIRED}" " required" "\n\n\n")
  endif()

  # skymp2-update-service
  file(GLOB SKYMP2_UPDATE_SERVICE_SRC "skymp2-update-service/*.cpp" "skymp2-update-service/*.h")
  add_executable(skymp2-upd ${SKYMP2_UPDATE_SERVICE_SRC})
  set_target_properties(skymp2-upd PROPERTIES CXX_STANDARD 17)
endif()
