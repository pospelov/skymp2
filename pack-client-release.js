// run with node to pack Release version of client

var fs = require('fs');
var path = require('path')

//https://gist.github.com/tkihira/3014700
var mkdir = function(dir) {
	// making directory without exception if exists
	try {
		fs.mkdirSync(dir, 0755);
	} catch(e) {
		if(e.code != "EEXIST") {
			throw e;
		}
	}
};

var rmdir = function(dir) {
	if (path.existsSync(dir)) {
		var list = fs.readdirSync(dir);
		for(var i = 0; i < list.length; i++) {
			var filename = path.join(dir, list[i]);
			var stat = fs.statSync(filename);

			if(filename == "." || filename == "..") {
				// pass these files
			} else if(stat.isDirectory()) {
				// rmdir recursively
				rmdir(filename);
			} else {
				// rm fiilename
				fs.unlinkSync(filename);
			}
		}
		fs.rmdirSync(dir);
	} else {
		console.warn("warn: " + dir + " not exists");
	}
};

var copyDir = function(src, dest) {
	mkdir(dest);
	let resultSize = 0;
	var files = fs.readdirSync(src);
	for (var i = 0; i < files.length; i++) {
		var current = fs.lstatSync(path.join(src, files[i]));
		if (current.isDirectory()) {
			resultSize += copyDir(path.join(src, files[i]), path.join(dest, files[i]));
		} else if(current.isSymbolicLink()) {
			var symlink = fs.readlinkSync(path.join(src, files[i]));
			fs.symlinkSync(symlink, path.join(dest, files[i]));
		} else {
			resultSize += copy(path.join(src, files[i]), path.join(dest, files[i]));
		}
	}
	return resultSize;
};

var copy = function(src, dest) {
	fs.writeFileSync(dest, fs.readFileSync(src));
	return fs.statSync(dest).size;
};


// https://stackoverflow.com/questions/34518389/get-hash-of-most-recent-git-commit-in-node
function getCommitHash() {
	return fs.readFileSync('.git/refs/heads/dev').toString();
}

function makeid() {
	mkdir('temp');
	let commitHash = getCommitHash();
	let v = {
		counter: 0,
		commitHash: commitHash
	};
	if (!fs.existsSync('temp/v.json')) {
		fs.writeFileSync('temp/v.json', JSON.stringify(v), 'utf8');
	}
	v = JSON.parse(fs.readFileSync('temp/v.json', 'utf8'));
	if (v.commitHash != commitHash) {
		v.commitHash = commitHash;
		v.counter = 0;
	}
	else v.counter++;
	fs.writeFileSync('temp/v.json', JSON.stringify(v), 'utf8');
	return v.commitHash.slice(0, 7) + '-' + v.counter;
}

let size = copyDir('client-requirements', 'requirements');
size += copyDir('../skymp2-scripts', 'requirements/SKYMP2/scripts');

let buildId = makeid();
let version = fs.readFileSync('version.txt');
fs.writeFileSync('requirements/SKYMP2/version.txt', version);
fs.writeFileSync('requirements/SkyMP.exe.tmp', fs.readFileSync('../skymp2-installer/Release/skymp2-installer.exe'));
fs.writeFileSync('requirements/SKYMP2/build-id.txt', buildId);

let files = ['SKYMP2.asi', 'SKYMP2.hooks', 'skymp2-upd.exe', 'boost_system-vc141-mt-x32-1_68.dll'];
files.forEach(v => {
  let p = path.join('requirements', v);
  fs.writeFileSync(p, fs.readFileSync(path.join('build/Release', v)));
  size += fs.statSync(p).size;
});

console.log('size is', Math.ceil(size / 1024 / 102.4) / 10, 'MB');
console.log('version is', version + '-' + buildId);
let versionInfoPath = path.join('requirements', 'version-info.json');

let versionInfo = {};
versionInfo.size = size;

fs.writeFileSync(versionInfoPath, JSON.stringify(versionInfo));

fs.unlinkSync('requirements/SKYMP2/scripts/README.md');
fs.renameSync('requirements/SKYMP2/scripts/.git', './temp/.git' + Math.random())
