
// https://stackoverflow.com/questions/7956519/how-to-kill-processes-by-name-win32-api
#include <windows.h>
#include <process.h>
#include <Tlhelp32.h>
#include <winbase.h>
#include <string.h>
void killProcessByName(const char *filename)
{
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);
	BOOL hRes = Process32First(hSnapShot, &pEntry);
	while (hRes)
	{
		if (strcmp(pEntry.szExeFile, filename) == 0)
		{
			HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
				(DWORD)pEntry.th32ProcessID);
			if (hProcess != NULL)
			{
				TerminateProcess(hProcess, 9);
				CloseHandle(hProcess);
			}
		}
		hRes = Process32Next(hSnapShot, &pEntry);
	}
	CloseHandle(hSnapShot);
}

////////////////////////////////////////////////////////////////////////////////
// Copied from skymp2-installer.cpp!
// Keep these macros and functions same in both projects!

#define TEMP_FILE_5 "zzzrep_5.bin" // compare result (json)
#define TEMP_FILE_6 L"zzzrep_6.bin" // installed commit sha (wstring)

#define _URL "gitlab.com"//185.211.246.231:10080
#define PROJ_ID "9188337"//2
#define OWNER "pospelov"//root


#define SKYMP2_COMPARE_URL_FMT "http://"_URL"/api/v4/projects/"PROJ_ID"/repository/compare?from=%s&to=%s"
#define SKYMP2_BRANCHES_URL "http://"_URL"/api/v4/projects/"PROJ_ID"/repository/branches"
#define SKYMP2_MASTER_BRANCH "master"

#include <fstream>
#include <string>
#include <vector>
#include "../json/single_include/nlohmann/json.hpp"
using nlohmann::json;

#pragma comment (lib, "urlmon");

std::wstring GetCommitSha() {
	wchar_t tempPath[MAX_PATH] = L"";
	GetTempPathW(MAX_PATH, tempPath);
	static auto fileName = (TEMP_FILE_6);
	swprintf_s(tempPath, L"%s/%s", tempPath, fileName);
	auto f = std::wifstream(tempPath);
	if (f.good() == false) return L"";
	std::wstring result;
	f >> result;
	return result;
}

// Throws!
void Compare(const char *commit, bool *outStructureChanged, std::vector<std::string> *outChangedPaths) {

	char url[1024];
	sprintf_s(url, SKYMP2_COMPARE_URL_FMT, commit, SKYMP2_MASTER_BRANCH);

	static char tempPath[MAX_PATH] = "";
	if (!tempPath[0]) {
		GetTempPathA(MAX_PATH, tempPath);
		sprintf_s(tempPath, "%s/" TEMP_FILE_5, tempPath);
	}
	URLDownloadToFileA(0, url, tempPath, 0, 0);

	std::ifstream t(tempPath);
	const std::string compareDump((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	auto compare = json::parse(compareDump);
	auto diffs = compare.at("diffs");
	for (size_t i = 0; i < diffs.size(); ++i) {
		auto &diff = diffs[i];
		const auto old_path = diff.at("old_path").get<std::string>();
		const auto new_path = diff.at("new_path").get<std::string>();
		if (old_path != new_path) {
			*outStructureChanged = true;
			outChangedPaths->clear();
			return;
		}
		outChangedPaths->push_back(new_path);
	}
}

////////////////////////////////////////////////////////////////////////////////

// https://stackoverflow.com/questions/215963/how-do-you-properly-use-widechartomultibyte
// Convert a wide Unicode string to an UTF8 string
inline std::string toUtf8(const std::wstring &wstr)
{
	if (wstr.empty()) return std::string();
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed, 0);
	WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
	return strTo;
}

int main() {
	if (std::ifstream("only_check.tmp").good()) {
		DeleteFileA("only_check.tmp");

		bool structChanged = false;
		std::vector<std::string> paths;

		auto currentCommit = toUtf8(GetCommitSha());
		Compare(currentCommit.data(), &structChanged, &paths);

		int result = structChanged || paths.size() || currentCommit.empty();
		std::ofstream("check_res.tmp") << result;
		return 0;
	}

	killProcessByName("TESV.exe");
	STARTUPINFOW cif;
	PROCESS_INFORMATION pi;
	ZeroMemory(&cif, sizeof(STARTUPINFO));
	auto res = CreateProcessW(L"skse_loader.exe", NULL,
		NULL, NULL, FALSE, DETACHED_PROCESS, NULL, NULL, &cif, &pi) == TRUE;
	return -1;
}
