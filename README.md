# ![](https://pp.userapi.com/c837339/v837339766/6a2be/wYK7LEvTyT8.jpg)
# Building
0) Install [Visual Studio 2017 Community](https://visualstudio.microsoft.com/ru/downloads/)
1) Download and install [DirectX SDK](http://www.microsoft.com/en-us/download/details.aspx?id=6812)
2) Download and install [CMake](https://cmake.org/download/) for Windows with `Windows win64-x64 Installer`. Accept adding CMake to PATH environment variable for all users
3) Clone  [vcpkg](https://github.com/Microsoft/vcpkg) repo into `C:\projects\vcpkg`. This path is hardcoded into CMakeLists.txt
4) Run `bootstrap-vcpkg.bat` and `vcpkg integrate install` to install and integrate vcpkg after cloning
5) Run `vcpkg install boost-filesystem boost-iostreams boost-lockfree boost-algorithm boost-lexical-cast boost-math boost-crc boost-stacktrace boost-signals2 boost-asio boost-beast --triplet x64-windows` to install Boost Libraries
6) Clone skymp2 repo into `C:\projects\skymp2` and run `gen.bat`
7) Download and extract [frida-gumjs-devkit-12.2.25-windows-x86.exe](https://github.com/frida/frida/releases/download/12.2.25/frida-gumjs-devkit-12.2.25-windows-x86.exe), copy `frida-gumjs.lib` from extracted archive into `skymp2/skymp2-client/frida-gumjs`
7) Open `skymp2/build/skymp2.sln` with VS2017 and Build
# Publishing new version
```batch
node pack-client-release
```
Push `requirements` directory content to `skymp2-binaries` repo on GitLab.
